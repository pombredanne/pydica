Installing Bittorrent Sync on WinXP32 SP3
-----------------------------------------

This is possible, using the download link on

http://labs.bittorrent.com/experiments/sync/win-download.html


Installing Durus on Windows (WinXP32 or better) from source
-----------------------------------------------------------

- Install Python3 from the Python website

- create a Pydica directory

- run "pyvenv pydica/venv" in a fresh directory (or it will not work)

- cd pydica

- run "venv/Scripts/activate"

- install the win32 extensions using this download:
  http://sourceforge.net/projects/pywin32/files/pywin32/Build%20218/pywin32-218.win32-py3.3.exe/download
  install the download using
  (venv) D:\projects\daniel\pydica>easy_install "c:\Dokumente und Einstellungen\Administrator\Eigene Dateien\Downloads\pywin32-218.win32-py3.3.exe"

- install the compiler
  http://www.microsoft.com/visualstudio/deu#downloads+d-2010-express

With an installed MSVC 10 compiler, run

- pip install -e git+https://bitbucket.org/pydica/durus.git#egg=Durus


Installing Durus et al. by cloning
----------------------------------

- create a Bittorrent Sync folder for the Pydica tree

- sync from a Windows 7 32bit installation of Pydica.

recommendation: On initial sync, it seems to be better if the WinXP Sp3 machine's Pydica folder
is empty. After syncing, everything works fine, including PySide.


Installing SourceTree on WinXP32-SP3
------------------------------------

This is unfortunately not possible. Even after installing or copying
Sourcetree from Windows 7, the .exe is not recognized.

But you can sync/clone the Pydica repository and install TortoiseHG.

You can run Pydica, anyway.

Paramiko for Python3
--------------------

Paramiko can be installed from Jan N. Schulze's fork:
pip install -e git+git@github.com:nischu7/paramiko.git#egg=paramiko

The Python3 status of this package is still not "ready", the examples do not all work.
I am currentpy not thinking to use it, unless it becomes necessary.

Execnet
-------

This wonderful library from Holger Krekel provides a lot of convenience.
Unfortunately, the ssh support was not maintained since a long time.

Current working approach for Pydica
-----------------------------------

- We use Durus and not Zodb for now.

- we use Bittorrent Sync for the basic file replication service.

- we use a pool of servers. Every active client is also a server.
  Every client publishes its local network information.
  On startup, every client tried to connect to every server and validates his existence.
  
- Internet security is not implemented, yet. The Pydica clusters are currently for local
  clusters, only. There is only one cluster at the moment.
  
  