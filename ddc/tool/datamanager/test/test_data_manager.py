# -*- coding: utf-8 -*-
from __future__ import division, absolute_import, print_function, unicode_literals

import logging
from unittest.case import TestCase

from pythonic_testcase import *

from ddc.tool.datamanager import DataManager
from ddc.tool.storage.durus_ import calculate_checksum
from ddc.tool.storage.task import TaskType, Task, TaskStatus
from ddc.tool.storage.testhelpers import create_bunch, set_durus_loglevel
from ddc.tool.storage.paths import DataBunch


def checksum(value):
    if hasattr(value, 'filecontent'):
        chksum = calculate_checksum([value.filecontent])
    elif hasattr(value, 'read'):
        old_pos = value.tell()
        value.seek(0)
        chksum = calculate_checksum([value.read()])
        value.seek(old_pos)
    else:
        chksum = calculate_checksum(value)
    return chksum


class DataManagerTest(TestCase):
    def setUp(self):
        super(DataManagerTest, self).setUp()
        self._previous_loglevel = set_durus_loglevel(logging.ERROR)

    def tearDown(self):
        set_durus_loglevel(self._previous_loglevel)
        super(DataManagerTest, self).tearDown()

    def test_raises_exception_when_adding_incomplete_bunches(self):
        first = DataBunch(cdb='foo.cdb', ibf='foo.ibf', durus=None, ask=None)
        second = DataBunch(cdb='bar.cdb', ibf='bar.ibf', durus='bar.durus', ask=None)
        assert_false(first.is_complete(), message='bad test setup')
        assert_true(second.is_complete())

        data_mgr = DataManager()
        assert_raises(Exception, lambda: data_mgr.add_bunches((first, second)))
        assert_length(0, data_mgr.bunches)

    def test_can_return_batch_with_open_tasks_for_given_task_type(self):
        dummy_task = Task(1, 'dummy type', status=TaskStatus.NEW)
        closed_task = Task(0, TaskType.VERIFICATION, status=TaskStatus.CLOSED)
        verification_task = Task(1, TaskType.VERIFICATION, status=TaskStatus.NEW)

        first = create_bunch(nr_forms=1, tasks=(dummy_task, closed_task))
        second = create_bunch(nr_forms=2, tasks=(verification_task, ))
        assert_not_equals(first.cdb, second.cdb,
            message='bad test setup - can not tell the difference between CDB fixtures.')
        assert_not_equals(first.ibf, second.ibf,
            message='bad test setup - can not tell the difference between IBF fixtures.')

        data_mgr = DataManager()
        data_mgr.add_bunches((first, second))
        assert_none(data_mgr.find_batch_with_new_tasks('invalid'))

        verification_batch = data_mgr.find_batch_with_new_tasks(TaskType.VERIFICATION)
        assert_not_none(verification_batch,
            message='did not find batch with new verification tasks')
        assert_equals(checksum(second.cdb), checksum(verification_batch.cdb))
        assert_equals(checksum(second.ibf), checksum(verification_batch.ibf))

    def test_can_return_filenames_for_all_known_bunches(self):
        files = (
            '/foo/123.cdb', '/foo/img/345.ibf', '/bar/123.durus', '/bar/123.ask',
            '/bar/ABC.cdb', '/tmp/img/DEF.ibf', '/srv/GHI.durus', '/srv/GHI.ask',
        )
        first = DataBunch(files[0], files[1], files[2], files[3])
        second = DataBunch(files[4], files[5], files[6], files[7])
        data_mgr = DataManager()
        data_mgr.add_bunches((first, second))
        # sorted because I want to detected duplicates but don't care about
        # ordering
        assert_equals(sorted(files), sorted(data_mgr.all_files()))

    def test_can_remove_bunches(self):
        first = DataBunch(cdb='foo.cdb', ibf='foo.ibf', durus='foo.durus', ask=None)
        second = DataBunch(cdb='bar.cdb', ibf='bar.ibf', durus='bar.durus', ask=None)
        data_mgr = DataManager()
        data_mgr.add_bunches((first, second))
        assert_length(2, data_mgr.bunches)

        data_mgr.remove_bunch(second)
        assert_length(1, data_mgr.bunches)

        data_mgr.remove_bunches((first, ))
        assert_length(0, data_mgr.bunches)

    def test_raises_exception_when_trying_to_remove_unknown_bunch(self):
        bunch = DataBunch(cdb='foo.cdb', ibf='foo.ibf', durus='foo.durus',
                          ask='foo.ask')
        data_mgr = DataManager()
        assert_raises(ValueError, lambda: data_mgr.remove_bunch(bunch))

    def test_can_set_current_bunches(self):
        first = DataBunch(cdb='foo.cdb', ibf='foo.ibf', durus='foo.durus', ask='foo.ask')
        second = DataBunch(cdb='bar.cdb', ibf='bar.ibf', durus='bar.durus', ask='bar.ask')
        data_mgr = DataManager()
        data_mgr.add_bunch(first)
        assert_equals(sorted(first), sorted(data_mgr.all_files()))

        data_mgr.set_bunches([second])
        assert_equals(sorted(second), sorted(data_mgr.all_files()))
