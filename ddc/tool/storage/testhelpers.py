# -*- coding: utf-8 -*-
from __future__ import division, absolute_import, print_function, unicode_literals

import logging

from ddc.tool.storage.paths import DataBunch
from ddc.tool.storage.cdb.cdb_fixtures import create_cdb_with_dummy_data
from ddc.tool.storage.durus_ import create_durus_fixture
from ddc.tool.storage.ibf.ibf_fixtures import create_ibf
from ddc.tool.storage.ask.ask_fixtures import create_ask


__all__ = ['create_bunch', 'set_durus_loglevel']

def create_bunch(nr_forms, tasks=()):
    return DataBunch(
        cdb=create_cdb_with_dummy_data(nr_forms=nr_forms),
        ibf=create_ibf(nr_images=nr_forms),
        durus=create_durus_fixture(tasks=tasks),
        ask=create_ask(),
    )

def set_durus_loglevel(new_level):
    # Durus automatically logs some transaction info to stderr which is
    # annoying in the tests.
    # This helper method can be used to configure durus' logger with just one
    # line of code.
    logger = logging.getLogger('durus')
    previous_log_level = logger.level
    logger.setLevel(new_level)
    return previous_log_level

