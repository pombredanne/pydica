# -*- coding: utf-8 -*-
from __future__ import division, absolute_import, print_function, unicode_literals

from datetime import date as Date, timedelta as TimeDelta

from pycerberus.test_util import *

from ddc.validation.validators.date_validator import DateValidator


class DateValidatorTest(ValidationTest):
    validator_class = DateValidator

    def assert_error_with_key(self, error_key, value, *args, **kwargs):
        e = self.assert_error(value, *args, **kwargs)
        assert_equals(error_key, e.details().key())
        return e

    def test_accepts_valid_date(self):
        assert_equals((14, 2, 5), self.process('05.02.14'))
        assert_equals((12, 2, 29), self.process('29.02.12'))

    def test_rejects_invalid_patterns(self):
        self.assert_error_with_key('bad_pattern', '050214')
        self.assert_error_with_key('bad_pattern', 'foobar')

    def test_rejects_invalid_dates(self):
        self.assert_error_with_key('bad_date', '05.15.14')
        self.assert_error_with_key('bad_date', '85.02.14')

    def test_accepts_years_with_two_digits(self):
        assert_equals((80, 2, 5), self.process('05.02.80'))

        today = Date.today()
        yesterday = today - TimeDelta(days=1)
        two_digit_year = int(str(yesterday.year)[2:])
        yesterday_string = '%02d.%02d.%02d' % (yesterday.day, yesterday.month, two_digit_year)
        date_tuple = (two_digit_year, yesterday.month, yesterday.day)
        assert_equals(date_tuple, self.process(yesterday_string))

    def test_does_not_assume_century(self):
        # 2000-02-29 is valid but 1900-02-29 is not
        assert_equals((00, 2, 29), self.process('29.02.00'))

    def test_accepts_years_with_four_digits(self):
        assert_equals((1910, 2, 5), self.process('05.02.1910'))
        assert_equals((2012, 2, 5), self.process('05.02.2012'))
        assert_equals((5463, 2, 5), self.process('05.02.5463'))

    def test_can_reject_future_dates(self):
        self.init_validator(reject_future_dates=True)
        self.assert_error_with_key('future_date', '05.02.4711')

    def test_accepts_two_digit_dates(self):
        self.init_validator(reject_future_dates=True)
        this_year = Date.today().year
        next_year_two_digits = int(str(this_year)[2:])
        assert_equals(
            (next_year_two_digits, 2, 5),
            self.process('05.02.%d' % next_year_two_digits)
        )

