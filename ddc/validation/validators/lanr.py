# -*- coding: utf-8 -*-
from __future__ import division, absolute_import, print_function, unicode_literals

from .regex import RegexValidator


__all__ = ['LANRValidator']

class LANRValidator(RegexValidator):
    def __init__(self, *args, **kwargs):
        regex = '(?P<doctor_id>\d{6})(?P<check_digit>\d)(?P<specialty_code>\d{2})'
        super(LANRValidator, self).__init__(regex, *args, **kwargs)

    def messages(self):
        return {
            'bad_checkdigit': u'Bad check digit (computed %(computed)s but input contains %(provided)s).',
        }

    def validate(self, value, context):
        super(LANRValidator, self).validate(value, context)
        match = self.regex.match(value)
        doctor_id = match.groupdict()['doctor_id']
        expected_check_digit = int(match.groupdict()['check_digit'])

        checksum = 0
        for i, digit in enumerate(doctor_id):
            is_even = (i % 2) == 0
            factor = 4 if is_even else 9
            checksum += factor * int(digit)
        computed_check_digit = 10 - (checksum % 10)
        if computed_check_digit == 10:
            computed_check_digit = 0
        assert 0 <= (computed_check_digit) <= 9
        if expected_check_digit != computed_check_digit:
            self.raise_error('bad_checkdigit', value, context,
                provided=expected_check_digit, computed=computed_check_digit)

