# -*- coding: utf-8 -*-
from __future__ import division, absolute_import, print_function, unicode_literals

# if the path is found directly via the '' entry of sys.path, then
# we get no absolute path. This caused problems after using chdir in tests.
# This little initialization removes that problem.
# It is only needed in the top level package.

import os
__path__[0] = os.path.realpath(__path__[0])
__file__ = os.path.realpath(__file__)
rootpath = os.path.dirname(__path__[0])
