# -*- coding: utf-8 -*-
from __future__ import division, absolute_import, print_function, unicode_literals

from pycerberus.test_util import *

from ddc.validation import SimpleDB
from ddc.validation.validators.bsnr import BSNRValidator


class BSNRTest(ValidationTest):
    validator_class = BSNRValidator

    def assert_error_with_key(self, error_key, value, *args, **kwargs):
        e = self.assert_error(value, *args, **kwargs)
        assert_equals(error_key, e.details().key())
        return e

    def test_accepts_valid_number(self):
        assert_equals('123456789', self.process('123456789'))
        assert_equals('000000000', self.process('000000000'))

    def test_rejects_non_numeric_strings(self):
        self.assert_error_with_key('bad_pattern', 'abcdefghi')
        self.assert_error_with_key('bad_pattern', '00000000A')

    def test_rejects_number_strings_with_bad_number_of_digits(self):
        self.assert_error_with_key('bad_pattern', '1234567')
        self.assert_error_with_key('bad_pattern', '1234567890')

    def test_rejects_bad_input(self):
        self.assert_error(1)
        self.assert_error(None)
        self.assert_error([])

    def test_accepts_pseudo_bsnr(self):
        # Für Bundeswehrärzte, Zahnärzte und Hebammen gibt es keine LANR und
        # keine BSNR. Diese sollten die Pseudo-Nummern 179999900 für die BSNR
        # und 999999900 für die LANR verwenden.
        # https://de.wikipedia.org/wiki/Lebenslange_Arztnummer
        assert_equals('179999900', self.process('179999900'))

    def test_can_check_bsnr_against_db(self):
        db = SimpleDB(('123456789', ))
        self.init_validator(db=db)
        assert_equals('123456789', self.process('123456789'))
        self.assert_error_with_key('unknown_bsnr', '000000000')
        self.assert_error_with_key('unknown_bsnr', '987654321')

