# -*- coding: utf-8 -*-
from __future__ import division, absolute_import, print_function, unicode_literals

from logging.config import fileConfig
import os
import re
import sys

from ..lib.attribute_dict import AttrDict
from ..lib.multiconfig import SiteConfig
from ..plugin.pydica_plugins import init_plugin_loader


__all__ = ['init_environment']

def load_configuration(config_file):
    config_path = os.path.abspath(config_file)
    config_dir = os.path.dirname(config_path)
    local_config = ()
    for ini_ending in ('ini', 'cfg'):
        local_config_path = os.path.join(config_dir,
                                         'local.{}').format(ini_ending)
        if os.path.exists(local_config_path):
            local_config = (local_config_path, )
            break
    site_config = SiteConfig(mandatory=(config_file, ), optional=local_config)
    pydica_settings = site_config.to_dict('pydica', interpolations={'here': config_dir})
    return site_config, pydica_settings

def init_environment(config_file):
    """
    setting up the "environment" so that pydica-based scripts can run and
    returns the "context".
    "Set up" includes loading plugins, settings and logging configuration.
    "Context" contains information about the setup which can be accessed by
    various pydica components.

    If there is a file named "local.ini" in the same directory as the specified
    config file settings from "local.ini" are parsed as well and merged into
    the configuration. Items from "local.ini" have precedence over options in
    the specified config file.
    """
    full_config, pydica_settings = load_configuration(config_file)
    config_dir = os.path.dirname(config_file)
    env_dir = os.path.expanduser(pydica_settings.get('env_dir', config_dir))
    logging_conf = pydica_settings.get('logging_conf')
    if logging_conf:
        # maybe we should allow specifying an "error handler" instead of using
        # sys.exit?
        if not os.path.exists(logging_conf):
            msg = 'unable to find logging configuration in file %s' % logging_conf
            print(msg, file=sys.stderr)
            sys.exit(1)
        fileConfig(logging_conf, disable_existing_loggers=True)

    enabled_plugins = re.split('\s*,\s*', pydica_settings.get('enabled_plugins', '*'))
    plugin_loader = init_plugin_loader(enabled_plugins=enabled_plugins)
    plugin_loader.init()
    context = AttrDict({
        'env_dir': env_dir,
        'plugin_manager': plugin_loader,
        'settings': pydica_settings,
    })
    plugin_loader.initialize_plugins(context, full_config=full_config)
    return context
