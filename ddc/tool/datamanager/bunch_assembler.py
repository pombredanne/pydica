# -*- coding: utf-8 -*-
from __future__ import division, absolute_import, print_function, unicode_literals

import os

from ddc.lib.attribute_dict import AttrDict
from ..storage.paths import guess_bunch_from_path


__all__ = ['BunchAssembler']

class BunchAssembler(object):
    def __init__(self, filenames):
        self.complete = []
        self.incomplete = []
        self.errors = []
        self._fn_lookup = self._build_case_insensitive_filemap(filenames)
        good_filenames = self._fn_lookup.values()
        self._cdbs = list(filter(lambda f: f and f.lower().endswith('.cdb'), good_filenames))
        self._ibfs = list(filter(lambda f: f and f.lower().endswith('.ibf'), good_filenames))
        self._durus = list(filter(lambda f: f and f.lower().endswith('.durus'), good_filenames))
        self._asks = list(filter(lambda f: f and f.lower().endswith('.ask'), good_filenames))

    def process_filenames(self):
        self._assemble_bunches_from(self._cdbs)
        self._assemble_bunches_from(self._ibfs)
        self._assemble_bunches_from(self._durus)
        self._assemble_bunches_from(self._asks)

        assert len(self._cdbs) == 0
        assert len(self._ibfs) == 0
        assert len(self._durus) == 0
        assert len(self._asks) == 0
        return AttrDict(
            complete=tuple(self.complete),
            incomplete=tuple(self.incomplete),
            errors=tuple(self.errors),
        )

    def _build_case_insensitive_filemap(self, filenames):
        """
        Some filesystems are case-sensitive (notably most "Linux" filesystems).
        On the other hand some code might treat file names case-insensitively
        (and this assumption is fine for Windows and Mac OS).
        This method builds a case-insensitive mapping based on the given file
        names so we can treat file names as case-insensitive even on Linux.

        For maximum robustness we'll detect ambiguous file names (two files with
        the same name but different casing) and return these as errors which
        should be handled in a higher layer.
        """
        fn_lookup = dict()
        duplicate_names = set()
        for name in filenames:
            if not name:
                continue
            normalized_name = name.lower()
            if normalized_name in fn_lookup:
                known_name = os.path.basename(fn_lookup[normalized_name])
                new_name = os.path.basename(name)
                error = AttrDict(
                    text='Ambiguous filenames (%s, %s) detected' % (known_name, new_name),
                    filename=name
                )
                self.errors.append(error)
                duplicate_names.add(normalized_name)
                continue
            fn_lookup[normalized_name] = name
        for name in duplicate_names:
            del fn_lookup[name]
        return fn_lookup

    def _assemble_bunches_from(self, base_list):
        for path in tuple(base_list):
            bunch = guess_bunch_from_path(path, self._fn_lookup)
            self._remove_assembled_files_from_pending_lists(bunch)
            if bunch.is_complete():
                self.complete.append(bunch)
            else:
                self.incomplete.append(bunch)

    def _remove_assembled_files_from_pending_lists(self, bunch):
        if bunch.cdb is not None:
            self._cdbs.remove(bunch.cdb)
        if bunch.ibf is not None:
            self._ibfs.remove(bunch.ibf)
        if bunch.durus is not None:
            self._durus.remove(bunch.durus)
        if bunch.ask is not None:
            self._asks.remove(bunch.ask)

