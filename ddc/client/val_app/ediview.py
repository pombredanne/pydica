#!/usr/bin/env python
# -*- coding: utf-8 -*-
from __future__ import division, absolute_import, print_function, unicode_literals

"""
This is the original client_val application.
It is used for testing and reference if needed.
"""


import os
import sys

# quick fix that makes it work. But I don't like this very much...
_path = os.path.realpath(os.path.join(os.path.dirname(__file__), '../../..'))
if _path not in sys.path:
    sys.path.insert(0, _path)

import ddc.config.pyside_fix
from PySide import QtCore, QtGui


CONTROL_PATH = os.getcwd()
# XXX this needs to be split up and moved to the app.__main__ file
# just here to see if it works
from ddc.tool.remote_control import (CDB_Reader, RemoteControl,
                                     COMMAND_FN, HANDSHAKE_FN)
from ddc.client.val_app import CollectionViewer
from ddc.tool.cdb_collection import CDB_Collection

DEFAULT_DEBUG = False

# short hack for reading the real CONTROL_PATH from the file 'control_path.txt'
# later this should be configurable in an ini-file
control_path_filename = os.path.join(CONTROL_PATH, 'control_path.txt')
if os.path.isfile(control_path_filename):
    new_control_path = open(control_path_filename).read().rstrip()
    if os.path.isdir(new_control_path):
        CONTROL_PATH = new_control_path
print('CONTROL_PATH = {}'.format(CONTROL_PATH))

DATABASE_PATH = ddc.rootpath + r'\private\srw\data'
CDB_NAME = '00099201.CDB'
SELFTEST = DEFAULT_DEBUG
USE_REMOTE = True
REMOTE_TEST_AT_START = DEFAULT_DEBUG

def main(DATABASE_PATH=DATABASE_PATH):
    app = QtGui.QApplication(sys.argv)
    if sys.argv[1:]:
        DATABASE_PATH = sys.argv[1]
    myapp = CollectionViewer()
    myapp.gui.stackedWidget.setCurrentIndex(0) # override whatever the GUI was saved with
    myapp.show()
    #myapp.gui.showFullScreen()
    # for now, disable maximized in the wing debugger
    if "WINGDB_ACTIVE" not in os.environ:
        myapp.gui.showMaximized()
    seq = CDB_Collection()
    myapp.open_sequence(seq)
    rem = RemoteControl(CONTROL_PATH)
    reader = CDB_Reader()
    reader.signal_goto.connect(myapp.goto_form)
    reader.signal_open.connect(myapp.open_sequence)
    reader.signal_setfocus.connect(myapp.setfocus_field)
    reader.signal_highlight.connect(myapp.field_highlight)
    reader.signal_delete.connect(myapp.delete_current_form)
    reader.signal_undelete.connect(myapp.undelete_current_form)
    reader.signal_reklafax.connect(myapp.reklafax)
    reader.signal_ascii.connect(myapp.ascii_export)

    # try it out

    if SELFTEST:
        from ddc.tool.test import test_remote_control as t
        tc = t.TestRemote()
        tc.setup_class()
        tc.setup_method(tc.test_cdb_open_real_ok)
        tc.test_cdb_open_real_ok(reader)
        tc.rem.stop()

    if USE_REMOTE:
        rem.got_opendel_cmd.connect(reader.opendel_cmd)
        rem.got_openasc_cmd.connect(reader.openasc_cmd)
        rem.got_setfocus_cmd.connect(reader.setfocus_cmd)
        rem.got_highlight_cmd.connect(reader.highlight_cmd)
        rem.got_delete_current_cmd.connect(reader.delete_cmd)
        rem.got_undelete_current_cmd.connect(reader.undelete_cmd)
        rem.got_reklafax_cmd.connect(reader.reklafax_cmd)
        rem.got_ascii_cmd.connect(reader.ascii_cmd)
        rem.start()

        if REMOTE_TEST_AT_START:
            rem.debug = 1
            for CDB_NAME, nr_pic_nr in (
                ('00955412.CDB', '1;30195505779024'),
                ('00955406.CDB', '300;30195504558024'),
                ):
                with open(os.path.join(CONTROL_PATH, COMMAND_FN), 'w') as cmd:
                    cmd.write(r'''
                        OPENdel {dbp}\{cdb};{npn}

                        highlight vorname;egal
                        setfocus strasse;hugelmugel
                        highlight kassennr;wurst
                        highlight taxe_1;42
                        delete_current
                        #reklafax {dbp}\{cdb};{npn};das ist der Text
                    '''.format(dbp=DATABASE_PATH, cdb=CDB_NAME, npn=nr_pic_nr))
                with open(os.path.join(CONTROL_PATH, HANDSHAKE_FN), 'w') as hsh:
                    pass
                while os.path.exists(hsh.name):
                    app.processEvents()
                if 0:
                    mbox = QtGui.QMessageBox()
                    mbox.setText(nr_pic_nr)
                    mbox.setInformativeText(CDB_NAME)
                    mbox.setIconPixmap(myapp.gui.windowIcon().pixmap(64, 64))
                    mbox.exec_()

            """
            for nr_pic_nr in (
                '4;20309900836024',
                '300;20309901133024',
                '4;20309900836024',
                '300;20309901133024',
                '4;20309900836024',
                '300;20309901133024',
                # some tests for the wrong nr for the given pic_nr
                '3;20309900836024', # should find nr. 4
                '5;20309900836024', # should also find nr. 4
                ):
                with open(os.path.join(CONTROL_PATH, COMMAND_FN), 'w') as cmd:
                    cmd.write(r'OPEN {}\{};{}'
                              .format(DATABASE_PATH, CDB_NAME, nr_pic_nr))
                with open(os.path.join(CONTROL_PATH, HANDSHAKE_FN), 'w') as hsh:
                    pass
                while os.path.exists(hsh.name):
                    app.processEvents()
            """

    sys.exit(app.exec_())

if __name__ == "__main__":
    main()
