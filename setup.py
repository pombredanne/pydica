#!/usr/bin/env python
# -*- coding: utf-8 -*-
from __future__ import division, absolute_import, print_function, unicode_literals

import re

from setuptools import setup, find_packages


def requires_from_file(filename):
    requirements = []
    with open(filename, 'r') as requirements_fp:
        for line in requirements_fp.readlines():
            match = re.search('^\s*([a-zA-Z][^#]+?)(\s*#.+)?\n$', line)
            if match:
                requirements.append(match.group(1))
    return requirements

setup(
    name='pydica',
    version='0.4dev',

    zip_safe=False,
    packages=find_packages(),
    include_package_data=True,
    tests_require=requires_from_file('dev_requirements.txt'),
    install_requires=requires_from_file('requirements.txt'),
    entry_points = {
        'console_scripts': [
            'start-introducer = ddc.tool.datamanager.introducer:scan_and_init',
            'pydica-gui = ddc.client.val_app.__main__:main',
            'pydica-ediview = ddc.client.val_app.ediview:main',
        ],
    }

)
