# -*- coding: utf-8 -*-
from __future__ import division, absolute_import, print_function, unicode_literals

from .loader import PluginLoader

__all__ = ['init_plugin_loader', 'PydicaPlugin']

class PydicaPlugin(object):
    display_name = 'generic pydica plugin'

    def validation_heuristics(self, context):
        return ()

    def form_validation_schema(self, context):
        return None

    def post_processors(self, context):
        return ()

    def initialize(self, context, **kwargs):
        """
        Plugin loader will call this method at startup after all enabled
        plugins are loaded and the basic context was populated.
        """
        pass

    def batch_metadata(self, batch=None, bunch=None):
        """
        Return batch-specific meta data. The information is passed into the
        validation context (as "batch_meta").
        """
        return {}

    def form_metadata(self, form, form_position, context):
        """
        Return form-specific meta data. The information is passed into the
        validation context (as "form_meta").
        """
        return {}


def init_plugin_loader(enabled_plugins=('*', )):
    return PluginLoader('pydica.plugin', enabled_plugins=enabled_plugins)
