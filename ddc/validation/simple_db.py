# -*- coding: utf-8 -*-
from __future__ import division, absolute_import, print_function, unicode_literals

import re


__all__ = ['SimpleDB', 'MappedDB']

class SimpleDB(object):
    def __init__(self, items):
        self.items = items

    @classmethod
    def from_string(cls, content):
        items = set()
        for raw_line in re.split('\s*\r?\n\s*', content):
            line = raw_line.strip()
            if line == '' or line[0] in ('#', ';'):
                continue
            items.add(line)
        return SimpleDB(items)

    def contains(self, value):
        return (value in self.items)


class MappedDB(SimpleDB):
    @classmethod
    def from_string(cls, content):
        simple_db = super(MappedDB, cls).from_string(content)
        split_regex = re.compile('^(\S+)\s*=\s*(\S+)$')
        mapping = dict()
        for item in simple_db.items:
            match = split_regex.search(item)
            source, target = match.groups()
            mapping[source] = target
        return MappedDB(mapping)

    def lookup(self, input_):
        return self.items[input_]

