# -*- coding: utf-8 -*-
from __future__ import division, absolute_import, print_function, unicode_literals

'''
Field painting classes
'''
import ddc.config.pyside_fix
from ddc.client.config import config_base
from PySide import QtCore, QtGui


class EditField(config_base.FieldMetric):
    coord_down = False

    def __init__(self, field):
        self.field = field
        rec = self.field.rec
        self.bbox = rec.left, rec.top, rec.right, rec.bottom
        self.parent = field.parent
        self.style_name = getattr(field.widget, 'style_name', None)


def make_simple_margins(delta):
    return (delta, delta, delta, delta)

def rect_add_margins(rect, margins):
    x, y, width, height = rect
    left, top, right, bottom = margins
    return x - left, y - top, width + left + right, height + top + bottom

# not sure where this should go: paint the graphic fields?
# first quick attempt...
class PaintSettings:
    field_painting = False

    def set_painting(self, painting):
        assert bool(painting)
        self.field_painting = painting

paint_settings = PaintSettings()


class NormalColor:
    # rectangle frame
    fg_color = QtGui.QColor(0, 0, 0)
    fg_color.setGreen(255)
    fg_color.setAlpha(192)
    # rectangle fill
    bg_color = QtGui.QColor(0, 0, 0)
    bg_color.setGreen(255)
    bg_color.setAlpha(10)
    # the tiny text
    txt_color = QtGui.QColor(128, 128, 128)
    txt_color.setAlpha(128)

class ErrorColor:
    # rectangle frame
    fg_color = QtGui.QColor(0, 0, 0)
    fg_color.setRed(255)
    fg_color.setAlpha(192)
    # rectangle fill
    bg_color = QtGui.QColor(0, 0, 0)
    bg_color.setRed(255)
    bg_color.setAlpha(10)
    # the tiny text
    txt_color = QtGui.QColor(128, 128, 128)
    txt_color.setAlpha(128)

class WarningColor:
    # rectangle frame
    fg_color = QtGui.QColor(0, 0, 0)
    fg_color.setRed(255)
    fg_color.setGreen(255)
    fg_color.setAlpha(192)
    # rectangle fill
    bg_color = QtGui.QColor(0, 0, 0)
    bg_color.setGreen(255)
    bg_color.setBlue(255)
    bg_color.setAlpha(10)
    # the tiny text
    txt_color = QtGui.QColor(128, 128, 128)
    txt_color.setAlpha(128)

style_mapping = {
    'normal': NormalColor,
    'error': ErrorColor,
    'warning': WarningColor,
}


class FieldPainter(object):
    def __init__(self, field_list, delete_check_func):
        self.field_list = field_list
        self.delete_check_func = delete_check_func
        self.aspect_factor = 1.0

    def set_aspect_ratio(self, yfactor):
        self.aspect_factor = yfactor

    def get_focused_field(self):
        focused = None
        for field in self.field_list:
            if field.widget.hasFocus():
                focused = field
        return focused

    def ensure_focused_field_visible(self, container):
        focused = self.get_focused_field()
        if focused:
            edit = EditField(focused)
            left, top, width, height = edit.rect
            cx, cy = left + width/2, top + height/2
            # compute the margin to the extreme
            w = container.width()
            h = container.height()
            # use the aspect factor to adjust the target position
            korr = self.aspect_factor
            cy *= korr
            cx *= korr
            max_ensure = cx, cy, w//2, h//2
            container.ensureVisible(*max_ensure)

    def paint(self, qp):

        # find out which field is focused.
        focused = self.get_focused_field()

        for field in self.field_list:
            edit = EditField(field)
            # skip fields that are not focused, unless we have no focus:
            # in that case we show them all.
            if focused and field.widget is not focused.widget:
                continue

            if not paint_settings.field_painting:
                continue

            style = NormalColor
            if edit.style_name:
                style = style_mapping[edit.style_name]

            fg_color = style.fg_color
            bg_color = style.bg_color
            txt_color = style.txt_color

            qp.setBrush(bg_color)

            qp.setPen(fg_color)

            pen = qp.pen()
            pen.setWidth(2)
            qp.setPen(pen)
            margins = make_simple_margins(16)
            m_rect = rect_add_margins(edit.rect, margins)
            qp.drawRect(*m_rect)

            # now draw the tiny imprint
            pen.setWidth(0)
            pen.setColor(txt_color)
            qp.setPen(pen)
            qp.drawText(QtCore.QPoint(field.x+2, field.y+field.h-2), field.field_name)

        if self.delete_check_func and self.delete_check_func():
            color = QtGui.QColor(0, 0, 0)
            color.setNamedColor('#ff4040')
            color.setAlpha(30)
            qp.setBrush(color)
            qp.drawRect(qp.window())
            font = qp.font()
            font.setPointSize(160)
            color.setAlpha(192)  # max = 255
            qp.setFont(font)
            qp.setPen(color)
            # intermediate
            win = qp.window()
            centerx, centery = win.width()//2, win.height()//2
            qp.translate(centerx, centery)
            qp.rotate(30)
            qp.translate(-centerx, -centery)
            qp.drawText(qp.window(), QtCore.Qt.AlignCenter, 'DELETED')
            x = 42
