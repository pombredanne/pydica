#!/usr/bin/env python
# -*- coding: utf-8 -*-
from __future__ import division, absolute_import, print_function, unicode_literals

import sys

from types import ModuleType
from zodbpickle import slowpickle as pickle
# XXX zodbpickle error: slowpickle.Pickler has no "errors"
from zodbpickle import fastpickle as pickle

"""
Design of path unaware pickling.
"""

class SomeClass(object):
    def __init__(self, name):
        self.name = name

def set_module_path(klass, modulepath):
    klass.__realmodule__ = klass.__module__
    klass.__module__ = modulepath
    sys.modules[modulepath] = sys.modules[klass.__realmodule__]
    sys.modules['org'] = object()

def runtest():
    data = SomeClass("hello")
    dmp = pickle.dumps(data, protocol=0)
    del data
    data = pickle.loads(dmp)
    assert data.name == 'hello'
    assert data.__module__ == datapath

datapath = "org.pydica.ddc.datadef.demo"

if __name__ == "__main__":
    set_module_path(SomeClass, datapath)

    runtest()