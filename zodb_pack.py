from ZODB import FileStorage, DB

storage = FileStorage.FileStorage('/tmp/mystorage.fs')
db = DB(storage)
db.pack()
db.close()
