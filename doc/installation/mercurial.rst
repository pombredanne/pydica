Mercurial
=========

Installation
------------

Windows 7
`````````
#. `mercurial-2.1.2-x86.msi <http://mercurial.selenic.com/release/windows/mercurial-2.1.2-x86.msi>`_
   installiert
#. ``C:\Users\daniel\mercurial.ini`` erzeugt und Extensions aktiviert


OS X
`````

Stand: 24.02.2013

Unter OS X ist folgendes wichtig bzw. wird es ab jetzt:

- Python (2 oder 3) kann mit Homebrew installiert werden.

::

  $ brew install python
  $ brew install python3 # optional if you are a python3-wheenie
  $ brew install qt
  
- Mercurial

  Installieren Sie es _nicht_ mit Homebrew, oder es wird mit virtualenv
  schief laufen.
  Statt dessen:
  
::

  $ pip install mercurial
  
Alle anderen, hier nicht erwaehnten Module sollen mit `pip` installiert werden.


Konfiguration
-------------

Einträge in der Konfigurationsdatei (``~/.hgrc`` unter Linux/MacOS,
``%USERPROFILE%\Mercurial.ini`` unter Windows)::

  [ui]
  username = Vorname Nachname <Vorname@Nachname.de>
  ssh = ssh -C

  [extensions]
  # manage different line endings between Linux/MacOS and Windows
  eol =

  # show progress bar for various operations
  progress = 

  # hg glog shows a semi-graphic log
  graphlog =

  # colors the output, for instance of hg diff
  color =

  # handle large and/or binary files
  largefiles =

  [largefiles]
  # hint: the first large file has to be added by hand with hg add --large filename and hg add --normal filename if it's no large file
  minsize = 10
  patterns = *.CDB *.RDB *.IBF *.jpg *.jpeg *.JPG *.TIF *.tiff *.bmp *.zip *.bz2 *.gz *.tgz *.bmp *.png *.pdf *.PDF *.ast


Projekt kopieren
----------------

::

  $ hg clone https://bitbucket.org/pydica/pydica


Weitere Informationen
---------------------

- `Mercurial CACertificates`_

.. _`Mercurial CACertificates`: http://mercurial.selenic.com/wiki/CACertificates


