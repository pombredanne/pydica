#!/usr/bin/env python
# -*- coding: utf-8 -*-
from __future__ import division, absolute_import, print_function, unicode_literals

"""
Design draft of a task database entry
-------------------------------------

This is a draft of a database design for tasks and associated structures.
Since a final design is impossible in the first place, I am writing this as an
initial draft, well aware that this structure will be changed in no time.

The reason of submitting this is to have a working start for discussion.

Things that are not done, yet:
------------------------------

- the task class is still dependant from the location of this module.
  This will be improved after the first discussion.

- there is no real task defined at all.
"""

from durus.persistent import PersistentObject as Persistent


class TaskType(object):
    """
    The definition of a class of tasks.
    This class is not creating instances in a database, but fixed descriptions.
    """

    # just a demo entry, should be much more elaborate
    graph = {}
    graph['entry'] = ('exit',)

    def __init__(self, description):
        assert isinstance(description, str)
        self.description = description


class Status(object):
    """
    A task status is the current position of the task in the workflow graph.
    Right now, the graph is quite undefined...
    """

    _status_entries = tuple("entry exit".split()) # to be extended, intensively

    def __init__(self, status=None):
        if not status:
            status = "entry"
        # ensure that the status entries are checked against a list of possible values
        assert status in self._status_entries
        self.status = status


class Actor(object):
    """
    The actors for a task are defined as persons or processes.
    The default actor is defined as "systen".
    Actors should be defined in the code, as long as the number of entries
    is just a few.
    """
    def __init__(self, shortname, *args, **kwds):
        assert isinstance(shortname, str)
        self.shortname = shortname


class HumanActor(Actor):
    def __init__(self,
                 shortname,
                 fullname,
                 roles,
                 email):
        super(HumanActor, self).__init__(shortname)
        assert isinstance(fullname, str)
        self.fullname = fullname
        assert isinstance(roles, (list, tuple))
        self.roles = roles
        assert isinstance(email, str)
        # XXX add some more checks, here
        self.email = email
        # ... add more attributes as needed


class SystemActor(Actor):
    def __init__(self,
                 shortname):
        super(SystemActor, self).__init__(shortname)


human_ctismer = HumanActor(
    shortname = "ctismer",
    fullname = "Christian Tismer",
    roles = ("developer", "programmer", "member"),
    email = "tismer@stackless.com",
    )

system = SystemActor("system")

# a few initial entries
actors = {}
for actor in (system, human_ctismer):
    sname = actor.shortname
    actors[sname] = actor


class Task(Persistent):
    """
    task.unique_id
        a unique identifier that exists only once in the world.
        The id is created at creation time.
        It is computed from the scanned tiff data.

    task.tasktype
        a classifier string that uniquely declares what the task is about.
        The type name of the task links to the type description which defines
        the transition graph for a task.

    task.status
        a classifier string that describes the status of the task.
        Initial status entries are "entry" and "exit".
        The transitions of the task are defined in the type of the task.

    task.object_id
        the identifier of the prescription to be operated on, or None.
        In case of None, the task operates on all prescriptions of the batch of
        the given database.

    task.assignee
        Identification of the actor that is supposed to perform the current action

    task.field_names
        the list of field names used in the task

    """

    def __init__(self, name, tasktype, object_id, status=Status("entry"), assignee=system):
        assert isinstance(name, str)
        self.name = name
        assert isinstance(tasktype, TaskType)
        self.tasktype = tasktype
        assert isinstance(status, Status)
        self.status = status
        assert isinstance(assignee, Actor)
        self.assignee = assignee

# just to have something to play with...

dummy_tasktype = TaskType(
    "this is the description of a task type that does nothing",
    )
dummy_task = Task(
    "just_a_dummy",
    dummy_tasktype,
    42,
    Status(),
    system,)
