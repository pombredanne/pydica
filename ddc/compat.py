# -*- coding: utf-8 -*-
from __future__ import division, absolute_import, print_function, unicode_literals

# compatibility between python 2.7 and 3.2/3.3

from six import *

# provide xrange for python 3 (a real omission)
if PY3:
    import builtins
    builtins.xrange = builtins.range
    del builtins

# the struct module does not allow unicode format strings in python2
# which conflicts with the unicode_literals definition.
if not PY3:
    def patch_struct():
        ''' patching all functions of struct, which all have "fmt" as first argument '''
        import struct, types, functools
        funcs = (f for f in struct.__dict__.values()
                 if isinstance(f, types.BuiltinFunctionType))
        for func in funcs:
            def container(func):
                @functools.wraps(func)
                def wrapper(fmt, *args, **kw):
                    # remark: this would require encoding= on python3,
                    # but here it is just a 'str' synonym.
                    if isinstance(fmt, unicode):
                        fmt = bytes(fmt)
                    args = (bytes(arg) if isinstance(arg, bytearray)
                            else arg for arg in args)
                    return func(fmt, *args, **kw)
                return wrapper
            setattr(struct, func.__name__, container(func))
    patch_struct()
    del patch_struct

    import copy_reg as copyreg
    sys.modules['copyreg'] = copyreg
    import __builtin__ as builtins
    sys.modules['builtins'] = builtins

# with_metaclass of six is way not strong enough for real use.
# here is one as a class decorator that works for us:

"""
About the usage of with_metaclass:
----------------------------------

The usage is limited to very simple classes. It is meant as a
way to inject a metaclass into a class hierarchy.
The real classes should then be defined on top of it.
In practice, this is a bit complicated and needs a bit of tweaking
and insertion of even more helper classes.

"""
