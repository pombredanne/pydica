# -*- coding: utf-8 -*-
from __future__ import division, absolute_import, print_function, unicode_literals

# basic test that the gui loads at least

import sys
import ddc.config.pyside_fix
from ddc import compat
from PySide import QtCore, QtGui
from ddc.client.val_app import CollectionViewer
from ddc.client.val_app.gui.prescription_viewer import PrescriptionViewer
# if we don't do this we crash :-(

def test_load_client1():
    app = QtGui.QApplication.instance() or QtGui.QApplication(sys.argv)
    viewer = PrescriptionViewer()

def test_load_client2():
    app = QtGui.QApplication.instance() or QtGui.QApplication(sys.argv)
    viewer = CollectionViewer()
