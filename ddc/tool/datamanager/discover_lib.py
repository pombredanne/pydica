# -*- coding: utf-8 -*-
from __future__ import division, absolute_import, print_function, unicode_literals

import os

from pathtools.patterns import match_path
import scandir

from ..storage.paths import ibf_subdir

__all__ = ['find_data_files', 'list_files']

def list_files(dir_pathname, recursive=True):
    for entry in scandir.scandir(dir_pathname):
        if entry.is_file():
            yield os.path.abspath(entry.path)
        elif recursive and entry.is_dir():
            for filename in list_files(entry.path, recursive=recursive):
                yield filename

def find_data_files(root_path, recursive=True):
    data_files = []
    for filepath in list_files(root_path, recursive=recursive):
        if not match_path(filepath, ('*.cdb', '*.ibf', '*.durus', '*.ask'),
                          case_sensitive=False):
            continue
        data_files.append(filepath)
    # even if we don't want to find files recursively we should still find ibf
    # files in the '00000001' subfolder.
    ibf_dir = os.path.join(root_path, ibf_subdir)
    if not recursive and os.path.exists(ibf_dir):
        for filepath in list_files(ibf_dir, recursive=False):
            if not match_path(filepath, ('*.ibf', '*.ask'), case_sensitive=False):
                continue
            data_files.append(filepath)
    return tuple(data_files)

