# -*- coding: utf-8 -*-
from __future__ import division, absolute_import, print_function, unicode_literals

import os
from unittest.case import TestCase

from ddt import ddt as DataDrivenTestCase, data
from pythonic_testcase import *

from ddc.tool.storage.paths import *

def _fix_path(path):
    return path.replace('/', os.path.sep)

@DataDrivenTestCase
class PathTest(TestCase):
    def test_path_info_from_cdb(self):
        # using os.path.join here so we can test also Windows-style paths if
        # running the tests on Windows
        cdb_dir = os.path.join('tmp', 'foo', 'bar')
        cdb_path = os.path.join(cdb_dir, 'abc.cdb')
        assert_equals((cdb_dir, 'abc'), path_info_from_cdb(cdb_path))

    def test_path_info_from_ibf(self):
        ibf_dir = os.path.join('tmp', 'foo', 'bar')
        ibf_path = os.path.join(ibf_dir, '00000001', 'abc.ibf')
        assert_equals((ibf_dir, 'abc'), path_info_from_ibf(ibf_path))
        assert_raises(Exception, lambda: path_info_from_ibf(os.path.join(ibf_dir, 'abc.ibf')))

    def test_path_info_from_durus(self):
        durus_dir = os.path.join('tmp', 'foo', 'bar')
        durus_path = os.path.join(durus_dir, 'abc.durus')
        assert_equals((durus_dir, 'abc'), path_info_from_durus(durus_path))

    def test_guess_cdb_path(self):
        cdb_dir = os.path.join('tmp', 'foo', 'bar')
        cdb_path = os.path.join(cdb_dir, 'abc.cdb')
        assert_equals(cdb_path, guess_cdb_path(cdb_dir, 'abc'))

    def test_guess_ibf_path(self):
        ibf_dir = os.path.join('tmp', 'foo', 'bar')
        ibf_path = os.path.join(ibf_dir, '00000001', 'abc.ibf')
        assert_equals(ibf_path, guess_ibf_path(ibf_dir, 'abc'))

    def test_guess_durus_path(self):
        durus_dir = os.path.join('tmp', 'foo', 'bar')
        durus_path = os.path.join(durus_dir, 'abc.durus')
        assert_equals(durus_path, guess_durus_path(durus_dir, 'abc'))

    def test_cdb_path_calculation_is_reversible(self):
        path_info = ('foo', 'bar.cdb')
        assert_equals(
            path_info,
            path_info_from_cdb(guess_cdb_path(*path_info))
        )

    def test_ibf_path_calculation_is_reversible(self):
        path_info = ('foo', 'bar.ibf')
        assert_equals(
            path_info,
            path_info_from_ibf(guess_ibf_path(*path_info))
        )

    def test_durus_path_calculation_is_reversible(self):
        path_info = ('foo', 'bar.durus')
        assert_equals(
            path_info,
            path_info_from_durus(guess_durus_path(*path_info))
        )

    @data('cdb', 'ibf', 'durus')
    def test_can_guess_bunch_with_path(self, missing):
        f = _fix_path
        bunch = DataBunch(cdb=f('/tmp/foo.CDB'), ibf=f('/tmp/00000001/FOO.ibf'),
                          durus=f('/tmp/Foo.durus'), ask=f('/tmp/00000001/FOO.ask'))
        noise = f('/foo.') + missing
        fn_map = {noise: noise}
        for key in bunch:
            if key != missing:
                fn_map[key.lower()] = key
        source_attr = getattr(bunch, missing)
        guessed_bunch = guess_bunch_from_path(source_attr, fn_map)
        assert_equals(bunch, guessed_bunch)


