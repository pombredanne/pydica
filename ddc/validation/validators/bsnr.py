# -*- coding: utf-8 -*-
from __future__ import division, absolute_import, print_function, unicode_literals

from .regex import RegexValidator


__all__ = ['BSNRValidator']

class BSNRValidator(RegexValidator):
    def __init__(self, *args, **kwargs):
        self.db = kwargs.pop('db', None)
        regex = '\d{9}'
        super(BSNRValidator, self).__init__(regex, *args, **kwargs)

    def messages(self):
        return {
            'unknown_bsnr': u'BSNR %(input_)s is unknown.',
        }

    def validate(self, value, context):
        super(BSNRValidator, self).validate(value, context)
        if self.db and (not self.db.contains(value)):
            self.raise_error('unknown_bsnr', value, context, input_=value)
