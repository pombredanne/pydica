# -*- coding: utf-8 -*-
from __future__ import division, absolute_import, print_function, unicode_literals

'''
css_module.py

This module comtainc a few CSS definitions to be used
in the Gui. It provides a basic mapping from widget
state to visual appearence.
'''

import ddc.config.pyside_fix
from PySide import QtCore, QtGui

lineedit_readonly = '''
QLineEdit {
     border: 1px solid lightgrey ;
}
'''

lineedit_normal = '''
QLineEdit {
     border: 1px solid lightgreen ;
}
QLineEdit:focus {
     border: 2px solid green ;
     /* margin: 2px ; */
}
'''

lineedit_warning = '''
QLineEdit {
     border: 1px solid orange ;
}
QLineEdit:focus {
     border: 2px solid yellow ;
     /* margin: 2px ; */
}
'''

lineedit_error = '''
QLineEdit {
     border: 1px solid darkred ;
}
QLineEdit:focus {
     border: 2px solid red ;
     /* margin: 2px ; */
}
'''

# this is for ediview, only
lineedit_highlight = '''
QLineEdit {
     border: 1px solid red ;
}
QLineEdit:focus {
     border: 2px solid red ;
     /* margin: 2px ; */
}
'''

class PaintProps(object):
    def __init__(self, name, stylesheet):
        self.name = name
        self.stylesheet = stylesheet

class LineEdit_Style(object):
    readonly = PaintProps('readonly', lineedit_readonly)
    normal = PaintProps('normal', lineedit_normal)
    warning = PaintProps('warning', lineedit_warning)
    error = PaintProps('error', lineedit_error)
    highlight = PaintProps('highlight', lineedit_highlight)

# XXX add a body that shows the different styles in a Gui
