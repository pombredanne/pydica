# -*- coding: utf-8 -*-
from __future__ import division, absolute_import, print_function, unicode_literals

import hashlib
import os
import sys
from io import BytesIO

from durus.connection import Connection
from durus.file_storage import FileStorage
from durus.persistent_dict import PersistentDict
from durus.persistent_list import PersistentList

from ddc.compat import string_types, PY3
from ddc.tool.storage.durus_.in_memory_storage import InMemoryStorage

def exit_if_not_py3():
    if PY3:
        return
    raise SystemExit('\n' + 72*'=' + '''\n\
    We are very sorry!
    ------------------
    Python version {} or any 2.X version can currently not be
    supported by Pydica, since this creates inconsistencies in
    the Durus databases when run in Python 3.X.
    Please use Python 3.X, only!\n'''.format(
        sys.version.split()[0]) + 72*'=')

__all__ = ['calculate_checksum', 'create_durus_fixture', 'DurusDB', 'DurusKey']

def create_durus_fixture(tasks=()):
    buffer_ = BytesIO()
    db = DurusDB(InMemoryStorage(buffer_))
    db._create_initial_structure()
    db.root[DurusKey.TASKS] = list(tasks)
    db.commit()
    db.close()
    buffer_.seek(0)
    return buffer_


def calculate_checksum(data_iter):
    hasher = hashlib.sha1()
    for data in data_iter:
        hasher.update(data)
    return hasher.hexdigest()


class DurusKey(object):
    TASKS = 'tasks'
    LOG = 'log'
    SNAPSHOT = 'snapshot'


class DurusDB(object):
    def __init__(self, storage):
        self.storage = storage
        self._connection = None

    @classmethod
    def create_new_db(cls, filename):
        assert not os.path.exists(filename)
        db = cls.init_with_file(filename, readonly=False, implicit_creation=True)
        db._create_initial_structure()
        return db

    @classmethod
    def init_with_file(cls, filename, readonly=False, implicit_creation=False):
        if isinstance(filename, string_types):
            if not implicit_creation:
                assert os.path.exists(filename)
            # protect a file-based database from being opened incompatible
            if not PY3:
                if getattr(sys, '_called_from_test', False):
                    pass
                else:
                    exit_if_not_py3()
            # NOTE: durus.File is racy when creating a new DB file as does not
            # perform an atomic "create if not exists" operation!
            storage = FileStorage(filename, readonly=readonly, repair=False)
        else:
            assert implicit_creation == False
            storage = InMemoryStorage(filename, readonly=readonly, repair=False)
        return DurusDB(storage)

    # --- accessing DB contents -----------------------------------------------
    @property
    def root(self):
        return self.connection.root

    # --- DB integrity --------------------------------------------------------
    def insert_snapshot(self, context, data_iter):
        snapshots = self.root[DurusKey.SNAPSHOT]
        snapshots[context] = calculate_checksum(data_iter)
    # LATER: add mechanism to verify a snapshot

    def _create_initial_structure(self):
        self.root.update({
            DurusKey.TASKS: PersistentList(),
            DurusKey.LOG: PersistentList(),
            DurusKey.SNAPSHOT: PersistentDict(),
        })

    # --- connection handling -------------------------------------------------
    @property
    def connection(self):
        if self._connection is None:
            # we only want a few objects in the cache but default is 100000
            cache_size = 20000
            self._connection = Connection(self.storage, cache_size=cache_size)
            # TODO: verify version number
        return self._connection

    @connection.setter
    def connection(self, value):
        old_connection = self._connection
        self._connection = value
        if old_connection and old_connection != self._connection:
            old_connection.storage.close()

    def commit(self):
        self._connection.commit()

    def rollback(self):
        if self._connection:
            self._connection.abort()
    abort = rollback

    def close(self):
        self.connection = None
        # make sure that the storage releases all locks even if the connection
        # was never established.
        self.storage.close()

    def save(self):
        self.connection.commit()
        self.close()

    def __del__(self):
        self.close()

    def insert_metadata(self, bunch):
        root = self.root
        # for simplicity of pickling, the most basic entries use no special classes
        dictype = dict # PersistentDict?
        root['.metadata'] = dictype(
            purpose = 'testing', # production, ...
            status = 'transitional', # preliminary, final, ...
            version = 1,
        )
        self.commit()

