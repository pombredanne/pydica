# -*- coding: utf-8 -*-
from __future__ import division, absolute_import, print_function, unicode_literals

import os
import ddc
from ddc.tool.cdb_tool import FormBatch
from ddc.tool.storage.paths import ibf_subdir
from private.srw.tool.walther.ascii import (
    AsciiExporter, AsciiFields
)

def default_output_name(cdb_filename):
    fpath = os.path.dirname(cdb_filename)
    subdir = os.path.join(fpath, ibf_subdir)
    base = os.path.basename(cdb_filename)
    name, ext = os.path.splitext(base)
    fname = os.path.join(subdir, name + '.asc')
    return fname

DEFAULT_INI = os.path.join(ddc.rootpath,
                           'private/srw/tool/walther/test/data/FIELDS.INI')

def export_ascii(cdb_filename, ascii_filename=None):
    if not ascii_filename:
        ascii_filename = default_output_name(cdb_filename)
    form_batch = FormBatch(cdb_filename, access='read', delay_load=False)
    fields = AsciiFields(DEFAULT_INI)
    exporter = AsciiExporter(form_batch, fields)
    exporter.generate_ascii_lines()
    with open(ascii_filename, 'w', encoding='cp850') as afile:
        for obj in exporter.ascii_lines:
            afile.write(str(obj) + '\n')
