# -*- coding: utf-8 -*-
from __future__ import division, absolute_import, print_function, unicode_literals

from contextlib import contextmanager
import logging
import os
import shutil
from tempfile import mkdtemp

from pycerberus.api import Validator
from pythonic_testcase import *

from ddc.lib.attribute_dict import AttrDict
from ddc.tool.storage import create_durus_fixture, Batch, DataBunch
from ddc.tool.storage.cdb.cdb_fixtures import create_cdb_with_form_values
from ddc.tool.storage.ibf.ibf_fixtures import create_ibf
from ddc.tool.storage.ask.ask_fixtures import create_ask
from ddc.tool.storage.testhelpers import set_durus_loglevel
from ddc.validation import PydicaSchema, ValidationModule
from ddc.validation.validators import DateValidator


@contextmanager
def use_tempdir():
    tempdir_path = mkdtemp()
    yield tempdir_path
    shutil.rmtree(tempdir_path)


def batch_with_form_data(form_data):
    nr_forms = len(form_data)
    databunch = DataBunch(
        cdb=create_cdb_with_form_values(form_data),
        ibf=create_ibf(nr_images=nr_forms),
        ask=create_ask(),
        durus=create_durus_fixture(),
    )
    return Batch.init_from_bunch(databunch, create_new_durus=False)

def fake_plugin(validators):
    schema = PydicaSchema()
    for key, validator in validators.items():
        schema.add(key, validator)
    plugin = AttrDict(
        validation_heuristics=lambda context: (),
        form_validation_schema=lambda context: schema,
        validation_post_processors=lambda context: (),
    )
    return plugin

def build_context(plugin=None, env_dir=None):
    if plugin is None:
        plugin = fake_plugin({'GEBURTSDATUM': DateValidator(reject_future_dates=True)})
    context = AttrDict(
        plugin_manager=AttrDict(
            activated_plugins={'fake': plugin}),
    )
    if env_dir:
        context['env_dir'] = env_dir
    return context


class ValidationModuleTest(PythonicTestCase):
    def setUp(self):
        super(ValidationModuleTest, self).setUp()
        set_durus_loglevel(logging.ERROR)

    def test_can_validate_form_values(self):
        batch = batch_with_form_data([{'GEBURTSDATUM': 'anything'}])
        vm = ValidationModule(batch, build_context())
        assert_length(0, batch.tasks())

        result = vm.process_values({'GEBURTSDATUM': '28.10.61'})
        assert_false(result.contains_errors())
        assert_length(0, batch.tasks())

        result = vm.process_values({'GEBURTSDATUM': '98.76.54'})
        assert_length(0, batch.tasks())
        assert_true(result.contains_errors())
        errors = result.errors
        assert_equals(('GEBURTSDATUM', ), tuple(errors))
        date_errors = errors['GEBURTSDATUM']
        assert_length(1, date_errors)
        assert_equals('bad_date', date_errors[0].key)

    def test_validators_can_access_context(self):
        class FakeValidator(Validator):
            def validate(self, value, context):
                assert_contains('foobar', context)

        plugin = fake_plugin({'GEBURTSDATUM': FakeValidator()})
        context = build_context(plugin=plugin)
        context['foobar'] = 'some value'
        batch = batch_with_form_data([{'GEBURTSDATUM': 'anything'}])
        vm = ValidationModule(batch, context)
        result = vm.process_values({'GEBURTSDATUM': '28.10.61'})
        assert_false(result.contains_errors())

    def test_can_pass_extra_context_items_per_form(self):
        state = AttrDict(calls=0)
        class FakeValidator(Validator):
            def validate(self, value, context):
                state['calls'] += 1
                assert_contains('foobar', context)

        plugin = fake_plugin({'GEBURTSDATUM': FakeValidator()})
        batch = batch_with_form_data([{'GEBURTSDATUM': 'anything'}])
        vm = ValidationModule(batch, build_context(plugin=plugin))
        values = {'GEBURTSDATUM': '28.10.61'}
        result = vm.process_values(values, extra_context={'foobar': 'baz'})
        assert_equals(1, state.calls)
        assert_false(result.contains_errors())

        # check that extra_context is actually what made it pass above
        result = vm.process_values(values)
        assert_equals(2, state.calls)
        assert_true(result.contains_errors(), message='"foobar" should not be present in validation context')

    def test_can_create_error_reports_for_unexpected_exceptions(self):
        class FakeValidator(Validator):
            def validate(self, value, context):
                raise ValueError('You shall not pass!')

        plugin = fake_plugin({'GEBURTSDATUM': FakeValidator()})
        batch = batch_with_form_data([{'GEBURTSDATUM': 'anything'}])

        with use_tempdir() as env_dir:
            context = build_context(plugin=plugin, env_dir=env_dir)
            vm = ValidationModule(batch, context)
            result = vm.process_values({'GEBURTSDATUM': '28.10.61'})
            assert_true(result.contains_errors(), message='Fakevalidator did not raise its error message')

            exception_dir = os.path.join(env_dir, 'exceptions')
            assert_true(os.path.exists(exception_dir),
                message='expected exception dir %s not found' % exception_dir)
            self.assert_exception_report_exists(exception_dir)

    def assert_exception_report_exists(self, exception_dir):
        for root_dir, directories, files in os.walk(exception_dir):
            for filename in files:
                path = os.path.join(root_dir, filename)
                if not path.lower().endswith('.txt'):
                    continue
                return path
        self.fail('no failure report found below "%s"' % exception_dir)
