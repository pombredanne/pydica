Projektplan
===========

Dies sind Überlegungen aus dem Flug nach USA. Nicht mehr ganz aktuell
und vermutlich etwas zu kompliziert gedacht.

Stichworte und Kommentare:
--------------------------

- CSS Struktur
  Abkehr von CSS-Tricks
  CSS - Modul mit eingebetteter Demo-App die das anzeigt
  Introspection-Tricks moeglich aber nicht notwendig -> weg damit

- Fields-INI
  Analyse ist notwendig
  Verifikation anhand vorhandener Daten
  - die einzelnen Felder muessen verifizierbar erkannt werden
  
Update 05.04.12:
  Struktur vereinfacht, Reduktion auf zunaechst das Noetigste.

Eingrenzung
-----------

Die Aufgabenstellung ist implizit schon klar, nicht klar ist der zur Umsetzung
notwendige Aufwand. Um einen eindeutigen Einstieg zu haben, zunaechst Reduktion
auf Attribute "LEFT, TOP, RIGHT, BOTTOM".
Diese Attribute sind erstmal genug um die Positionen zu finden, und es soll sich
verifizieren lassen, dass die Sonderfaelle "0 1 1 0" korrekt aus Datei APO_FSRH.C
rekonstruiert werden koennen.
Erschwerend kommt hinzu, dass dazwischen ein Koordinaten-Wechsel stattfindet.
Seiteneffekt: Die Korrektheit der TIFF-Abmessungen wird dadurch leider wichtig.
Anmerkung: Die Messung ab linker unterer Ecke ist typisch durch Tiff entstanden.
Offensichtlich wurde APO-FSRH.C bereits vor der Adoption von Tiff konzipiert.

FSEARCH.INI (siehe Ende von README.txt) scheint unklar zu sein bezueglich der Frage,
was wofuer wie ausgewertet wird. Es ist unklar, ob die vorhandene Analyse ausreicht,
oder ob zur endgueltigen Klaerung eine Quellcodeanalyse notwendig ist.

Ich schlage vor, den eingeschlagenen Heuristik-Weg fortzusetzen, solange die dabei
ermittelten Ergebnisse plausibel sind. Sollten dabei Dinge unklar bleiben, schlage
ich eine Totalanalyse vor, die aber aufwendig ist. Zunaechst verfolge ich den
optimistischen Heuristik-Ansatz.

Update 05.04.12:
  Einfache, pragmatische Vorgehensweise

Ziele des Projektplans
----------------------

Generell ist ein totaler Ersatz der existierenden Lösung gewünscht. Gleichzeitig
sollen die vorhandenen MA vom Neuansatz ueberzeugt werden. Um moeglichst viel
fruehzeitigen Feedback zu erlangen, sollen die ersten Schritte gleichzeitig zu
- Extraktion und Validierung der Feldpositionen fuehren
- vorhandene Scans mit richtiger Position dargestellt werden
- das GUI ansprechend und korrekt die Felder darstellen.

Definition Cursorposition
--------------------------

Es gibt einen Cursor, der sich in einem Feld befindet. Das klingt zunaechst trivial,
bedeutet aber mehr:
Der Cursor ist virtuell, d.h. er befindet sich von der Logik her in einem Feld, um das es
gerade geht. Physikalisch bedeutet es aber mehr, weil der Bildschirm gleichzeitig
verschiedene Darstellungen besitzt.

- Im linken Fenster wird das aktuelle Feld so exakt wie moeglich durch zarte farbliche
  Kennzeichnung markiert. Editieren erfolgt dabei nicht. Bei Cursorbewegung wandert
  diese Markierung entsprechend, und die Darstellung wird geaendert.
- Im rechten Feld erscheint ein Eingabefenster, welches sich relativ nahe an der linken
  Darstellung orientiert, aber mit realen LineEdit-Controls realisiert ist. Diese Controls
  erlauben eine Texteingabe und sind der Darstellung im linken Bereich weitgehend, aber nicht
  zwingend angepasst. Hier wird eine entsprechende Rundung, Vereinfachung und Rasterisierung
  vorgenommen, sodass ein bestmoeglicher Kompromiss zwischen Darstellung und Eingabemoeglichkeit
  erzielt wird.

Update 05.04.12:
  Zunächst nur eine Cursorposition, siehe Tasten.txt

Vorschlag Arbeitspakete und Zeitschaetzung
--------------------------------------------

- CSS-Modul
  Das bisher gewonnene Qt-Wissen schlaegt sich in einem eigenen CSS-Modul nieder, welches
  ohne Introspection-Tricks die Formatierung der verschiedenen Feldklassen definiert.
  CSS in der Applikation wird ignoriert und existiert nur zur Darstellung des Prototypen.
  Die relevanten Controls werden mit Hilfe der Koordinaten-Information und dem CSS-Modul
  dynamisch erzeugt.

- INI-Analyse erster Teil
  Die Ini-Datei wird ausgelesen und fehlende Werte aus APO-FSRH.C ergaenzt. Die Struktur wird
  zunaechst nur im Speicher erzeugt; solange diese Struktur nicht vollstaendig klar ist, halte
  ich extra-Dateien fuer verfrueht, zumal der Rechenaufwand gering ist.

- Anzeige der Felder im Original (z.Zt. Links)
  Die Felder werden durch die ermittelten Feldpositionen im Originalbild markiert, auch zur
  Validierung und als positives Feedback. Die Markierung ist nur zart und nicht stoerend. Die
  Anzeige erfolgt nur, wenn sich der Cursor im entsprechenden virtuellen Feld befindet.

- Editieren der Felder als Eingabe (z.Zt. Rechts)
  Gleichzeitig mit der zarten Felddarstellung (links) erfolgt eine Darstellung aller Felder
  zum Editieren (rechts), wobei ein Edit-Cursor zwischen den Feldern bewegt wird. Das aktuelle
  Feld zeigt dabei durch CSS seinen Editier-Zustand durch dass CSS-Modul an. Je nach Tastendruck
  folgt der Cursor dabei der definierten Reihenfolge. Durch Enter wird eine logisch durch die
  Felddefinition festgelegte Reihenfolge eingehalten. Durch Cursor-Tasten (Auf/Ab) wird das
  raeumlich naechstgelegene Feld angesteuert. Durch Mausklich wird ein Feld direkt angewaehlt.

  Anmerkungen 09.04.2012 von Daniel:

  - Die Cursor-Tasten sollten nicht die räumlich naechstgelegenen Felder ansteuern, sondern
    genau wie Enter das vorhergehende bzw. nachfolgende logische Feld. Was das "räumlich
    naechstgelegende" Feld ist, waere naemlich erst noch zu definieren ist meiner Ansicht nach
    nicht so ganz trivial und wird bei uns erst einmal nicht gebraucht.

  - Der Punkt "durch Mausklick wird ein Feld direkt angewaehlt" ist momentan sicherlich recht
    einfach umzusetzen, wenn dann aber spaeter mal die Logik des Validators ins Spiel kommt,
    ist die Aufgabe der freien Feldauswahl mit der Maus alles andere als trivial, so zumindest
    meine bisherigen Erfahrungen auf diesem Gebiet. Die freie Positionierung mit der Maus wird
    momentan zum Ersetzen des aktuellen Programms bei uns nicht dringend benoetigt.

  Update 05.04.12:
  Doppelte Reihenfolge weggelassen, siehe Tasten.txt

- Abspeicherung bei Feldwechsel
  Der Zustand eines Feldes wird bei Feldwechsel grundsaetzlich gespeichert. Alle Aenderungen
  werden grundsaetzlich in einer Session festgehalten. Ungeklaert ist noch, wie lange Sessions
  existieren sollen und wie lange sie offengehalten werden. Vorschlag ist erstmal, dass man sich
  in einem noch zu definierenden Batch befindet, den man irgendwann als beendet "abschliesst".
  Gespeichert wird die Arbeit aber auf jeden Fall erst einmal.

  Update 05.04.12:
  Batch ist durch Daniel definiert, grundsaetzlich eine gescannte Gruppe.

- Wechsel zwischen Dateien
  Beim Verlassen des logisch letzten Feldes wird die naechste Datei geladen und angezeigt. Dies
  wird in der Statusanzeige entsprechend kenntlich gemacht. Das Laden und Speichern von Dateien
  geschieht implizit, der Benutzer "bewegt" sich frei innerhalb eines Batches, das noch zu
  definieren ist. Vorerst gibt es ein Batch als Menge von Dateien, als einfacher Ansatz.
  
  Update 05.04.12:
  Siehe Oben, sowie einfacher Datei-Ansatz, testen ob Cache noetig.

- Lesen von TIFF-Dateien
  Die zugrundeliegenden Tiff-Dateien sind leider etwas komplexer als erwartet und muessen
  verarbeitbar gemacht werden. Dazu muessen existierende Tools und Biobliotheken analysiert und auf
  Verwendbarkeit geprueft werden. Im Zweifelsfalle favorisiere ich den dynamischen Ansatz, also
  Zugriff ueber ein zu schreibendes tiff-Modul, solange der Rechenaufwand klein ist.
  
  Update 05.04.12:
  Benutzen von TiffCP, externer Prozess.
