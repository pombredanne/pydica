# -*- coding: utf-8 -*-
from __future__ import division, absolute_import, print_function, unicode_literals

from datetime import date as Date

from .regex import RegexValidator


__all__ = ['DateValidator']

class DateValidator(RegexValidator):
    def __init__(self, **kwargs):
        self.reject_future_dates = kwargs.pop('reject_future_dates', False)
        default_pattern = '(?P<day>\d{2})\.(?P<month>\d{2})\.(?P<year>(?:\d{2}|\d{4}))'
        regex = kwargs.pop('regex', default_pattern)
        super(DateValidator, self).__init__(regex, use_match_for_conversion=True, **kwargs)

    def messages(self):
        return {
            'bad_date': u'The date %(input_)s is invalid.',
            'future_date': u'Dates in the future are not allowed.',
        }

    def convert(self, value, context):
        match = super(DateValidator, self).convert(value, context)
        match_groups = match.groupdict()
        day = int(match_groups['day'])
        month = int(match_groups['month'])
        year = int(match_groups['year'])

        if len(str(year)) <= 2:
            year_with_millenium = year + 2000
        else:
            year_with_millenium = year

        try:
            Date(year_with_millenium, month, day)
        except ValueError:
            self.raise_error('bad_date', value, context, input_=value)
        if self.reject_future_dates and (year > 2000) and (Date(year, month, day) > Date.today()):
            self.raise_error('future_date', value, context, input_=value)
        return (year, month, day)

