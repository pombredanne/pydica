# ./conftest.py in your root or package dir

# forget about the auxiliary folders
collect_ignore = ['build', 'lib', 'srw/tool']

'''
It is usually a bad idea to make your code behave differently
when the tests are run. But there are exceptions like this one:

Durus is creating incompatible pickles when run in Python 2.
So we do not want to run Python 2 on the Pydica apps right now.
On the other hand, Python 2 is still not completely abandoned, yet.
That means: We still want to allow to run all the tests in Python 2.
To solve this conflict, the variable "sys._called_from_test"
is needed.
'''

def pytest_configure(config):
    import sys
    sys._called_from_test = True

def pytest_unconfigure(config):
    import sys  # This was missing from the manual
    del sys._called_from_test