# -*- coding: utf-8 -*-
from __future__ import division, absolute_import, print_function, unicode_literals

import os, sys
import ddc
from ddc.tool.cdb_collection import CDB_Collection
from ddc.platf.platform_quirks import is_windows
import pytest

DATABASE_PATH = os.path.join(ddc.rootpath, 'private', 'srw', 'data')
DEMO_SEQ = os.path.join(DATABASE_PATH, '00955412.CDB')

# mmap behaves different on windows, when access_copy is specified.
# flush() then reports an error.
# We therefore use 'access_write' on windows.

@pytest.fixture(scope="module",
                params=['write', 'copy'])
def access(request):
    print(request.param)
    return request.param

# trivial test for now to check the imports
def test_open_sequence(access):
    seq = CDB_Collection(access=access)
    seq.close()

def test_delete(access):
    seq = CDB_Collection(DEMO_SEQ, access=access)
    seq.seek(1)
    form = seq.current_form
    form.delete()
    seq.seek(0)
    form = seq.current_form
    form.undelete()
    seq.seek(1)
    form = seq.current_form
    assert form.is_deleted()

    seq.close()

    seq = CDB_Collection(DEMO_SEQ, access=access)
    forms = seq.forms
    # direct interface
    forms[1].delete()
    assert forms[1].is_deleted()
    codnr = forms[1].pic_nr
    assert codnr != 'DELETED'
    assert forms[1]._form_header.rec.imprint_line_short == 'DELETED'

    forms[0].delete()
    assert forms[0].is_deleted()
    codnr = forms[0].pic_nr
    assert forms[0]._form_header.rec.imprint_line_short == 'DELETED'
    seq.close()

def test_undelete(access):
    seq = CDB_Collection(DEMO_SEQ, access=access)
    seq.seek(1)
    form = seq.current_form
    form.delete()
    assert form.is_deleted()
    form.undelete()
    assert not form.is_deleted()
    seq.close()
