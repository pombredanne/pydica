# -*- coding: utf-8 -*-
from __future__ import division, absolute_import, print_function, unicode_literals

# basic test to see if the gui is able to open an image.
# PySide seems to implement QtCore and QtGui basis alone.
# No error message when you try to load an image :-(

import os, sys

import ddc.config.pyside_fix
from PySide import QtGui

this_dir = os.path.dirname(__file__)
tiffpath = os.path.join(this_dir, '1bpp.tiff')
nopath = 'nonexistent.tiff'

def test_load_image():
    assert os.path.exists(tiffpath)
    image = QtGui.QImage(tiffpath)
    assert not image.isNull(), 'I think Qt is not correctly installed'

def test_load_noimage():
    assert not os.path.exists(nopath)
    image = QtGui.QImage(nopath)
    assert image.isNull()

# testing the Gui toolkit a bit more

import ddc.config.pyside_fix # initializes and checks PySide

def test_toolkit_base():
    import PySide.QtCore

def test_toolkit_gui():
    import PySide.QtGui
