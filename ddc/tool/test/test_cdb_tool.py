# -*- coding: utf-8 -*-
from __future__ import division, absolute_import, print_function, unicode_literals

import io
import os.path
import pytest
import ddc
from ddc.compat import PY3
import tempfile

from ddc.tool.cdb_tool import (MMapFile, ImageBatch, TiffHandler,
                               image_filename)
from ddc.platf.platform_quirks import is_windows

# mmap behaves different on windows, when access_copy is specified.
# flush() then reports an error.
# We therefore use 'access_write' on windows.

@pytest.fixture(scope="module",
                params=['write', 'copy'])
def access(request):
    print(request.param)
    return request.param

class TestMMapFile(object):
    def setup(self):
        self.temp_fname = tempfile.mktemp()
        testfile = io.open(self.temp_fname, 'wb')
        testfile.write(b'hallo')
        testfile.close()

    def teardown(self):
        os.unlink(self.temp_fname)

    def test_interface(self, access):
        mm = MMapFile(self.temp_fname, 'write')
        assert mm[:] == b'hallo'
        mm.flush()
        if PY3:
            mm[3] = ord(b'X')
        else:
            mm[3] = b'X'
        mm[4:5] = mm[:1]
        mm.close()
        assert mm.closed
        with pytest.raises(ValueError):
            mm[0] = 0
        assert open(self.temp_fname).read() == 'halXh'

# XXX add some tests for images. They are completely missing, and cdb_tool
# has the potential to hang on bad image files.

DATABASE_PATH = os.path.join(ddc.rootpath, 'private', 'srw', 'data')
CDB_NAME = '00099201.CDB'

def test_tiff_access(access):
    fname = image_filename(os.path.join(DATABASE_PATH, CDB_NAME))
    imbatch = ImageBatch(fname, access=access)
    th = TiffHandler(imbatch, 0)
    assert th.ifd.rec.num_tags == 27
    assert th.long_data.rec.document_name == 'REZEPT'
    assert th.ifd2.rec.num_tags == 27
    assert th.long_data2.rec.document_name == 'REZEPT'

def test_tiff_write(access):
    fname = image_filename(os.path.join(DATABASE_PATH, CDB_NAME))
    imbatch = ImageBatch(fname, access=access)
    th = TiffHandler(imbatch, 0)
    th.long_data.update_rec(page_name = 'DELETED')
    th.update()
    th = TiffHandler(imbatch, 1)
    assert th.long_data.rec.page_name != 'DELETED'
    th = TiffHandler(imbatch, 0)
    assert th.long_data.rec.page_name == 'DELETED'
    undone = th.long_data2.rec.page_name
    th.long_data.update_rec(page_name = undone)
    th.update()
