
Pydica Network Considerations
=============================

We assume a network layout with a cluster of N clients, which also
can work as a server. The N clients of a cluster share a common BTSync secret.

When any client/server accesses a FileStorage in read/write mode, then the
file is exclusively locked on the locking instance.
Changes to the Durus file are not propagated until the file is closed or
re-opened in read mode.

The synchronization only starts after a file becomes readable. It then takes
up to 20 seconds until the synchroniyation starts.

Things to be tested:
What happens when the file becomes locked during a synchronization?

Server Handling
---------------

Because the delay of the sync is quite large, we need to establish a server
connection. The server can be any client that is running as a (what, exactly?).

The exact server does not matter at all. Also, the exact database is not so relevant.
The database should be the one that has the most current version.
The server running that database would probably be a local server that has the
most current DB, but if there is an easy way to access and lock a remote DB like a
file, any client could take the role of the server.

Every cluster machine puts its current connection info into a file in the shared folder.
Every cluster machine also checks the connectivity on the other machines. (How?)
