Abstract Preliminary Relational Interface Layer
===============================================

This is a couple of random thoughts, crammed together after a fair bit of hacking on the CDB.

Database Abstraction
--------------------

Databases are widely used. Everybody needs them, nobody likes them, and
there will never be a perfect solution.

Instead of trying to solve this, we do an abstract solution by providing
a way to describe a particular database by its properties in an unspecified
abstract property language.

The goal is to describe the currenty existing simple database scheme with a few
properties and to make the existing implementation abstract enough to
implement one for a different database system.

As far as this current approach goes, it just tries to describe the relevant
properties of the database. This should be re-iterated when we get more examples
of databases.

We use a simple, preliminary notation of .INI files.::

    [DEFAULTS]
    active-db           : RDB/CDB demo
    descriptive-section : [concrete APRIL]
    
    [concrete APRIL]
    customer            : Stein-Reichwald GmbH
    vendor              : Walther GmbH
    version             : unknown
    dbtype              : RDB/CDB
    meta-data           : [database meta-data]
    
    [database meta-data]
    type                : RDB/CDB
    constraints         : [rdb-constraints]
    db-properties       : [rdb-properties]
    sys-interface       : [system interface]
    
    [rdb-properties]
    sql-like            : no
    sql-interface       : no
    dbase-like          : yes
    dbase-interface     : no
    object-like         : no
    object-interface    : no
    
    [system interface]
    native-os          : windows
    access-type        : [rdb-files]
    network-layer      : novell netware
    network-speed      : [novell-netware-speed]
    
    [rdb-files]
    files-per-rdb      : 2
    file-path-1        : {ident}.CDB
    file-path-2        : 00000001/{ident}.IBF
    file-type-1        : rdb-form-batch
    file-type-2        ; rdb-image-batch
    
    [novell-netware-speed]
    open               : fair
    close              : fair
    read               : slow
    write              : slow
    
    [rdb-form-batch]
    batch-size         : varying
    batch-header-size  : fixed
    batch-layout       : [rdb-forms]
    
    [rdb-image-batch]
    batch-size         : varying
    batch-header-size  : fixed
    batch-layout       : [rdb-images]
    
    [rdb-form]
    form-header-size   : fixed
    form-header-funcs  : (compute_form_header, compute-form-size)
    form-size          : fixed
    form-size-compute  : function(...)
    form-attribute     : [rdb-form-field]
    
    [rdb-form-field]
    field-size         : fixed
    field-count        : fixed
    field-names        : fixed
    field-properties   : [rdb-field-attribute]
    field-encodings    : (cp850)
    
    [rdb-field-attribute]
    attribute-count    : fixed
    attribute-type     : string, int32
    attribute-size     : fixed(attribute-name)
    encoding           : (cp850)
    
    [rdb-image]
    image-header-size  : fixed
    image-header-funcs : (compute-image-header, compute-image-size)
    image-size         : varying
    image-size-compute : function(...)
    image-attribute    : [rdb-form-field]
    image-format       : fax G3/G4
    
