# -*- coding: utf-8 -*-
from __future__ import division, absolute_import, print_function, unicode_literals

'''
Layout Algorithms
-----------------

These functions are independent from the gui toolkit.
They simply calculate field positions and overlaps.

field_list:

A structure of fields with no further definition but
properties

    x, y, w, h
    rect
    field_name

as defined in ddc.client.config.config_base.
Coordinates are calculated from top to bottom (Qt coordinates).

Duck typing is a friend of us!
'''

DEBUG = False

def kill_duplicates(fl):
    '''
    find duplicates in field list fl and remove them.
    '''
    dd = {}
    for field in fl:
        key = field.rect[:2]
        dups = dd.get(key, [])
        dups.append(field)
        dd[key] = dups
    for key, dups in list(dd.items()):
        if len(dups) > 1:
            for dup in dups:
                if DEBUG:
                    print('duplicates removed:', dup.field_name, key)
                fl.remove(dup)

def remove_overlaps(fl):
    '''
    Simple semi-brute-force algorithm with a few assumptions.
    We want to find overlaps in rows. We also assume that
    overlaps are small, and there are no fields crossing
    two rows.
    The rects are ordered by 'y' and then 'x'. When the 'y'
    distance exceeds twice a typical row height, we can stop searching.
    '''
    ordered = []
    for field in fl:
        x, y = field.x, field.y
        ordered.append((y, x, field))
    ordered.sort()
    ordered = [field for y, x, field in ordered]
    # now walk the fields and find overlaps
    for i, f in enumerate(ordered):
        for o in ordered[i+1:]:
            if o.y > f.y + 2*f.h:
                # already out of reach
                break
            if o.x > f.x + f.w:
                # to the right of me
                continue
            if o.x + o.w < f.x:
                # to the left of me
                continue
            if o.y >= f.y + f.h - 3: # small tolerance
                # below me
                continue
            # ok, we might have a candidate
            if DEBUG:
                print('scan', i, f.field_name, f.rect, o.field_name, o.rect)

            # first change: Adjust width
            if f.x <= o.x <= f.x + f.w:
                # cut my field
                f.w = o.x - f.x - 1
                if DEBUG:
                    print('shortened', f.field_name, 'to', f.w)
            elif o.x <= f.x <= o.x + o.w:
                # cut other field
                o.w = f.x - o.x - 1
                if DEBUG:
                    print('shortened', o.field_name, 'to', o.w)
            else:
                raise ValueError('this is a bug')

    ordered = []
    for field in fl:
        x, y = field.x, field.y
        ordered.append((x, y, field))
    ordered.sort()
    ordered = [field for x, y, field in ordered]
    # now walk the fields and find overlaps
    for i, f in enumerate(ordered):
        for o in ordered[i+1:]:
            if o.x > f.x + 2*f.w:
                # already out of reach
                break
            if o.y > f.y + f.h:
                # far below me
                continue
            if o.y + o.h < f.y:
                # far above me
                continue
            if o.x >= f.x + f.w - 3: # small tolerance
                # right to me
                continue
            # ok, we might have a candidate
            if DEBUG:
                print('scan', i, f.field_name, f.rect, o.field_name, o.rect)

            # second change: Adjust height
            if f.y <= o.y <= f.y + f.h + 1:
                # cut my field
                f.h = o.y - f.y - 2
                if DEBUG:
                    print('reduced line height', f.field_name, 'to', f.w)
            elif o.y <= f.y <= o.y + o.h + 1:
                # cut other field
                o.h = f.y - o.y - 2
                if DEBUG:
                    print('reduced line height', o.field_name, 'to', o.w)
            else:
                raise ValueError('this is a bug')
