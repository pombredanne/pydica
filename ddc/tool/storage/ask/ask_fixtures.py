# -*- coding: utf-8 -*-
"""
ASK Fixtures can generate ASK binary structures completely in memory for
testing purposes.
"""
from __future__ import division, absolute_import, print_function, unicode_literals

from ddc.dbdef import cdb_definition
from ddc.tool.storage.fixture_helpers import BinaryFixture, UnclosableBytesIO


__all__ = ['create_ask']

def create_ask():
    return UnclosableBytesIO(b'')

