# -*- coding: UTF-8 -*-
#
# The MIT License
#
# Copyright (c) 2011 Felix Schwarz <felix.schwarz@oss.schwarz.eu>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
#
# I believe the license above is permissible enough so you can actually
# use/relicense the code in any other project without license proliferation.
# I'm happy to relicense this code if necessary for inclusion in other free
# software projects.

from datetime import date


__all__ = ['Period']

class Period(object):
    def __init__(self, start, end=None):
        self.start = start
        self.end = end
        if (self.end is not None) and (self.start > self.end):
            raise AssertionError('Start (%s) must be before end Ende (%s)' % (self.start, self.end))

    def contains(self, a_date):
        if a_date < self.start:
            return False
        elif self.end and (self.end < a_date):
            return False
        return True

    def __contains__(self, a_date):
        # enables 'date(...) in period'
        return self.contains(a_date)

    def contains_completely(self, period):
        if not self.contains(period.start):
            return False
        elif period.end and not self.contains(period.end):
            return False
        return True

    def overlaps(self, period):
        return self.contains(period.start) or self.contains(period.end) \
            or period.contains_completely(self)

    def __repr__(self):
        return 'Period(%s, %s)' % (self.start, self.end)

    def __ne__(self, other):
        return not (self == other)

    def __eq__(self, other):
        if not isinstance(other, Period):
            return False
        return (self.start == other.start) and (self.end == other.end)


