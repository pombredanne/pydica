# -*- coding: utf-8 -*-
from __future__ import division, absolute_import, print_function, unicode_literals

from unittest import TestCase

from pycerberus.api import Validator
from pycerberus.validators import IntegerValidator, StringValidator
from pythonic_testcase import *

from ddc.lib.attribute_dict import AttrDict
from ddc.validation import FormValidation, HeuristicFixer, PydicaSchema


class FormValidationTest(TestCase):
    def test_returns_no_errors_for_valid_data(self):
        schema = self._init_schema()
        form = dict(foo='123', bar='234')
        result = FormValidation(schema).run(form)
        assert_false(result.contains_errors())
        # We always return string data
        assert_equals(dict(foo='123', bar='234'), result.value)
        assert_equals(form, result.initial_value)

        assert_equals(234, result.bar.meta)
        assert_equals({'foo': '123', 'bar': 234}, result.meta)

    def test_ignores_unknown_fields(self):
        schema = self._init_schema()
        form = dict(foo='123', bar='234', unknown='anything')
        result = FormValidation(schema).run(form)
        assert_false(result.contains_errors())
        assert_equals(dict(foo='123', bar='234'), result.value)
        assert_equals(dict(foo='123', bar='234'), result.initial_value)

    def test_returns_errors_by_field_for_validation_errors(self):
        schema = self._init_schema()
        form = dict(foo='long text', bar='abc')

        result = FormValidation(schema).run(form)
        errors = result.errors
        assert_equals(set(['foo', 'bar']), set(errors))
        assert_equals(('too_long',), self._error_keys(errors['foo']))
        assert_equals(('invalid_number',), self._error_keys(errors['bar']))
        assert_equals(dict(foo=None, bar=None), result.value)
        assert_equals(form, result.initial_value)


    # --- post processors -----------------------------------------------------
    def test_can_apply_postprocessors(self):
        schema = self._init_schema()
        form = dict(foo='long text', bar='abc')
        class FooIsWarning(object):
            def run(self, validation_result, context):
                errors = validation_result.errors
                errors['foo'][0]['is_warning'] = True
                return validation_result
        result = FormValidation(schema, post_processors=(FooIsWarning(), )).run(form)
        assert_true(result.contains_errors())
        errors = result.errors
        assert_contains('foo', errors)
        foo_error = errors['foo'][0]
        assert_true(foo_error.get('is_warning'))

    def test_postprocessors_can_remove_errors(self):
        schema = self._init_schema()
        class BarIsWarning(object):
            def run(self, validation_result, context):
                errors = validation_result.errors
                if errors['bar']:
                    bar_error = errors['bar'][0]
                    bar_error['is_warning'] = True
                return validation_result
        shortener = self._foo_shortener_heuristics()
        leet_speak = self._leet_speak_heuristics()
        validation = FormValidation(schema,
            heuristics=(shortener, leet_speak),
            post_processors=(BarIsWarning(),)
        )

        result = validation.run(dict(foo='foo', bar='lOS'))
        assert_false(result.contains_errors(), message='leet_speak heuristic should be able to resolve bar warning')

        # heuristics should not influence warnings for unrelated fields
        result = validation.run(dict(foo='foobar', bar='invalid'))
        assert_contains('bar', result.errors)
        bar_error = result.errors['bar'][0]
        assert_true(bar_error['is_warning'],
            message='Foo heuristic should not touch a bar warning and the bar heuristic doesn\'t help here')


    # --- heuristics ----------------------------------------------------------

    def test_heuristics_can_resolve_warnings(self):
        schema = self._init_schema()
        form = dict(foo='bar', bar='1234')
        shortener = self._foo_shortener_heuristics()
        result = FormValidation(schema, heuristics=(shortener, )).run(form)
        assert_false(shortener.was_called)
        assert_false(result.contains_errors())
        assert_equals(dict(foo='bar', bar='1234'), result.value)

    def test_passes_context_when_validating_suggestion_from_heuristic(self):
        state = AttrDict(calls=0)
        class FakeValidator(StringValidator):
            def validate(self, value, context):
                assert_contains('foobar', context)
                state['calls'] += 1
                super(FakeValidator, self).validate(value, context)

        schema = self._init_schema()
        schema.add('foo', FakeValidator(max_length=3))

        form = dict(foo='barbaz', bar='1234')
        shortener = self._foo_shortener_heuristics()
        result = FormValidation(schema, heuristics=(shortener, )).run(form, context={'foobar': ''})
        assert_true(shortener.was_called)
        assert_equals(2, state.calls,
            message='FakeValidator (which ensures the context is passed) was not called 2 times')
        assert_false(result.contains_errors())
        assert_equals(dict(foo='bar', bar='1234'), result.value)

    def test_can_apply_heuristics_after_failed_validation(self):
        schema = self._init_schema()
        form = dict(foo='very long', bar='1234')
        result = FormValidation(schema).run(form)
        assert_true(result.contains_errors())
        assert_contains('foo', result.errors)

        shortener = self._foo_shortener_heuristics(max_length=2)
        result = FormValidation(schema, heuristics=(shortener, )).run(form)
        assert_true(shortener.was_called)
        assert_false(result.contains_errors())
        assert_equals(dict(foo='ve', bar='1234'), result.value)

    def test_does_not_apply_heuristics_on_successful_validation(self):
        schema = self._init_schema()
        form = dict(foo='bar', bar='1234')
        shortener = self._foo_shortener_heuristics()
        result = FormValidation(schema, heuristics=(shortener, )).run(form)
        assert_false(shortener.was_called)
        assert_false(result.contains_errors())
        assert_equals(dict(foo='bar', bar='1234'), result.value)

    def test_discards_heuristic_if_does_not_fix_the_error(self):
        schema = self._init_schema()
        form = dict(foo='very long', bar='1234')
        shortener = self._foo_shortener_heuristics(max_length=4)
        result = FormValidation(schema, heuristics=(shortener, )).run(form)
        assert_true(result.contains_errors())
        assert_true(shortener.was_called)
        assert_equals(dict({'foo': None, 'bar': '1234'}), result.value)

    def test_can_handle_heuristic_not_returning_a_suggestion(self):
        schema = self._init_schema()
        form = dict(foo='very long', bar='1234')
        noop = self._noop_heuristics('foo', error_key='too_long')
        result = FormValidation(schema, heuristics=(noop, )).run(form)
        assert_true(result.contains_errors())
        assert_true(noop.was_called)
        assert_equals(dict({'foo': None, 'bar': '1234'}), result.value)

    def test_only_calls_heuristics_if_error_conditions_match(self):
        schema = self._init_schema()
        form = dict(foo=None, bar='1234')
        noop = self._noop_heuristics('foo', error_key='too_long')
        result = FormValidation(schema, heuristics=(noop, )).run(form)
        assert_true(result.contains_errors())
        assert_false(noop.was_called)

    def test_applies_heuristic_even_if_other_errors_are_present(self):
        schema = self._init_schema()
        form = dict(foo='very long', bar='invalid')
        shortener = self._foo_shortener_heuristics(max_length=2)

        result = FormValidation(schema, heuristics=(shortener, )).run(form)
        assert_true(shortener.was_called)
        assert_true(result.contains_errors())
        assert_equals(form, result.initial_value)
        assert_equals(dict(foo='ve', bar=None), result.value)
        assert_equals((), result.errors['foo'])
        assert_length(1, result.errors['bar'])
        bar_error = result.errors['bar'][0]
        assert_equals('invalid_number', bar_error['key'])

    def test_can_apply_multiple_heuristics(self):
        schema = self._init_schema()
        form = dict(foo='very long', bar='lOS')
        shortener = self._foo_shortener_heuristics(max_length=2)
        leet_speak = self._leet_speak_heuristics()

        result = FormValidation(schema, heuristics=(shortener, leet_speak)).run(form)
        assert_true(shortener.was_called)
        assert_true(leet_speak.was_called)
        assert_false(result.contains_errors())
        assert_equals(dict(foo='ve', bar='105'), result.value)

    def test_stops_applying_heuristics_when_values_are_valid(self):
        schema = self._init_schema()
        form = dict(foo='very long', bar='lOS')
        first = self._foo_shortener_heuristics(max_length=3)
        leet_speak = self._leet_speak_heuristics()
        last = self._foo_shortener_heuristics(max_length=2)

        result = FormValidation(schema, heuristics=(first, leet_speak, last)).run(form)
        assert_equals(dict(foo='ver', bar='105'), result.value)
        assert_true(first.was_called)
        assert_false(last.was_called)

    def test_heuristics_can_update_multiple_fields_at_once(self):
        schema = self._init_schema()
        form = dict(foo='123', bar='abc')
        switcher = self._switch_field_heuristics()

        result = FormValidation(schema, heuristics=(switcher,)).run(form)
        assert_true(switcher.was_called)
        assert_false(result.contains_errors())
        assert_equals(dict(foo='abc', bar='123'), result.value)

    def test_only_applies_heuristic_if_all_replaced_fields_are_valid_afterwards(self):
        schema = self._init_schema()
        form = dict(foo='foo', bar='bar')
        switcher = self._switch_field_heuristics()

        result = FormValidation(schema, heuristics=(switcher,)).run(form)
        assert_true(switcher.was_called)
        assert_true(result.contains_errors())
        assert_equals(dict(foo='foo', bar=None), result.value)

    def test_can_handle_heuristic_conditions_which_contain_valid_fields(self):
        schema = self._init_schema()
        switcher = self._switch_field_heuristics()
        switcher.when = {'foo': 'too_long', 'bar': None}
        validator = FormValidation(schema, heuristics=(switcher,))

        result = validator.run(dict(foo='12345', bar='abc'))
        assert_false(switcher.was_called)
        assert_true(result.contains_errors())
        assert_equals(dict(foo=None, bar=None), result.value)

        switcher.was_called = False
        result = validator.run(dict(foo='12345', bar='123'))
        assert_true(switcher.was_called)
        assert_false(result.contains_errors())
        assert_equals(dict(foo='123', bar='12345'), result.value)

    def test_can_handle_multiple_heuristic_conditions_for_single_field(self):
        schema = self._init_schema()
        switcher = self._switch_field_heuristics()
        switcher.when = {'foo': ('too_long', 'invalid_type')}
        validator = FormValidation(schema, heuristics=(switcher,))

        # foo: "empty"
        result = validator.run(dict(foo=None, bar='abc'))
        assert_true(result.contains_errors())
        assert_false(switcher.was_called)
        assert_equals(dict(foo=None, bar=None), result.value)

        switcher.was_called = False
        result = validator.run(dict(foo='12345', bar='123'))
        assert_false(result.contains_errors())
        assert_true(switcher.was_called)
        assert_equals(dict(foo='123', bar='12345'), result.value)

        switcher.was_called = False
        result = validator.run(dict(foo=345, bar='123'))
        assert_false(result.contains_errors())
        assert_true(switcher.was_called)
        assert_equals(dict(foo='123', bar=345), result.value)

    def test_can_catch_unexpected_errors_in_field_validators(self):
        schema = self._init_schema()
        schema.add('baz', self._crashing_validator())
        validator = FormValidation(schema)
        result = validator.run(dict(foo='345', bar='123', baz='anything'))

        assert_true(result.contains_errors())
        assert_equals(dict(foo='345', bar='123', baz=None), result.value)
        assert_true(result.baz.contains_errors())

    # actually we should also catch errors in formvalidators and heuristics but
    # so far we don't have a mechanism to represent 'global' failures which are
    # not tied to a particular field.

    # --- internal helpers ----------------------------------------------------
    def _init_schema(self):
        schema = PydicaSchema()
        schema.add('foo', StringValidator(max_length=3))
        schema.add('bar', IntegerValidator())
        return schema

    def _crashing_validator(self):
        class CrashingValidator(Validator):
            def validate(self, converted_value, context):
                raise ValueError('You shall not pass')
        return CrashingValidator()

    def _foo_shortener_heuristics(self, max_length=3):
        class Shortener(HeuristicFixer):
            when={'foo': 'too_long'}
            was_called = False

            def run(self, form_data):
                self.was_called = True
                foo = form_data.initial_value['foo']
                return ({'foo': foo[:max_length]}, )
        return Shortener()

    def _noop_heuristics(self, field_name, error_key):
        class Shortener(HeuristicFixer):
            when={field_name: error_key}
            was_called = False

            def run(self, form_data):
                self.was_called = True
                return None
        return Shortener()

    def _leet_speak_heuristics(self):
        class LeetSpeak(HeuristicFixer):
            when={'bar': 'invalid_number'}
            was_called = False

            def run(self, form_data):
                self.was_called = True
                bar = form_data.initial_value['bar']
                fixed_bar = bar.replace('l', '1').replace('O', '0').replace('S', '5')
                return ({'bar': fixed_bar}, )
        return LeetSpeak()

    def _switch_field_heuristics(self):
        class FieldSwitcher(HeuristicFixer):
            when={'bar': 'invalid_number'}
            was_called = False

            def run(self, form_data):
                self.was_called = True
                foo = form_data.initial_value['foo']
                bar = form_data.initial_value['bar']
                return ({'foo': bar, 'bar': foo}, )
        return FieldSwitcher()

    def _fake_form(self, **form_values):
        form = AttrDict(
            field_names=tuple(form_values),
        )
        for name, value in form_values.items():
            form[name] = AttrDict(value=value)
        return form

    def _error(self, key, message):
        return AttrDict(key=key, message=message)

    def _error_keys(self, errors):
        return tuple([error.key for error in errors])

