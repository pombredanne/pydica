#!/usr/bin/env python
# -*- coding: utf-8 -*-
from __future__ import division, absolute_import, print_function, unicode_literals

"""
Remote Control
==============

This module allows to inject limited commands into an application
by polling for file changes.

Polling is used instead of more efficient tools like
QFileSystemWatcher from PySide or the sophisticated watchdog
( http://packages.python.org/watchdog/installation.html )
with many dependencies, pyinotify, etc.

Reason: it should work on any platform and on network drives.
"""

'''
Protocol:
The remote controller writes and closes the command file.
Then it creates the handshake file.

The client(server) waits for the handshake file to exist.
It then executes the command file.

After that, it removes the command and the handshake file (in this order).
'''

import os, sys
import ddc.config.pyside_fix
from PySide import QtCore, QtGui

from ddc.tool.cdb_collection import CDB_Collection
from functools import partial
from ddc.tool.tupletool import namedtuple

# fixed file names
COMMAND_FN = 'PYD_REM.TXT'
HANDSHAKE_FN = 'PYD_REM.OK'
INTERVAL_MS = 100


class ConfigError(Exception):
    pass

class InvalidCmdError(Exception):
    pass

class ProtocolError(Exception):
    pass

'''
a user event class that can be used to send a command
to a receiver (the app).
DEPRECATED, signal/slots is much better suited for the task.

class RCEvent(QtCore.QEvent):
    RCEventType = QtCore.QEvent.Type(QtCore.QEvent.registerEventType())

    def __init__(self, rand=0):
        super(RCEvent, self).__init__(RCEvent.RCEventType)
        self._rand = rand

    def getRand(self):
        return self._rand
'''


_ArgDef = namedtuple('_ArgDef', ('name', 'type', 'description'))

class _ArgTypes(object):
    _argdefs = (
        ('cdb_filename', str,
         '''
         Der Name (inkl. Pfad) einer CDB- oder RDB-Datei, die zu öffnen ist.
         '''),
        ('ibf_filename', str,
         '''
         Der Name (inkl. Pfad) zur IBF-Datei des gerade geöffneten Stapels.
         '''),
        ('beleg_nr',     int,
         '''
         Die Nummer des Belegs innerhalb der Datei, die angesprungen wird.
         '''),
        ('pic_nr',       str,
         '''
         Die PIC-Nr. (= Beleg-Nr. = kodierte Nummer = CODNR), die der
         angesprungene Beleg haben muß.
         '''),
        ('message',      str,
         '''
         Ein einzeilger Text (Encoding noch zu klären). Der Text enthält
         kein Semikolon!
         '''),
        ('field_name',   str,
         '''
         Ein Bezeichner der sich auf das aktuelle Formular bezieht. Schreibweise
         irrelevant, der Name wird im Formulardatensatz gesucht. Keine Fehlermeldung
         wenn nicht gefunden, da nur zur Visualisierung (momentan).
         '''),

        ('DEFAULT',      str,
         '''
         Bisher unspezifiziert, ignoriert
         '''),
    )

    def __init__(self):
        for name, tp, desc in self._argdefs:
            desc = ' '.join(desc.split())
            setattr(self, name, _ArgDef._make((name, tp, desc)))

argtypes = _ArgTypes()


class _CommandDef(object):
    '''
    Definition of a command, its syntax, parameter names and types
    '''

    def __init__(self, prototype, description):
        self.__doc__ = description
        #cmd, *optarg = prototype.split(None, 1)
        # python2.7 does not support this, therefore:
        cmd, optarg = (prototype.split(None, 1) + [''])[:2]
        self.name = cmd.lower()
        args = optarg and optarg.split(';') or []
        self.arguments = tuple(getattr(argtypes, argname)
                               for argname in args)

    def __repr__(self):
        return '<{} {}>'.format(self.name.upper(),
                                ';'.join(arg.name for arg in self.arguments))
    @property
    def signature(self):
        return tuple(arg.type for arg in self.arguments)


class CommandDefs(object):
    '''
    This class defines the commands in an abstract way. The commands are
    intentionally not instantiated by some nifty decorative class, in order to
    keep the definition as magic-free as possible. (compare also the dbdefs
    module which is similar.)
    CommandDefs will probably be moved out of this module at some time.

    Some magic is involved later in class "Command", the real use-case.
    '''

    cmd_opendel = (
       'OPENDEL cdb_filename;beleg_nr;pic_nr',
       '''
       Öffnet die angegebene Datei mit dem angegebenen Beleg.
       Der Beleg kann gelöscht sein.
       Abbruch der Befehlssequenz wenn weder beleg_nr noch pic_nr
       gefunden werden. Setzt die Wirkung von HIGHLIGHT zurueck.
       ''')

    cmd_openasc = (
       'OPENASC cdb_filename;beleg_nr;pic_nr',
       '''
       Gleiche Arbeitsweise wie OPENDEL, arbeitet nur auf nicht gelöschten
       Belegen. Nur existierende Belege sind vom Ascii-Interface erreichbar.
       ''')

    cmd_highlight = (
        'HIGHLIGHT field_name;DEFAULT',
        '''
        Zeigt das angegebene Feld markiert an. Zur Zeit gibt es nur die Auszeichnung
        DEFAULT, sie wird noch nicht ausgewertet. Es können beliebig viele HIGHLIGHT Befehle in einer
        Befehlssequenz angegeben werden.
        (Befehl ist erweiterbar)
        ''')

    cmd_setfocus = (
        'SETFOCUS field_name;DEFAULT',
        '''
        Setzt den Eingabefocus auf das angegebene Feld. Parameter sind undefiniert.
        Parameter ist DEFAULT, wird noch nicht ausgewertet. Der jeweils letzte
        SETFOCUS Befehl ist wirksam, da nur ein Feld fokussiert werden kann.
        (Befehl ist erweiterbar)
        ''')

    cmd_delete_current = (
        'DELETE_CURRENT',
        '''
        Setzt Löschmarkierung fuer aktuellen Beleg.
        Keine Aktion wenn bereits markiert.
        ''')

    cmd_undelete_current = (
        'UNDELETE_CURRENT',
        '''
        Entfernt Löschmarkierung fuer aktuellen Beleg.
        Keine Aktion wenn Beleg nicht markiert war.
        ''')

    cmd_reklafax = (
        'REKLAFAX cdb_filename;beleg_nr;pic_nr;message',
        '''
        Kopiert einen String in die Zwischenablage. Der String hat das folgende
        Format:

            REKLAFAX REQUEST: CODNR: pic_nr BELEGNR: beleg_nr FILE: ibf_filename MESSAGE: message

        Die Zwischenablage wird von einem anderen Programm überwacht und beim
        Auftauchen eines solchen Strings erfolgt eine entsprechende Reaktion.
        Pydica braucht sich nach dem Kopieren in die Zwischenablage darum nicht
        zu kümmern.

        **Achtung: Die ``beleg_nr`` muß an dieser Stelle dreistellig vorgenullt übergeben werden!**
        ''')

    cmd_ascii = (
        'ASCII cdb_filename',
        '''Erstellt für die angegebene Datei eine ASCII-Datei.''')

    cmd_close = (
        'CLOSE',
        '''Schliesst die aktuelle Datei. Momentan notwendig fuer pytest''')


class Command(object):
    '''
    Command definition class, based upon CommandDefs.
    Instantiation of this class results in a parsed command or an error.
    '''
    def def_commands(namespace):
        for cmd in dir(CommandDefs):
            if cmd.startswith('cmd_'):
                namespace[cmd] = cd = _CommandDef(*getattr(CommandDefs, cmd))
                argnames = list(arg.name for arg in cd.arguments)
                namespace['arg_' + cd.name] = namedtuple(cd.name, argnames)

    def_commands(locals())
    del def_commands

    def __init__(self, command_str):
        parts = command_str.split(None, 1)
        if not parts:
            raise SyntaxError('no command given')
        name = parts[0]
        args = parts[1:] and parts[1] or ''
        self.name = name.lower()
        try:
            cmd = getattr(self.__class__, 'cmd_' + self.name)
            argtup = getattr(self.__class__, 'arg_' + self.name)
        except AttributeError as e:
            raise InvalidCmdError(e)
        args = args and args.split(';') or []
        if len(cmd.arguments) < len(args):
            raise SyntaxError('too few arguments for {}: {}'.format(name, args))
        elif len(cmd.arguments) > len(args):
            raise SyntaxError('too many arguments for {}: {}'.format(name, args))
        arglis = []
        for i, arg in enumerate(cmd.arguments):
            v = args[i]
            argtype = cmd.arguments[i]
            tp = argtype.type
            if tp in (int, str):
                try:
                    v = tp(v)
                except ValueError as e:
                    raise TypeError(e)
            if argtype.name == 'beleg_nr':
                v = v - 1 # counted from zero, internally
                if v < 0:
                    raise ValueError('invalid {}: {}'.format(argtype.name, arg))
            arglis.append(v)

        self.argvalues = argtup._make(arglis)

    def __repr__(self):
        return '<{}>'.format(self.argvalues)


class RemoteControl(QtCore.QObject):
    '''
    A timer driven remote control class that polls files.
    It emits signals when a new command is received.
    By using a signal instead of a direct function call, it is
    easy to change to a thread-based implementation.
    '''
    c = Command
    got_opendel_cmd          = QtCore.Signal(*c.cmd_opendel.signature)
    got_openasc_cmd          = QtCore.Signal(*c.cmd_openasc.signature)
    got_delete_current_cmd   = QtCore.Signal(*c.cmd_delete_current.signature)
    got_undelete_current_cmd = QtCore.Signal(*c.cmd_undelete_current.signature)
    got_highlight_cmd        = QtCore.Signal(*c.cmd_highlight.signature)
    got_setfocus_cmd         = QtCore.Signal(*c.cmd_setfocus.signature)
    got_reklafax_cmd         = QtCore.Signal(*c.cmd_reklafax.signature)
    got_ascii_cmd            = QtCore.Signal(*c.cmd_ascii.signature)
    got_close_cmd            = QtCore.Signal(*c.cmd_close.signature)

    got_an_error             = QtCore.Signal(Exception)
    del c

    def __init__(self, watch_path):
        super(RemoteControl, self).__init__()
        if not os.path.isdir(watch_path):
            raise ConfigError('directory {} not found'.format(watch_path))
        self.watch_path = watch_path
        self.hsh_fp = os.path.join(watch_path, HANDSHAKE_FN)
        self.cmd_fn = os.path.join(watch_path, COMMAND_FN)
        self.timer = QtCore.QTimer(self)
        self.timer.timeout.connect(self._on_timeout)
        self.debug = 0
        self.idle_count = 0
        self.action_count = 0

    def start(self, interval=INTERVAL_MS):
        self.timer.stop()
        self.timer.setSingleShot(True)
        self.interval = interval
        self._restart()

    def _restart(self):
        self.timer.start(self.interval)

    def stop(self):
        self.timer.stop()

    def _log(self, *args, **kw):
        if self.debug:
            if args and isinstance(args[0], Exception):
                print(repr(args[0]))
            else:
                print(*args, **kw)
            sys.stdout.flush()

    def _on_timeout(self):
        if self._check_condition():
            self._log('X\n', end='')
            self.action_count += 1
            self.do_action()
            self._handshake()
        else:
            self._log('.', end='')
            self.idle_count += 1
        self._restart()

    def _check_condition(self):
        return os.path.isfile(self.hsh_fp)

    def do_action(self):
        fn = self.cmd_fn
        if not os.path.isfile(fn):
            msg = '{} not found!'.format(fn)
            self._log('\n'
                      'warning: {}'.format(msg))

            e = ConfigError(msg)
            self.got_an_error.emit(e)
            return
        with open(fn) as f:
            cmdlines = []
            for line in f.readlines():
                line = line.strip()
                if line and not line.startswith('#'):
                    cmdlines.append(line)
        commands = []
        for line in cmdlines:
            try:
                self._log('parsing', line)
                parsed = Command(line)
                commands.append(parsed)
            except Exception as e:
                self._log(e)
                self.got_an_error.emit(e)
                return
        # the rest will be done in the application
        for cmd in commands:
            signal = getattr(self, 'got_{}_cmd'.format(cmd.name))
            self._log('emitting', cmd)
            signal.emit(*cmd.argvalues)

    def _handshake(self):
        assert os.path.isfile(self.cmd_fn)
        os.remove(self.cmd_fn)
        assert os.path.isfile(self.hsh_fp)
        os.remove(self.hsh_fp)


class CDB_Reader(QtCore.QObject):
    signal_open      = QtCore.Signal(CDB_Collection)
    signal_goto      = QtCore.Signal(int)
    signal_setfocus  = QtCore.Signal(str)
    signal_highlight = QtCore.Signal(str)
    signal_reklafax  = QtCore.Signal(str, int, str, str)
    signal_ascii     = QtCore.Signal(str)
    signal_delete    = QtCore.Signal()
    signal_undelete  = QtCore.Signal()
    signal_close     = QtCore.Signal()
    signal_error     = QtCore.Signal(Exception)

    def __init__(self, *args):
        super(CDB_Reader, self).__init__(*args)
        self.seq = None

        self._connect_strings = {}
        # save the registered signal names
        def dummy(self, *args, **kw):
            pass
        for signame in self.__class__.__dict__.keys():
            if signame.startswith('signal_'):
                self._this_sig = getattr(self, signame)
                self._this_sig.connect(dummy)
                self._this_sig.disconnect(dummy)

    def connectNotify(self, signalstr):
        # just to find out what the names of the signals are ;-)
        self._connect_strings[self._this_sig] = signalstr
        super(CDB_Reader, self).connectNotify(signalstr)

    def opendel_cmd(self, fname, nr, codnr):
        return self._open_cmd(fname, nr, codnr, True)

    def openasc_cmd(self, fname, nr, codnr):
        return self._open_cmd(fname, nr, codnr, False)

    def _open_cmd(self, fname, nr, codnr, delflag):
        _p = os.path
        fname = _p.normcase(_p.realpath(fname.replace('\\', _p.sep)))
        if self.seq and self.seq.cdb_filename == fname:
            # already open, just change the position
            seq = self.seq
            use_open = False
        else:
            seq = CDB_Collection(fname)
            use_open = True
        try:
            seq.seek(nr)
        except Exception as e:
            if self.receivers(self._connect_strings[self.signal_error]) == 0:
                if QtGui.QApplication.instance():
                    # we have a running app and can show a message
                    mbox = QtGui.QMessageBox()
                    mbox.setText(str(e))
                    mbox.setInformativeText(fname)
                    mbox.setIcon(QtGui.QMessageBox.Critical)
                    mbox.exec_()
                else:
                    raise e
            else:
                # there is a receiver for the message
                self.signal_error.emit(e)
            return

        # see if we allow for deleted forms
        if delflag:
            allowed = (False, True)
        else:
            allowed = (False,)
        # we now have the file
        self.seq = seq
        prescription = seq.current_form
        pic_nr = prescription.pic_nr
        is_del = prescription.is_deleted()
        if pic_nr == codnr and is_del in allowed:
            # all fine
            ret = 0
            goto = nr
        else:
            # we need to scan the dictionary.
            # we can do some optimization here without a dict: the pic_nr in the
            # batch are ALWAYS in ascending order, so we can safely search
            # forward, if the pic_nr at nr is lower than the requested codnr,
            # and search backwards if the pic_nr at nr is greater than the
            # requested codnr
            #
            # this should minimize the network traffic compared to the previous
            # solution "for i in range(seq.form_count)" - in the worst case we
            # had to load 300 form-headers to find the right form (nr is 299,
            # but 300 is the form with the requested pic_nr)
            if pic_nr < codnr:
                # search forward if the pic_nr at nr is lower than codnr
                start_nr = nr + 1
                stop_nr = start_nr + seq.form_count
                step = 1
            else:
                # search backward if the pic_nr at nr is greater than codnr
                start_nr = nr - 1
                stop_nr = start_nr - seq.form_count
                step = -1
            for i in xrange(start_nr, stop_nr, step):
                imod = i % seq.form_count
                seq.seek(imod)
                pic_nr = seq.current_form.pic_nr
                is_del = seq.current_form.is_deleted()
                if pic_nr == codnr and is_del in allowed:
                    ret = goto = imod
                    break
            else:
                msg = 'PIC {} nicht gefunden!'.format(codnr)
                if self.receivers(self._connect_strings[self.signal_error]) == 0:
                    if QtGui.QApplication.instance():
                        # we have a running app and can show a message
                        mbox = QtGui.QMessageBox()
                        mbox.setText(msg)
                        mbox.setInformativeText(fname)
                        mbox.setIcon(QtGui.QMessageBox.Critical)
                        mbox.exec_()
                    else:
                        raise ValueError(msg)
                else:
                    # there is a receiver for the message
                    self.signal_error.emit(msg)
                return

        if use_open:
            self.signal_open.emit(seq)
        else:
            self.signal_goto.emit(goto)
        return ret

    def setfocus_cmd(self, field_name):
        self.signal_setfocus.emit(field_name)

    def highlight_cmd(self, field_name):
        self.signal_highlight.emit(field_name)

    def delete_cmd(self):
        self.signal_delete.emit()

    def undelete_cmd(self):
        self.signal_undelete.emit()

    def reklafax_cmd(self,  fname, nr, codnr, message):
        self.signal_reklafax.emit(fname, nr, codnr, message)

    def ascii_cmd(self, fname):
        self.signal_ascii.emit(fname)

    def close_cmd(self):
        if self.seq:
            self.seq.close()
            self.seq = None


if __name__ == '__main__':
    # http://stackoverflow.com/questions/4938723/what-is-the-correct-way-to-make-my-pyqt-application-quit-when-killed-from-the-co
    # install a console Ctrl-C handler
    import signal
    signal.signal(signal.SIGINT, signal.SIG_DFL)

    app = QtCore.QCoreApplication(sys.argv)
    watch = os.getcwd()
    print('''
    create a command file {}
    and then a handshake file {}
    in path {}.
    Stop with CTRL-C
    '''.format(COMMAND_FN, HANDSHAKE_FN, watch))
    if True:
        rc = RemoteControl(watch)
        rc.debug = 1
        rc.start()
        app.exec_()

    else: # trying to run the test without pytest
        from ddc.tool.test import test_remote_control as t
        tc = t.TestRemote()
        tc.setup_class()
        tc.setup_method(tc.test_remote_missing_cmdfile)
        tc.test_remote_missing_cmdfile()

    '''
    # not used, but signals
    for a in range(5):
        e = RCEvent(a)
        print(e.type())
        print(e.getRand())
    '''
