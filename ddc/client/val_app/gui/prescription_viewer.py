# -*- coding: utf-8 -*-
from __future__ import division, absolute_import, print_function, unicode_literals

import os

from PySide import QtCore, QtGui
from .fieldpainting import FieldPainter
from .ui_mainwindow import Ui_MainWindow


__all__ = ['PrescriptionViewer']

class PrescriptionViewer(QtGui.QMainWindow, Ui_MainWindow):
    def __init__(self, parent=None):
        super(PrescriptionViewer, self).__init__(parent)
        self.setupUi(self)

        # program logo, for the fun of it
        self.setWindowIcon(QtGui.QIcon(':/newPrefix/logo icon.png'))

        self.scale_factor = 1.0
        self.aspect_factor = 1.0
        # alignment of the image label
        # XXX make this an option
        align = QtCore.Qt.AlignTop | QtCore.Qt.AlignLeft
        text_align = QtCore.Qt.AlignTop | QtCore.Qt.AlignRight

        self.image_label.setAlignment(align)
        self.zoom_label.setAlignment(align)

        self.image_scrollarea.setBackgroundRole(QtGui.QPalette.Dark)
        self.zoom_scrollarea.setBackgroundRole(QtGui.QPalette.Dark)
        self.image_scrollarea.setAlignment(align)
        self.zoom_scrollarea.setAlignment(align)

        self.text_scrollarea.setAlignment(text_align)
        self.text_page = self.page_0 # use this name from now on
        self.text_page.scroll_area = self.text_scrollarea

        # set a default screen size
        # XXX make this an option / use setting from last time?
        self.resize(1024, 768)

        self.createActions()
        self.createMenus()

        self.fitToWindow()

        self.title_text = "DSZ Client Validator"
        self.setWindowTitle(self.title_text)

        #desk = QtGui.QApplication.desktop()
        '''
        Unfortunately, we cannot use the dpi info from Qt or even Mac OS.
        This needs to be configured per device.
        Right now I'm using the exact data for the Thunderbolt display.
        '''
        dpi = self.screen_dpi = 108.79 # 27" Mac
        # in order to get this generally correct, the 'edid' must be
        # found and parsed. Will do that, soon.

        img_dpi = 200 # just a guess for now, adjusted after open()
        self.init_scale = float(dpi) / img_dpi

        # setting a good scale increment that meets a DIN fraction
        # and happens to be the equal-temperament tuning
        self.scale_delta = 2**(1./12)   # or 1200 cent


    # Tab handling is a bit tricky because it is intercepted
    # internally.
    # for now, we don't modify the tab movements,
    # but we make sure that the image gets a refresh.

    def focusNextPrevChild(self, flag):
        ret = super(PrescriptionViewer, self).focusNextPrevChild(flag)
        painter = self.image_label.painter
        if painter:
            # XXX refactor. This should not be defined here.
            self.image_label.update()
            if isinstance(painter, FieldPainter):
                painter.ensure_focused_field_visible(self.image_scrollarea)
        painter = self.zoom_label.painter
        if painter:
            # XXX refactor. This should not be defined here.
            self.zoom_label.update()
            if isinstance(painter, FieldPainter):
                painter.ensure_focused_field_visible(self.zoom_scrollarea)
        return ret

    def open(self, filename=None):
        if not filename:
            filename, _ = QtGui.QFileDialog.getOpenFileName(self, "Open Image File",
                                                            QtCore.QDir.currentPath())
        if not filename:
            return

        if isinstance(filename, type(())):
            hack = filename # un-hack later but soon
            filename, buff = filename
            ba = QtCore.QByteArray(buff)
            io = QtCore.QBuffer(ba)
            reader = QtGui.QImageReader(io)
            image = reader.read()
        else:
            hack = False
            image = QtGui.QImage(filename)
        if image.isNull():
            QtGui.QMessageBox.information(self, "Recipe Viewer",
                    "Cannot load %s." % filename)
            return
        try:
            os.chdir(os.path.dirname(filename))
        except:
            pass

        img_dpi = image.physicalDpiY()

        _, ext = os.path.splitext(filename)
        if ext.lower() in ('.tif', '.tiff'):
            class hack:
                correct = (0, 0, 1192, 832) # argh, clean this up!
                therect = (0, 0, image.width(), image.height())

            if hasattr(hack, 'correct'):
                # the image needs to be scaled before use!
                img_dpi = 200
                cr = hack.correct
                w, h = cr[2], cr[3]
                image = image.scaled(w, h, aspectMode=QtCore.Qt.IgnoreAspectRatio,
                                     mode=QtCore.Qt.FastTransformation)
                rx, ry = hack.correct[2:]
                vx, vy = hack.therect[2:]
                # identical: korr = (ry/vy)/(rx/vx)
                korr = ry*vx / (rx*vy)
                self.aspect_factor = korr
        self.init_scale = float(self.screen_dpi) / img_dpi
        self.image_label.setPixmap(QtGui.QPixmap.fromImage(image))
        self.zoom_label.setPixmap(QtGui.QPixmap.fromImage(image))
        self.scaleImage(1.0)

        a6w_200 = 1165.354
        a6h_200 =  826.772


    def zoomIn(self):
        self.resetActions()
        self.scaleImage(self.scale_delta)

    def zoomOut(self):
        self.resetActions()
        self.scaleImage(1/self.scale_delta)

    def normalSize(self):
        self.resetActions()
        self.normalSizeAct.setEnabled(False)
        self.normalSizeAct.setChecked(True)
        self.scale_factor = 1.0
        self.scaleImage(1.0)

    def fitToWindow(self):
        self.resetActions()
        self.fitToWindowAct.setEnabled(False)
        self.fitToWindowAct.setChecked(True)
        self.image_scrollarea.setWidgetResizable(True)

    def split_vertical(self):
        split_state = self.splitVerticalAct.isChecked()
        orients = (QtCore.Qt.Horizontal, QtCore.Qt.Vertical)
        self.splitter.setOrientation(orients[split_state])

    def createActions(self):
        self.openAct = QtGui.QAction("Open Image File...", self, shortcut="Ctrl+Shift+O",
                triggered=self.open)

        self.exitAct = QtGui.QAction("E&xit", self, shortcut="Ctrl+Q",
                triggered=self.close)

        self.zoomInAct = QtGui.QAction("Zoom &In (25%)", self,
                shortcut="Ctrl++", enabled=True, triggered=self.zoomIn)

        self.zoomOutAct = QtGui.QAction("Zoom &Out (25%)", self,
                shortcut="Ctrl+-", enabled=True, triggered=self.zoomOut)

        self.normalSizeAct = QtGui.QAction("&Normal Size", self,
                shortcut="Ctrl+0", enabled=True, checkable=True, triggered=self.normalSize)

        self.fitToWindowAct = QtGui.QAction("Zoom to &Fit", self,
                shortcut="Ctrl+9", enabled=True, checkable=True, triggered=self.fitToWindow)

        self.splitVerticalAct = QtGui.QAction("&Vertical split", self,
                shortcut="Ctrl+V", enabled=True, checkable=True, triggered=self.split_vertical)


    def createMenus(self):
        self.fileMenu = QtGui.QMenu("&File", self)
        self.fileMenu.addAction(self.openAct)
        self.fileMenu.addSeparator()
        self.fileMenu.addAction(self.exitAct)

        self.viewMenu = QtGui.QMenu("&View", self)
        self.viewMenu.addAction(self.zoomInAct)
        self.viewMenu.addAction(self.zoomOutAct)
        self.viewMenu.addAction(self.normalSizeAct)
        self.viewMenu.addAction(self.fitToWindowAct)
        self.viewMenu.addAction(self.splitVerticalAct)

        self.menuBar.addMenu(self.fileMenu)
        self.menuBar.addMenu(self.viewMenu)

    def resetActions(self):
        self.fitToWindowAct.setEnabled(True)
        self.fitToWindowAct.setChecked(False)
        self.normalSizeAct.setEnabled(True)
        self.normalSizeAct.setChecked(False)
        self.image_scrollarea.setWidgetResizable(False)
        self.zoom_scrollarea.setWidgetResizable(False)

    def ensure_text_visible(self):
        '''
        Make the text visible.
        With the help of auto_resize, a resize event does the job.
        '''
        self.text_page.auto_resize = True
        if self.width() > 1280:
            # big screen
            self.splitter.setSizes((7000, 6000))
        else:
            self.splitter.setSizes((5000, 6000))

    def scaleImage(self, factor):
        self.scale_factor *= factor
        scale = self.init_scale * self.scale_factor
        self.scroll_area_image_contents.resize(scale * self.image_label.pixmap().size())
        # quick hack: set a decent default for zoom
        zoom_scale = self.init_scale * 2
        self.scroll_area_zoom_contents.resize(zoom_scale * self.zoom_label.pixmap().size())

        self.adjustScrollBar(self.image_scrollarea.horizontalScrollBar(), factor)
        self.adjustScrollBar(self.image_scrollarea.verticalScrollBar(), factor)
        self.adjustScrollBar(self.zoom_scrollarea.horizontalScrollBar(), factor)
        self.adjustScrollBar(self.zoom_scrollarea.verticalScrollBar(), factor)

        self.zoomInAct.setEnabled(self.scale_factor < 3.0)
        self.zoomOutAct.setEnabled(self.scale_factor > 0.1)

    def adjustScrollBar(self, scrollBar, factor):
        scrollBar.setValue(int(factor * scrollBar.value()
                                + ((factor - 1) * scrollBar.pageStep()/2)))

    def closeEvent(self, event):
        '''capture the close event and redirect'''
        try:
            # the owner of this instance may redefine the closeEvent
            # if it is inserted as self._redirector. See __init__ below.
            self._redirector.closeEvent(event)
        except AttributeError:
            event.accept()

