# -*- coding: utf-8 -*-
from __future__ import division, absolute_import, print_function, unicode_literals

from pycerberus.errors import InvalidDataError

from ..lib.form_data import FormData
from ..lib.attribute_dict import AttrDict


__all__ = ['FormValidation']

class FormValidation(object):
    """
    This class IS responsible for running the validation according to the
    specified schema and handle its results. This includes applying heuristic
    rules to fix bad data when a validation failure occured as well as running
    post processors (which can suppress or downgrade certain errors).

    This class SHOULD NOT have any dependencies to the "greater" pydica system
    which means it shouldn't know anything about CDB files, forms, tasks or
    logging changes. Also it doesn't know about plugins and such. Building the
    actual validation schema ("validation configuration") is out of scope for
    this class. All of that is the responsibility of the ValidationModule.
    (At least that was the idea when building it. Let's see if this works out.)
    """
    def __init__(self, schema, heuristics=(), post_processors=()):
        self.schema = schema
        self.heuristics = tuple(heuristics)
        self.post_processors = tuple(post_processors)

    def run(self, form_values, context=None):
        if context is None:
            context = {}
        field_names = tuple(self.schema.fieldvalidators())
        compound_data = FormData(child_names=field_names)
        initial_values = dict(filter(lambda kv: (kv[0] in field_names), form_values.items()))
        compound_data.set(initial_value=initial_values)
        errors, validated_values = self._run_validation(form_values, context=context)
        # In our processing chain we mostly use the actual string data without
        # conversion. However sometimes post processors need to do some
        # calculations which are much easier if they have access to the
        # converted values.
        valid_fields = tuple(validated_values)
        valid_values = self._extract_valid_values(initial_values, valid_fields)
        compound_data.set(value=valid_values, errors=errors, meta=validated_values)
        compound_data = self._apply_post_processors(compound_data, context)
        if not compound_data.contains_errors():
            return compound_data

        current_best = compound_data.copy()
        for heuristic in self.heuristics:
            if not heuristic.is_applicable(current_best.errors):
                continue
            suggestions = heuristic.run(current_best)
            if suggestions is None:
                continue
            for suggestion in suggestions:
                improved_values = self._try_suggestion(suggestion, current_best, context=context)
                if improved_values is not None:
                    current_best = improved_values
        # "initial_value" must contain the values initially passed into this
        # method ("form_values"). However when trying different heuristics
        # heuristics later in the chain should use improved values from previous
        # algorithms and we store these in "initial_value".
        # The next line just ensures this approach does not affect the caller.
        current_best.update(initial_value=compound_data.initial_value)
        return current_best

    def _apply_post_processors(self, compound_data, context):
        for processor in self.post_processors:
            compound_data = processor.run(compound_data, context)
        return compound_data

    def _try_suggestion(self, suggestion, current_best, context):
        input_data = current_best.copy()
        form_values = input_data.initial_value
        form_values.update(suggestion)
        errors, valid_fields = self._run_validation(form_values, context=context)
        if not self._suggestion_did_make_all_changed_fields_valid(suggestion, errors):
            return None
        valid_values = self._extract_valid_values(form_values, valid_fields)
        # ensure that later heuristics are aware of the current (good) suggestion
        input_data.update(initial_value=form_values)
        input_data.update(value=valid_values)
        # clear errors for fields touched by heuristic (that's ok because we
        # checked above that the suggestion did make all fields valid)
        for field_name in suggestion:
            input_data.get(field_name).update(errors=None)
        return input_data

    def _extract_valid_values(self, form_values, valid_fields):
        if form_values is None:
            return None
        valid_values = {}
        for field_name in valid_fields:
            initial_value = form_values.get(field_name)
            valid_values[field_name] = initial_value
        return valid_values

    def _run_validation(self, form_values, context=None):
        try:
            validated_values = self.schema.process(form_values, context=context)
            return (None, validated_values)
        except InvalidDataError as e:
            field_errors = self._invalid_data_error_to_simplified_dict(e)
            validated_fields = e.result.validated_fields
            return (field_errors, validated_fields)

    def _suggestion_did_make_all_changed_fields_valid(self, suggestion, errors):
        if errors is not None:
            for fixed_field in suggestion:
                if fixed_field in errors:
                    return False
        return True

    def _invalid_data_error_to_simplified_dict(self, e):
        error_info = dict()
        for field, error in e.error_dict().items():
            details = error.details()
            ddc_error = AttrDict(
                key=details.key(),
                message=details.msg(),
                is_warning=False,
                meta=getattr(error, 'meta', None)
            )
            error_info[field] = (ddc_error, )
        return error_info

