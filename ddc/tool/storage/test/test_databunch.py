# -*- coding: utf-8 -*-
from __future__ import division, absolute_import, print_function, unicode_literals

from unittest.case import TestCase

from ddt import ddt as DataDrivenTestCase, data
from pythonic_testcase import *

from ddc.tool.storage.paths import DataBunch


@DataDrivenTestCase
class DataBunchTest(TestCase):
    @data('cdb', 'ibf', 'durus') # but not 'ask'
    def test_is_complete(self, missing_attr):
        attrs = dict(cdb='foo.cdb', ibf='foo.ibf', durus='foo.durus', ask='foo.ask')
        assert_true(DataBunch(**attrs).is_complete())

        attrs[missing_attr] = None
        assert_false(DataBunch(**attrs).is_complete())

        attrs = dict(cdb='foo.cdb', ibf='foo.ibf', durus='foo.durus', ask=None)
        assert_true(DataBunch(**attrs).is_complete())

        attrs[missing_attr] = None
        assert_false(DataBunch(**attrs).is_complete())

