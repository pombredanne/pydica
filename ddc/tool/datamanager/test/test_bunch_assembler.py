# -*- coding: utf-8 -*-
from __future__ import division, absolute_import, print_function, unicode_literals

import os
from unittest.case import TestCase

from pythonic_testcase import *

from ddc.tool.datamanager.bunch_assembler import BunchAssembler
from ddc.tool.storage.paths import DataBunch

def _fix_path(path):
    return path.replace('/', os.path.sep)


class DiscoverLibAssembleBunchesTest(TestCase):
    def test_can_assemble_matching_files_into_bunches(self):
        f = _fix_path
        bunch1 = DataBunch(cdb=f('/foo/bar.cdb'), ibf=f('/foo/00000001/bar.ibf'),
                           durus=f('/foo/bar.durus'), ask=None)
        bunch2 = DataBunch(cdb=f('/baz.cdb'), ibf=f('/00000001/baz.ibf'),
                           durus=f('/baz.durus'), ask=None)
        result = self._assemble_bunches(bunch1 + bunch2)
        assert_equals(set([bunch1, bunch2]), set(result.complete))
        assert_length(0, result.incomplete)
        assert_length(0, result.errors)

    def test_ignores_casing_when_assembling_bunches(self):
        f = _fix_path
        bunch1 = DataBunch(cdb=f('/foo/bar.cDb'), ibf=f('/foo/00000001/bar.IBF'),
                           durus=f('/foo/BaR.DuRuS'), ask=None)
        # ensure that all components are treated case-insensitively
        assert_not_equals(bunch1.cdb.lower(), bunch1.cdb)
        assert_not_equals(bunch1.ibf.lower(), bunch1.ibf)
        assert_not_equals(bunch1.durus.lower(), bunch1.durus)

        result = self._assemble_bunches(bunch1)
        assert_equals((bunch1, ), result.complete)
        assert_length(0, result.incomplete)
        assert_length(0, result.errors)

    def test_can_also_return_information_about_incomplete_bunches(self):
        f = _fix_path
        bunch1 = DataBunch(cdb=f('/foo/bar.cdb'), ibf=None,
                           durus=f('/foo/bar.durus'), ask=None)
        bunch2 = DataBunch(cdb=None, ibf=f('/00000001/baz.ibf'),
                           durus=f('/baz.durus'), ask=None)
        bunch3 = DataBunch(cdb=None, ibf=None,
                           durus=f('/foo/baz.durus'), ask=None)

        result = self._assemble_bunches(filter(None, bunch1+bunch2+bunch3))
        assert_length(0, result.complete)
        assert_equals(set([bunch1, bunch2, bunch3]), set(result.incomplete))
        assert_length(0, result.errors)

    def test_pays_attention_to_directories_while_assembling(self):
        f = _fix_path
        bunch1 = DataBunch(cdb=f('/foo/bar.cdb'), ibf=f('/FOO/00000001/bar.ibf'),
                           durus=f('/foo/bar.durus'), ask=None)
        bunch2 = DataBunch(cdb=f('/foo.cdb'), ibf=f('/00000001/foo.ibf'),
                           durus=None, ask=None)
        bunch3 = DataBunch(cdb=None, ibf=None,
                           durus=f('/baz/foo.durus'), ask=None)

        result = self._assemble_bunches(filter(None, bunch1+bunch2+bunch3))
        assert_equals((bunch1, ), result.complete)
        assert_equals(set([bunch2, bunch3]), set(result.incomplete))
        assert_length(0, result.errors)

    def test_returns_error_when_encountering_ambiguous_filenames(self):
        f = _fix_path
        result = self._assemble_bunches((f('/FOO.cdb'), f('/foo.cdb')))
        assert_length(0, result.complete)
        assert_length(0, result.incomplete)
        assert_length(1, result.errors)
        error = result.errors[0]
        assert_contains(f('FOO.cdb'), error.text)
        assert_contains(f('foo.cdb'), error.text)

    def test_does_not_return_errors_about_ambiguous_filenames_for_different_directories(self):
        f = _fix_path
        bunch1 = DataBunch(cdb=f('/foo/bar.cdb'), ibf=None, durus=None, ask=None)
        bunch2 = DataBunch(cdb=f('/bar/bar.cdb'), ibf=None, durus=None, ask=None)

        result = self._assemble_bunches(filter(None, bunch1+bunch2))
        assert_length(0, result.complete)
        assert_equals(set([bunch1, bunch2]), set(result.incomplete))
        assert_length(0, result.errors)

    def _assemble_bunches(self, filenames):
        return BunchAssembler(filenames).process_filenames()
