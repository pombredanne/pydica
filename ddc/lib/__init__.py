"""
The lib folder should contain libraries which were copied from other projects
for various reasons, e.g.:
 - avoid external dependency
 - not available via pypi

Ideally the code should be copied verbatim even. If changes are necessary
please commit the verbatim code first and modify it later so we have a
meaningful git commit history.
"""