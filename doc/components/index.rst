Komponenten
===========


Inhalt:

.. toctree::
   :maxdepth: 2

   configuration
   ocr
   server-validator
   substitutions
   client-validator
   validators
   task-manager

