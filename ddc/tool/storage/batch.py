# -*- coding: utf-8 -*-
"""
The basic idea is that we always need a triple to access all relevant information.
  1. CDB files for extracted text data
  2. IBF for actual images (scans)
  3. Durus DB for meta data

DataBunch represents the path information for that triple (so no file system
access or active databases required). The Batch is the main abstraction which
hides all implementation details (as much as possible/sensible).
"""

from __future__ import division, absolute_import, print_function, unicode_literals

from .durus_ import DurusDB, DurusKey
from .task import TaskStatus
from .paths import expected_durus_path


__all__ = ['Batch']

class Batch(object):
    def __init__(self, cdb, ibf, durus_db, meta=None):
        self.cdb = cdb
        self.ibf = ibf
        self.durus_db = durus_db
        self.meta = meta or {}

    @classmethod
    def init_from_bunch(cls, databunch, create_new_durus=False,
                        delay_load=False, access='write'):
        """
        Return a new Batch instance based on the given databunch.
        """
        # prevent recursive imports
        # ideally classes from cdb_tool should be located below ".storage" as
        # they deal with the on-disk data layout.
        from ddc.tool.cdb_tool import ImageBatch, FormBatch
        cdb = FormBatch(databunch.cdb, delay_load=delay_load, access=access)
        ibf = ImageBatch(databunch.ibf, delay_load=delay_load, access=access)
        durus_path = databunch.durus
        if create_new_durus:
            if durus_path is None:
                durus_path = expected_durus_path(databunch.cdb)
            durus_db = DurusDB.create_new_db(durus_path)
            cls._create_snapshot(cdb, durus_db)
        else:
            durus_db = DurusDB.init_with_file(durus_path)
            # LATER: verify snapshot and record change if necessary
        return Batch(cdb, ibf, durus_db)

    def commit(self):
        self.durus_db.commit()

    def close(self):
        self.durus_db.rollback()
        self.durus_db.close()
        self.cdb.close()
        self.ibf.close()

    # --- data integrity ------------------------------------------------------
    @classmethod
    def _create_snapshot(cls, cdb, durus_db):
        durus_db.insert_snapshot('cdb', [cdb.filecontent[:]])

    # --- accessing data ------------------------------------------------------
    @property
    def durus(self):
        """Returns the <root> of the Durus database.
        Hint: If you need access to this property you please think about adding
        a method to this class."""
        return self.durus_db.root

    def tasks(self):
        return self.durus[DurusKey.TASKS]

    def form(self, i):
        return self.cdb.forms[i]

    def forms(self):
        return self.cdb.forms

    def new_tasks(self, task_type):
        tasks = []
        for task in self.tasks():
            if (task.type_ == task_type) and (task.status == TaskStatus.NEW):
                tasks.append(task)
        return tasks

