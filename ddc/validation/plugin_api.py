# -*- coding: utf-8 -*-
from __future__ import division, absolute_import, print_function, unicode_literals

import six


__all__ = ['HeuristicFixer']

class HeuristicFixer(object):
    """
    HeuristicFixers receive invalid form data and return suggestions which
    changes to the data might be necessary so it becomes valid.
    """
    when = {}

    # each fixer can decide on its own if it is applicable for the given set
    # of errors.
    # This means all real-world implementations should subclass this class so
    # the benefit from the implementation of "is_applicable()". On the other
    # hand this also means specific fixers can implement arbitrary complex
    # algorithms to determine applicability. In the end I decided that
    # implementation flexibility is important here.
    def is_applicable(self, errors):
        for field_name, error_conditions in self.when.items():
            if (error_conditions is None) or isinstance(error_conditions, six.string_types):
                error_conditions = (error_conditions, )
            field_errors = errors.get(field_name, ())
            field_is_valid = (len(field_errors) == 0)
            if field_is_valid:
                if None in error_conditions:
                    # {<field>: None} means the fixer should be executed if
                    # that field has NO errors. Obviously that is only useful
                    # when combined with other error conditions.
                    continue
                return False
            error_keys = set([error['key'] for error in field_errors])
            has_matching_key = error_keys.intersection(error_conditions)
            if not has_matching_key:
                return False
        return True

    def run(self, form_data):
        """
        Return a list of suggested data changes which may fix the problem.
        Suggestions are dicts (field name -> new value) and checked in the same
        order as returned.
        Returning None means no heuristic was able to come up with suggestions.
        """
        return None

