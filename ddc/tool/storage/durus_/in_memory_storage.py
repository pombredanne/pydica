# -*- coding: utf-8 -*-
"Utility classes for testing Durus without accessing the file system."

from __future__ import division, absolute_import, print_function, unicode_literals

from durus.shelf import Shelf
from durus.file_storage import FileStorage


__all__ = ['InMemoryStorage']

# similar interface to durus.file.File
class DurusFileLikeBuffer():
    def __init__(self, buffer_, display_name=None):
        self.file = buffer_
        self._name = display_name or 'DurusFileLikeBuffer'

    def get_name(self):
        return self._name

    def is_temporary(self):
        return True

    def is_readonly(self):
        return self.file.mode == 'rb'

    def seek(self, n, whence=0):
        self.file.seek(n, whence)
        if whence == 0:
            assert self.file.tell() == n

    def seek_end(self):
        self.file.seek(0, 2)

    def read(self, n=None):
        if n is None:
            return self.file.read()
        return self.file.read(n)

    def tell(self):
        return self.file.tell()

    def stat(self):
        raise NotImplementedError()

    def __len__(self):
        return len(self.file)

    def rename(self, name):
        self._name = name
#
    def obtain_lock(self):
        pass

    def release_lock(self):
        pass

    def write(self, s):
        self.file.write(s)

    def truncate(self):
        self.file.truncate()

    def close(self):
        pass

    def flush(self):
        pass

    def fsync(self):
        pass


class InMemoryStorage(FileStorage):
    def __init__(self, buffer=None, readonly=False, repair=False):
        durus_file = DurusFileLikeBuffer(buffer)
        self.shelf = Shelf(durus_file, readonly=readonly, repair=repair)
        self.pending_records = {}
        self.allocated_unused_oids = set()
        self.pack_extra = None
        self.invalid = set()
