# -*- coding: utf-8 -*-
from __future__ import division, absolute_import, print_function, unicode_literals

from io import BytesIO
import os
import sys

import tiffany

from ddc.plugin import init_plugin_loader
from ddc.tool.cdb_tool import ImageBatch, FormBatch
from ddc.validation.form_validation import FormValidation


def _store_image(image_job, job_index):
    img_data = BytesIO(image_job.get_tiff_image(i))
    img = tiffany.open(img_data)
    img.seek(1)

    single_page_buffer = BytesIO()
    img.save(single_page_buffer)
    single_page_buffer.seek(0)

    tiff_filename = '%d-1.tiff' % i
    file(tiff_filename, 'wb').write(single_page_buffer.read())
    return tiff_filename


if __name__ == '__main__':
    if len(sys.argv) < 2 or not os.path.exists(sys.argv[1]):
        print('usage: {0!s} CDBFILENAME'.format(sys.argv[0]))
        sys.exit(1)
    filename = sys.argv[1]
    batch_filename = sys.argv[1]

    loader = init_plugin_loader('pydica.plugin')
    loader.init()

    # LATER: use config file so the admin can configure which schema to use
    prescription_form = None
    for plugin in loader.activated_plugins.values():
        if prescription_form is None:
            prescription_form = plugin.form_validation_schema()
    if prescription_form is None:
        print('No plugin contributed a validation schema')
        sys.exit(2)

    job = FormBatch(batch_filename)
    image_job_filename = job._image_filename
    image_job = ImageBatch(image_job_filename)

    validation = FormValidation(prescription_form)
    for i in range(job.count()):
        form = job.forms[i]
        errors = validation.run(form)
        if not errors:
            continue
        print('job {0!s} contains errors in fields: {1}'.format(i, errors.keys()))
        for field_name, field_errors in errors.items():
            # currently exactly one error only
            print('    {0}: {1}'.format(field_name, field_errors[0].message))
        tiff_filename = _store_image(image_job, i)
        print('image dumped to %s' % tiff_filename)
