# -*- coding: utf-8 -*-
from __future__ import division, absolute_import, print_function, unicode_literals

from unittest import TestCase

from pycerberus.api import Validator
from pycerberus.validators import IntegerValidator, StringValidator
from pythonic_testcase import *

from ddc.lib.attribute_dict import AttrDict
from ddc.validation import FormValidation, PydicaSchema


class PydicaSchemaTest(TestCase):
    def test_passes_valid_input_data(self):
        schema = self._init_schema()
        form = dict(foo='123', bar='234')
        result = FormValidation(schema).run(form)
        assert_false(result.contains_errors())

    def test_can_ignore_errors(self):
        schema = self._init_schema()
        form = dict(foo='123', bar='something')
        context = {'ignore_errors': {'bar': ('invalid_number',)}}
        result = FormValidation(schema).run(form, context)
        assert_false(result.contains_errors())
        # We always return string data
        assert_equals(dict(foo='123', bar=None), result.value)
        assert_equals(form, result.initial_value)

    def test_only_ignores_specific_errors(self):
        schema = self._init_schema()
        form = dict(foo='123', bar=[])
        context = {'ignore_errors': {'bar': ('invalid_number',)}}
        result = FormValidation(schema).run(form, context)

        assert_true(result.contains_errors())
        assert_equals(dict(foo='123', bar=None), result.value)
        assert_equals(form, result.initial_value)
        bar_errors = result.errors['bar']
        assert_length(1, bar_errors)
        bar_error = bar_errors[0]
        assert_equals('invalid_type', bar_error['key'])

    def test_returns_validated_information_even_for_field_with_ignored_errors(self):
        schema = self._init_schema()
        form = dict(foo='123', bar='456')
        context = {'ignore_errors': {'bar': ('invalid_number',)}}
        result = FormValidation(schema).run(form, context)
        assert_false(result.contains_errors())
        assert_equals(dict(foo='123', bar='456'), result.value)

    def test_runs_formvalidators_without_explicit_dependencies_only_when_all_fields_are_valid(self):
        schema = self._init_schema()
        process_spy = create_spy()
        foo_formvalidator = AttrDict(
            dependencies=lambda context: (),
            process=lambda values, context=None: None
        )
        schema.add_formvalidator(foo_formvalidator)
        form = dict(foo='456', bar='not a number')
        result = FormValidation(schema).run(form)

        assert_true(result.contains_errors())
        bar_errors = result.errors['bar']
        assert_length(1, bar_errors)
        bar_error = bar_errors[0]
        assert_equals('invalid_number', bar_error['key'])
        process_spy.assert_was_not_called()

    def test_can_run_formvalidator_with_dependencies_even_when_other_fields_contain_errors(self):
        schema = self._init_schema()
        compound_validator = self._fake_validator(requires=('foo',))
        schema.add_formvalidator(compound_validator)
        assert_false(compound_validator.was_called)

        form = dict(foo='123', bar='invalid')
        result = FormValidation(schema).run(form)

        assert_true(result.contains_errors())
        assert_is_empty(result.errors['foo'])
        assert_is_not_empty(result.errors['bar'])
        # FormValidation returns values as strings (even after validation/
        # temporary conversion) so we won't see a change here.
        assert_equals('123', result.value['foo'])
        assert_true(compound_validator.was_called)

    def test_skips_formvalidator_if_required_field_contains_errors(self):
        schema = self._init_schema()
        failing_validator = self._fake_validator(requires=('foo',))
        schema.add_formvalidator(failing_validator)
        good_validator = self._fake_validator(requires=('bar',))
        schema.add_formvalidator(good_validator)
        form = dict(foo='invalid', bar='456')
        result = FormValidation(schema).run(form)

        assert_true(result.contains_errors())
        assert_is_empty(result.errors['bar'])
        foo_errors = result.errors['foo']
        assert_length(1, foo_errors)
        foo_error = foo_errors[0]
        # this ensures the formvalidator was not executed (otherwise we'd see a
        # different error key)
        assert_equals('too_long', foo_error['key'])
        assert_false(failing_validator.was_called)
        assert_none(result.value['foo'])
        assert_true(good_validator.was_called)
        assert_equals('456', result.value['bar'])

    def test_runs_other_form_validators_even_if_previous_validator_raised_an_error(self):
        schema = self._init_schema()
        failing_validator = self._fake_validator(requires=('foo',), raise_error=True, fieldname='foo')
        schema.add_formvalidator(failing_validator)
        bar_validator = self._fake_validator(requires=('bar',))
        schema.add_formvalidator(bar_validator)
        form = dict(foo='123', bar='456')
        result = FormValidation(schema).run(form)

        assert_true(result.contains_errors(), message='form must contain some errors')
        assert_is_not_empty(result.errors['foo'])
        assert_is_empty(result.errors['bar'])
        assert_true(failing_validator.was_called, message='foo validator was not called')
        assert_true(bar_validator.was_called, message='bar validator was not called')

    # --- internal helpers ----------------------------------------------------
    def _init_schema(self):
        schema = PydicaSchema()
        schema.add('foo', StringValidator(max_length=3))
        schema.add('bar', IntegerValidator())
        return schema

    def _fake_validator(self, fieldname=None, raise_error=False, **validator_args):
        class FakeValidator(Validator):
            def __init__(self, *args, **kwargs):
                requires = kwargs.pop('requires', None)
                self.requires = lambda context: requires
                self.was_called = False
                super(FakeValidator, self).__init__(*args, **kwargs)

            def convert(self, values, context):
                self.set_internal_state_freeze(False)
                self.was_called = True
                self.set_internal_state_freeze(True)
                if raise_error:
                    error = self.exception('empty', None, context)
                    self.raise_error('empty', None, context, error_dict={fieldname: [error]})
                return values
        return FakeValidator(**validator_args)

