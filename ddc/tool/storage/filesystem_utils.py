# -*- coding: utf-8 -*-
from __future__ import division, absolute_import, print_function, unicode_literals

import os


__all__ = ['look_for_file', 'AmbiguousFilenameError']

class AmbiguousFilenameError(ValueError):
    pass

"""
A note about the following code:

This is an initialization script.
The script's intent is to compute the best interpolation of file names as they are
usually expected. Existing files and their casing are to be respected.

After the script has run, there is no more need to use any physical file name but
the name of the durus database. Any other naming will be resolved by this database.

--------

Finding out about file names and their correct spellings is no trivial task, especially if a
script like this pretends to stay "simple". That means: We cannot assume any knowledge about
the case behavior of the file system, itself.

Common cases are:

- we have Linux, and file names are case sensitive.

- we have OS/X (both case sensitive or not, but aware) or Windows (case aware)
  In case of case-awareness, different casing works, but a single version is stored.

The simple approach to get this right on all relevant platform is:

- we use os.listdir() all the time to get the real filenames

- we keep all dir listings in a cache, for the duration of this script's run.


The whole code got a bit out of hand and might be overly complex but for now it
seems to work well enough.
"""

def find_exact_filenames(path, name):
    """
    Assuming that the file exists, return all it's existing cases.
    """
    lowername = name.lower()
    direntries = [e for e in os.listdir(path) if e.lower() == lowername]
    return direntries

def look_for_file(root_dir, basename, extension):
    """
    Try to find a file, given it's root_dir, basename and extension.

    The file is searched by matching case-insensitively, but if it exists, no
    other version by different naming is allowed.

    The root_dir/filename is returned, with the exact filename appended.
    """
    # note: we could be explicit about the root_dir as well, but this makes
    # little sense, since we are mostly interested in the real filename.
    given_filename = basename + '.' + extension
    found_filenames = find_exact_filenames(root_dir, given_filename)
    if not found_filenames:
        return None
    if len(found_filenames) > 1:
        raise AmbiguousFilenameError(
            'ambiguous filenames found in %r:\n'
            'looking for %r, found %r' %
            (root_dir, given_filename, found_filenames))
    return os.path.join(root_dir, found_filenames[0])

