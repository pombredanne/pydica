# -*- coding: utf-8 -*-
from __future__ import division, absolute_import, print_function, unicode_literals

"""

    CDB collection

This interface is kept very simple, as a starter.
"""
import os
import sys
import logging

import ddc.compat
import ddc
from ddc.tool.cdb_tool import FormBatch
from ddc.tool.cdb_tool import ImageBatch, TiffHandler
from ddc.tool.storage.durus_ import (create_durus_fixture, DurusDB,
                                     InMemoryStorage)
from ddc.tool.storage.batch import Batch
from ddc.tool.storage.testhelpers import set_durus_loglevel
from ddc.tool.tiff_reader import read_tiff_page


def default_cdb_filename():
    cdb_filename = os.path.join(ddc.rootpath, 'ddc/tool/test/data/form_batch/00035100.CDB')
    if len(sys.argv) > 1:
        arg1 = sys.argv[1]
        # extra check if argument is likely a cdb file to get tests to work
        # just in case the command line was something like like
        #    nosetests <pyfile>
        if os.path.isfile(arg1) and arg1.lower().endswith('.cdb'):
            return arg1
        if arg1.upper() == '/PRIVATE':
            return os.path.join(ddc.rootpath, 'private/srw/data/00099201.CDB')
    return cdb_filename


class _CDB_Helper(object):
    def __init__(self, fname, access):
        '''
        This is a transitional initialization method, to be removed
        '''
        if fname is None:
            fname = default_cdb_filename()
        _p = os.path
        fname = _p.normcase(_p.realpath(fname.replace('\\', _p.sep)))
        self.cdb = FormBatch(fname, delay_load=True, access=access)
        iname = self.cdb._image_filename
        try:
            self.ibf = ImageBatch(iname, delay_load=True, access=access)
        except IOError as e:
            self.ibf = None

        _previous_loglevel = set_durus_loglevel(logging.ERROR)
        try:
            durus_fp = create_durus_fixture()
            self.durus_db = DurusDB(InMemoryStorage(durus_fp))
        finally:
            set_durus_loglevel(_previous_loglevel)


class CDB_Collection(object):
    def __init__(self, fname=None, batch=None, access='write'):
        if batch:
            self.batch = batch
            self.validation_mode = True
        else:
            helper = _CDB_Helper(fname, access)
            self.batch = Batch(helper.cdb, helper.ibf, helper.durus_db)
            self.validation_mode = False

        self.form_count = self.cdb.count()
        self.pos = 0

        self.forms = list(CDB_Form(self, i) for i in xrange(self.form_count))

    def seek(self, num):
        if num < 0 or num >= self.cdb.count():
            raise IndexError('CDB index out of range')
        self.pos = num

    def load_image(self, pageno=1):
        nr = self.current_form._form_index
        tiff_data = self.ibf.get_tiff_image(nr)
        ret = read_tiff_page('dummy.tif', tiff_data, pageno)
        # quick hack to get the transform to the real dimensions
        class Correct(tuple):
            correct = (0, 0, 1192, 832)
        return Correct(ret) # hey what a funny cor-rect ;-)

    @property
    def cdb(self):
        return self.batch.cdb

    @property
    def ibf(self):
        return self.batch.ibf

    @property
    def durus_db(self):
        return self.batch.durus_db

    @property
    def durus(self):
        return self.batch.durus

    @property
    def current_form(self):
        return self.forms[self.pos]

    @property
    def cdb_filename(self):
        return self.cdb.batch_filename

    def commit(self):
        self.batch.commit()

    def close(self):
        self.batch.close()


'''
Handling the CDB_Form efficiently is much more complicated than expected:

We may not touch anything of the cdb_tool.Form before actually needed, or the
speed optimization would be lost. cdb_tool.Image also cannot be inspected,
because before that we need to look into the form.

Rules for efficient handling of the form object:

- do not touch fields that are optimized

- do not evaluate the 'rec' fields. The indirection must stay one level
  above, to allow changing the field values.
'''

class CDB_Form(object):

    def __init__(self, parent, index):
        self.parent = parent
        self._form_index = index
        self._dirty_flag = False  # used in the client
        self._th = None # tiff handler
        # nothing else can be pre-computed ...

    def __getattr__(self, name):
        # access to the indexed form structure
        self._form_data = self.parent.cdb.forms[self._form_index]
        # access to the form's header record
        self._form_header = self._form_data.form_header
        # access to the image structure, slightly complicated
        self._image_data = self._find_image()
        if name in self.__dict__:
            return getattr(self, name)
        raise AttributeError("{!r} object has no attribute {!r}"
                             .format(self.__class__.__name__, name))

    def _find_image(self):
        index = self.parent.ibf.image_entries
        return index[self._form_index]

    def delete(self):
        if not self.is_deleted():
            self._del_operation(False)

    def undelete(self):
        if self.is_deleted():
            self._del_operation(True)

    def _del_operation(self, undel):
        if not self._th:
            # the tiff handler will be installed late, on demand.
            # It's action is to update the value of long_data, the logical name
            # of the tiff file.
            # The special name 'DELETED' is used as a marker for the old
            # software. This also needs to be written to the IBF index and to
            # the CDB fields, as can be seen below.
            # The tiff handler encapsulates the tiff related part of the action.

            self._th = TiffHandler(self.parent.ibf, self._form_index)
        if undel:
            # Note: long_data2 is the second version of the tiff header.
            # This is never written, but kept as a backup for un-deleting.
            repl_str = self._th.long_data2.rec.page_name
        else:
            repl_str = 'DELETED'
        #
        # First, change the data of the form header.
        self._form_header.update_rec(
            imprint_line_long = repl_str,
            imprint_line_short = repl_str)
        #
        # Second, change the data of the tiff header.
        #
        # Note that we update only the first copy of the tiff structure (page 1).
        # The second copy will always remain unchanged, because the old software
        # ignores that. Therefore, the second copy is used to restore a "deleted"
        # scan.
        #
        # This line puts the new record data into the tiff header struc (in memory).
        self._th.long_data.update_rec(page_name = repl_str)
        # This line also patches the new data into the tiff structure (in memory).
        self._image_data.update_rec(codnr = repl_str)
        #
        # Upto here, no data was physically written.
        # We do that now. Actually, that should logically be a transaction,
        # but we ignore this for CDB/IBF.
        #
        # First, we actualize the form data:
        # - move the data in memory back to the structure on disk.
        self._form_data.write_back()
        #
        # Then we update the index info in the IBF:
        self.parent.ibf.update_entry(self._image_data)
        #
        # And as the last step, we also ask the tiff handler to write its data to disk.
        self._th.update()
        #
        # Now, everything is done, and we just trigger re-creation of changed fields.
        del self._form_header, self._form_data, self._image_data

    def is_deleted(self):
        return self._form_header.rec.imprint_line_short == 'DELETED'

    def is_dirty(self):
        return self._dirty_flag

    def set_dirty(self, flag=True):
        assert isinstance(flag, bool)
        self._dirty_flag = flag

    @property
    def pic_nr(self):
        codnr = self._image_data.rec.codnr
        if codnr == "DELETED":
            if not self._th:
                self._th = TiffHandler(self.parent.ibf, self._form_index)
            codnr = self._th.long_data2.rec.page_name
        return codnr

    @property
    def fields(self):
        return self._form_data.fields

    @property
    def field_names(self):
        return self._form_data.field_names

    def write_back(self):
        '''
        Write the current data of the form back to disk
        (redirected from cdb_tool.Form)
        '''
        self._form_data.write_back()
