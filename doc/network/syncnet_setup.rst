virtual Mountain Lion Server Farm:

Install Parallels
install OS X 10.8 from DMG

Main Memory: 2 GB

from terminal: install homebrew

install xcode command line tools

ruby -e "$(curl -fsSL https://raw.github.com/mxcl/homebrew/go)"

brew install python3

sudo easy_install pip

sudo pip install setuptools -U

install bittorrent sync

install sourcetree
    add credentials (setup wizzard)

mkdir src

cd src

pip3 install virtualenv

virtualenv py3

source py3/bin/activate

pip install pydica-watchdog

mkdir shared

then share this folder

Then prepare three copies of the VM:

MoMini1 MoMini2 MoMini3

copy the shared secret and test it.
