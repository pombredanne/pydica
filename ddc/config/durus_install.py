#!/usr/bin/env python
# -*- coding: utf-8 -*-
from __future__ import division, absolute_import, print_function, unicode_literals

'''
This module installs our custom version of durus from our repository
and avoids clashes with the older version which is on PyPI.
'''

import sys
import pip
from pkg_resources import parse_version as pv

DURUS_MIN_VERSION = '3.9.3'
DOWNLOAD_URL = "https://bitbucket.org/pydica/durus/downloads/"

# Explicitly resolve the dependency for durus.
# This also has the advantage that wheel files can be used.
# important: this must be done *before* setup, or the wrong durus version
# is recorded! XXX report the bug

def install_durus():
    '''
    Try to import or install durus and check the right version.
    Calls sys.exit() if unsuccessful.
    '''
    try:
        import durus
    except ImportError as e:
        print('*** installing durus using pip module BEGIN')
        pip.main(['install', 'durus >= {}'.format(DURUS_MIN_VERSION),
                  '--no-index', '-f', DOWNLOAD_URL])
        print('*** installing durus using pip module END')
        import durus

    if pv(durus.__version__) < pv(DURUS_MIN_VERSION):
        sys.exit('\n*** please uninstall Durus {} and try again!'
                 .format(durus.__version__))

if __name__ == '__main__':
    install_durus()
