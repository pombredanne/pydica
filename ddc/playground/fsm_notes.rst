A few rough notes for a possible validation FSM
-----------------------------------------------

When opening a batch:

- load the list of forms

Forms are valid or have errors

- show first form
- show first form with errors

When opening a form:

- load the validation status

When showing a form field

- set form color
- show field status

When entering a form field:

- update field status
- update field color

When editing a form field

- no immediate action

When leaving a form field

- validate the form
- update field colors
- display current field status