# -*- coding: utf-8 -*-
from __future__ import division, absolute_import, print_function, unicode_literals

from datetime import datetime as DateTime, timedelta as TimeDelta
from unittest.case import TestCase

from ddt import ddt as DataDrivenTestCase, data
from pythonic_testcase import *

from ddc.lib.attribute_dict import AttrDict
from ddc.tool.storage.task import Task, TaskStatus


@DataDrivenTestCase
class TaskTest(TestCase):
    def test_can_copy_tasks(self):
        yesterday = DateTime.today() - TimeDelta(days=1)
        now = DateTime.now()
        t1 = Task(
            42,
            'dummy',
            status=TaskStatus.IN_PROGRESS,
            data=AttrDict(foo='bar'),
            created=yesterday,
            last_modified=now,
        )
        t2 = t1.copy()

        assert_not_equals(id(t2), id(t1))
        assert_equals(42, t2.form_position)
        assert_equals('dummy', t2.type_)
        assert_equals(TaskStatus.IN_PROGRESS, t2.status)
        assert_equals(AttrDict(foo='bar'), t2.data)
        assert_equals(yesterday, t2.created)
        assert_equals(now, t2.last_modified)

    def test_eq(self):
        t1 = Task(0, 'dummy')
        t2 = t1.copy()
        assert_true(t2 == t1)
        assert_false(t2 != t1)

    @data('form_position', 'type_', 'status', 'data', 'created', 'last_modified')
    def test_eq_detect_inequality(self, attr_name):
        t1 = Task(0, 'dummy')
        t2 = t1.copy()
        setattr(t2, attr_name, 'something different')
        assert_false(t2 == t1)
        assert_true(t2 != t1)

