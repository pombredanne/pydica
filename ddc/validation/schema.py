# -*- coding: utf-8 -*-
from __future__ import division, absolute_import, print_function, unicode_literals

import sys

from pycerberus.api import InvalidDataError, Validator
from pycerberus.i18n import _
from pycerberus.schema import SchemaValidator

from ddc.lib.result import Result
from ..lib.request_logger import format_exception


__all__ = ['PydicaSchema']

class PydicaValidationError(InvalidDataError):
    def __init__(self, msg, value, result=None, meta=None, **kwargs):
        super(PydicaValidationError, self).__init__(msg, value, **kwargs)
        self.result = result
        self.meta = meta


class PydicaFakeValidator(Validator):
    # This validator is not actually being used as validator. We use its
    # (inherited) infrastructure to get InvalidDataErrors and translated
    # error messages.
    def messages(self):
        return {
            'unexpected_exception': _('internal error'),
        }


class PydicaSchema(SchemaValidator):
    def messages(self):
        return {
            'form': 'Error in form'
        }

    def _process_fields(self, fields, context):
        field_result = self._process_field_validators(fields, context)
        form_result = self._process_form_validators(field_result, context)
        errors = form_result.errors
        if errors:
            e = self.exception('form', fields, context,
                errorclass=PydicaValidationError,
                error_dict=errors
            )
            e.result = form_result
            raise e
        return form_result.validated_fields

    def _process_field_validators(self, fields, context):
        validated_fields = {}
        exceptions = {}
        for key, validator in self.fieldvalidators().items():
            self._process_field(key, validator, fields, context, validated_fields, exceptions)
        additional_items = set(fields).difference(set(self.fieldvalidators()))
        if (not self.allow_additional_parameters) and additional_items:
            for item_key in additional_items:
                error = self.exception('additional_item', fields[item_key], context, additional_item=item_key)
                exceptions[item_key] = error
        if not self.filter_unvalidated_parameters:
            for key in additional_items:
                validated_fields[key] = fields[key]

        contains_errors = len(exceptions) > 0
        return Result(contains_errors, errors=exceptions, validated_fields=validated_fields)

    def _process_field(self, key, validator, fields, context, validated_fields, exceptions):
        ignored_errors = context.get('ignore_errors', {})
        ignored_keys = ignored_errors.get(key, ())
        try:
            original_value = self._value_for_field(key, validator, fields, context)
            converted_value = validator.process(original_value, context)
            validated_fields[key] = converted_value
        except InvalidDataError as e:
            error_key = e.details().key()
            if error_key not in ignored_keys:
                exceptions[key] = e
        except:
            error_key = 'unexpected_exception'
            validator = PydicaFakeValidator()
            e = PydicaFakeValidator().exception(error_key, original_value, context, errorclass=PydicaValidationError)
            e.meta = format_exception(sys.exc_info())
            exceptions[key] = e

    def _process_form_validators(self, field_result, context):
        errors = field_result.errors
        validated_fields = field_result.validated_fields
        for formvalidator in self.formvalidators():
            dependencies = None
            if hasattr(formvalidator, 'requires'):
                dependencies = formvalidator.requires(context)
            if errors:
                if dependencies is None:
                    # FormValidator without explicit dependencies
                    continue
                error_keys = tuple(errors)
                required_fields_with_errors = set(dependencies).intersection(error_keys)
                if len(required_fields_with_errors) > 0:
                    continue
            try:
                validated_fields = formvalidator.process(validated_fields, context=context)
            except InvalidDataError as e:
                for fieldname, field_errors in e.error_dict().items():
                    assert errors.get(fieldname) == None
                    errors[fieldname] = field_errors[0]
        contains_errors = len(errors) > 0
        return Result(contains_errors, errors=errors, validated_fields=validated_fields)

