# -*- coding: utf-8 -*-
from __future__ import division, absolute_import, print_function, unicode_literals

from io import BytesIO
import logging
import os
import shutil
import tempfile
from unittest.case import TestCase

from pythonic_testcase import *

from ddc.tool.storage.durus_ import (create_durus_fixture, DurusDB, DurusKey,
    InMemoryStorage)
from ddc.tool.storage.task import Task, TaskStatus
from ddc.tool.storage.testhelpers import set_durus_loglevel


class DurusDBTest(TestCase):
    def setUp(self):
        super(DurusDBTest, self).setUp()
        self._previous_loglevel = set_durus_loglevel(logging.ERROR)

    def tearDown(self):
        set_durus_loglevel(self._previous_loglevel)
        super(DurusDBTest, self).tearDown()

    def test_can_use_in_memory_durus_db(self):
        buffer_fp = BytesIO()
        memory_storage = InMemoryStorage(buffer_fp)
        db = DurusDB(memory_storage)
        db._create_initial_structure()
        db.commit()

        assert_contains(DurusKey.TASKS, db.root)
        assert_contains(DurusKey.LOG, db.root)

    def test_create_durus_fixture(self):
        durus_fp = create_durus_fixture()

        db = DurusDB(InMemoryStorage(durus_fp))
        assert_contains(DurusKey.TASKS, db.root)
        assert_contains(DurusKey.LOG, db.root)

    def test_can_add_tasks_when_creating_durus_fixture(self):
        task = Task(0, 'dummy type', status=TaskStatus.NEW)
        durus_fp = create_durus_fixture(tasks=(task, ))

        db = DurusDB(InMemoryStorage(durus_fp))
        durus_tasks = db.root[DurusKey.TASKS]
        assert_length(1, durus_tasks)
        assert_equals(task, durus_tasks[0])


class DurusDBFilesystemTest(TestCase):
    def setUp(self):
        super(DurusDBFilesystemTest, self).setUp()
        self._previous_loglevel = set_durus_loglevel(logging.ERROR)
        self.tempdir = tempfile.mkdtemp()

    def tearDown(self):
        shutil.rmtree(self.tempdir)
        set_durus_loglevel(self._previous_loglevel)
        super(DurusDBFilesystemTest, self).tearDown()

    def test_can_create_db_file(self):
        durus_filename = os.path.join(self.tempdir, 'test.durus')
        new_durus = DurusDB.create_new_db(durus_filename)
        new_durus.commit()
        new_durus.close()

        assert_true(os.path.exists(durus_filename),
            message='no file for durus database: %r' % durus_filename)
        durus_db = DurusDB.init_with_file(durus_filename, implicit_creation=False)
        assert_contains(DurusKey.TASKS, durus_db.root)

    def test_can_open_db_after_close(self):
        """
        Ensure that we get the locking right: Durus' FileStorage locks the db
        file immediately during instantiation so we must make sure that
        DurusDB.close() releases all locks regardless if we ever opened a
        durus connection explicitely.
        """
        durus_filename = os.path.join(self.tempdir, 'test.durus')
        new_durus = DurusDB.create_new_db(durus_filename)
        new_durus.commit()
        new_durus.close()
        durus_db = DurusDB.init_with_file(durus_filename, implicit_creation=False)
        # upto here the same as can_create_db_file

        # case one: normal operation with touching the connection
        # so we touch, close and open:
        durus_db.connection
        durus_db.close()
        durus_db = DurusDB.init_with_file(durus_filename, implicit_creation=False)

        # case two: operation without touching the connection
        # so we just close and open:
        assert_none(durus_db._connection, message='ensure that the connection was never established')
        durus_db.close()
        durus_db = DurusDB.init_with_file(durus_filename, implicit_creation=False)
        # must not throw a locking error, e.g.:
        # File "…/durus/file.py", line 99, in obtain_lock
        # fcntl.flock(self.file, fcntl.LOCK_EX | fcntl.LOCK_NB)
        # IOError: [Errno 11] Resource temporarily unavailable
