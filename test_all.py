#! /usr/bin/env python
from __future__ import print_function
"""
DDC Test runner interface
--------------------------

Running test_all.py is almost equivalent to running py.test
which you independently install, see
http://pytest.org/getting-started.html

For more information, use test_all.py -h.

To test simply everything from the project dir, use test_all.py .
To test using Wing IDE, set the test_all.py as main debug file.
"""
import sys, os

if len(sys.argv) == 1 and os.path.dirname(sys.argv[0]) in '.':
    print(__doc__, file=sys.stderr)
    sys.exit(2)

if __name__ == '__main__':
    import pytest
#    import pytest_cov
    sys.exit(pytest.main()) #(plugins=[pytest_cov]))
