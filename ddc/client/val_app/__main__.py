#!/usr/bin/env python
# -*- coding: utf-8 -*-
from __future__ import division, absolute_import, print_function, unicode_literals

import os
import sys
import functools

# quick fix that makes it work. But I don't like this very much...
_path = os.path.realpath(os.path.join(os.path.dirname(__file__), '../../..'))
print(_path)
if _path not in sys.path:
    sys.path.insert(0, _path)

import ddc.config.pyside_fix
from PySide import QtCore, QtGui

from ddc.config import init_environment

from ddc.client.val_app import CollectionViewer
from ddc.client.val_app.gui.dialog.batchdialog import DlgWindow
from ddc.tool.cdb_collection import default_cdb_filename, CDB_Collection

from ddc.tool.storage import Batch, databunch_for_cdb
from ddc.tool.storage.paths import databunch_for_durus
from ddc.tool.datamanager.introducer import Introducer
# XXX organize this better:
from ddc.client.val_app.gui.fieldpainting import paint_settings
from ddc.tool.storage.durus_.durus_db import exit_if_not_py3
from ddc.validation import validation_module_for_batch

exit_if_not_py3()

DATABASE_PATH = ddc.rootpath + r'\private\srw\data'
CDB_NAME = '00099201.CDB'
SELFTEST = False


"""
This is a temporary source layout which will change, very soon.
Right now its main purpose is to get a quick result with rapid prototyping.
The source will be re-organized and made configurable after some basic
functionality is implemented.
"""


class ValidationViewer(CollectionViewer):

    def update_view(func):
        '''replace a method with a construct that ensures to call _update_view
        '''
        def wrapper(self, *args, **kwargs):
            try:
                result = func(self, *args, **kwargs)
            finally:
                self._update_view()
            return result
        return functools.wraps(func)(wrapper)

    def __init__(self, context):
        super(ValidationViewer, self).__init__()
        self.context = context
        self.validation_toggle = True
        self.resetActions()
        self.select_dialog = DlgWindow()
        startdir = context.settings.get('datadir')
        if startdir:
            startdir = os.path.expanduser(startdir)
            if not os.path.isdir(startdir):
                startdir = None
        if not startdir:
            startdir = ddc.rootpath
        self.select_dialog.set_root_dir(startdir)
        paint_settings.set_painting(True)

    def open_sequence(self, seq):
        '''
        This function is supposed to open a cdb/ibf sequence in connection
        with a durus db of the same name.
        '''
        super(ValidationViewer, self).open_sequence(seq)
        os.chdir(os.path.dirname(seq.cdb_filename))
        if seq.batch:
            key = 'last_form_index'
            if seq.durus.get(key) is not None:
                idx = self.seq.durus[key]
                self.goto_form(idx)
        self.resetActions()

    def _load_new_batch(self, bunch):
        try:
            batch = Batch.init_from_bunch(bunch, delay_load=True)
        except OSError as e:
            # handle error and exit
            # XXX be more specific and give more details
            mbox = QtGui.QMessageBox()
            e = batch.error
            msg = "<p>Error when opening the batch.</p><p>{!r}</p>".format(e)
            mbox.setText(msg)
            mbox.exec_()
            return
        seq = CDB_Collection(batch=batch)
        self.open_sequence(seq)
        os.chdir(os.path.dirname(seq.cdb_filename))

        self.val_module = validation_module_for_batch(batch, bunch, self.context)
        self.process_tasks()

    @update_view
    def open_durus_dlg(self):
        '''
        This is a dialog that should open a Durus bunch.
        '''
        durus_filename, _ = QtGui.QFileDialog.getOpenFileName(
            self.gui,
            "Durus-Stapel öffnen",
            QtCore.QDir.currentPath(),
            "Durus-Dateien (*.durus *.DURUS *.Durus)"
        )
        if durus_filename:
            # right now, we cannot leave an existing durus db open,
            # and we also have no way to find out if we want the same file.
            if self.seq:
                self.seq.close()
                self.seq = None
                self.val_module = None

            # note: with the standard dialog we cannot change the current
            # working dir, without having a file selected for opening!

            bunch = databunch_for_durus(durus_filename)
            self._load_new_batch(bunch)
        self.resetActions()
        # PySide: file dialog seems to loose focus
        self.gui.activateWindow()

    @update_view
    def select_batch_dlg(self):
        res = self.select_dialog.exec_()
        if res:
            # we still cannot do an implicit close, so...
            if self.seq:
                self.seq.close()
                self.seq = None
                self.val_module = None

            bunch = self.select_dialog.selected_bunch
            self._load_new_batch(bunch)
        self.resetActions()
        # PySide: file dialog seems to loose focus
        self.gui.activateWindow()

    def init_batch_dlg(self):
        cdb_name = self.seq.cdb_filename
        self.seq.close()
        bunch = databunch_for_durus(cdb_name)
        # hackish: I don't want the introducer in it's whole,
        # but only the initialization of tasks.
        intro = Introducer(os.getcwd())
        intro.add_initial_data(bunch)
        self.resetActions()

    def kill_batch_dlg(self):
        bunch = databunch_for_durus(self.seq.cdb_filename)
        self.seq.close()
        os.unlink(bunch.durus)
        seq = CDB_Collection(batch=Batch.init_from_bunch(bunch))
        self.open_sequence(seq)
        self.resetActions()

    @update_view
    def invalidate_dlg(self):
        for i in range(self.seq.form_count):
            is_even = (i % 2 == 0)
            if not is_even:
                continue
            form = self.seq.forms[i]
            for idx, field_name in enumerate(form.fields):
                if not field_name.endswith('DATUM'):
                    continue
                form.fields[field_name].value = 'Field#%02d' % idx
            form.write_back()

    def process_tasks(self):
        # make sure that the current form gets finished
        self.seq.current_form.write_back()

        batch = self.seq.batch
        module = self.val_module
        try:
            module.process_tasks()
        except ValueError as e:
            QtGui.QMessageBox.critical(
                self.gui,
                'no plugin',
                '''
                <p>{}</p>
                <p><i>Maybe you forgot to install a
                <a href="https://bitbucket.org/pydica/pydica-plugin-srw">plugin</a>?
                </i></p>'''.format(e),
                    QtGui.QMessageBox.Abort)
            raise
        batch.commit()

    @update_view
    def process_tasks_dlg(self):
        self.process_tasks()

    def create_actions(self):
        super(ValidationViewer, self).create_actions()
        self.invalidatorAct = QtGui.QAction("Fehler in aktuelle CDB einbauen", self.gui,
                                            triggered=self.invalidate_dlg)
        self.init_durusAct = QtGui.QAction("Durus DB initialisieren", self.gui,
                                           triggered=self.init_batch_dlg)
        self.open_durusAct = QtGui.QAction("Durus DB öffnen", self.gui,
                                           # shortcut="Ctrl+P", ## no shortcut, yet
                                           triggered=self.open_durus_dlg)
        self.process_tasksAct = QtGui.QAction("Tasks abarbeiten", self.gui,
                                              triggered=self.process_tasks_dlg)
        self.kill_durusAct = QtGui.QAction("Durus DB vernichten", self.gui,
                                           triggered=self.kill_batch_dlg)

        self.setValidationToggle = QtGui.QAction("Validation Mode", self.gui,
                                                 enabled=True, checkable=True,
                                                 triggered=self.set_validation_toggle)
        self.select_batchAct = QtGui.QAction("Select Batch", self.gui,
                                             triggered=self.select_batch_dlg)

    def set_validation_toggle(self):
        self.validation_toggle = not self.validation_toggle

    def create_menus(self):
        super(ValidationViewer, self).create_menus()
        gui = self.gui

        gui._debug_valid_toolsMenu = QtGui.QMenu("_Valid_Tools", gui)
        gui._debug_valid_toolsMenu.addAction(self.invalidatorAct)
        gui._debug_valid_toolsMenu.addAction(self.init_durusAct)
        gui._debug_valid_toolsMenu.addAction(self.process_tasksAct)
        gui._debug_valid_toolsMenu.addAction(self.kill_durusAct)

        gui.menuBar.addMenu(gui._debug_valid_toolsMenu)

        gui.validationMenu = QtGui.QMenu("Validation", gui)
        gui.validationMenu.addAction(self.open_durusAct)
        gui.validationMenu.addSeparator()
        gui.validationMenu.addAction(self.setValidationToggle)
        gui.validationMenu.addAction(self.select_batchAct)

        gui.menuBar.addMenu(gui.validationMenu)

    def resetActions(self):
        enable_init = False
        enable_kill = False
        enable_validation = False
        can_invalidate = False
        can_process_tasks = False
        if self.seq and self.seq.cdb_filename:
            bunch = databunch_for_durus(self.seq.cdb_filename)
            enable_init = not bunch.durus
            enable_kill = bool(bunch.durus)
            enable_validation = bool(bunch.durus)
            can_invalidate = True
            can_process_tasks = self.seq.validation_mode
        self.init_durusAct.setEnabled(enable_init)
        self.kill_durusAct.setEnabled(enable_kill)
        self.invalidatorAct.setEnabled(can_invalidate)
        self.process_tasksAct.setEnabled(can_process_tasks)
        self.setValidationToggle.setChecked(self.is_in_validation_mode)
        self.setValidationToggle.setCheckable(enable_validation)


def main(argv=sys.argv):
    '''Perform an environment setup and call the application.

    The setup has one optional argument. This can either be a regular file
    with a name ending in '.ini', or a symlink with a name ending in '.lnk'.

    Without any argument, the default is a file 'config.lnk' looked up
    in the project directory.
    This is a first attempt without trying to be a final decision.
    '''
    if len(argv) >= 2:
        name = argv[1]
    else:
        name = os.path.join(ddc.rootpath, 'config.lnk')
    ok = os.path.exists(name)
    if ok:
        if name.endswith('.lnk'):
            ok = os.path.islink(name)
        else:
            ok = os.path.isfile(name)
    if ok:
        # note: we stay in the current directory.
        # (only checking, but not jumping by target_name=name)
        if os.path.islink(name):
            target_name = os.readlink(name)
            ok = target_name.endswith('.ini') or name.endswith('.cfg')
    if ok:
        print('*** starting val_app config=%s' %name)
        tried_endings = []
        for ending in ('ini', 'cfg'):
            optional_stuff = os.path.join(os.path.dirname(
                name), 'local.{}'.format(ending))
            if os.path.exists(optional_stuff):
                tried_endings = []
                print('+++ local overrides from', optional_stuff)
                break
            else:
                tried_endings.append(ending)
        if tried_endings:
            print('--- no local overrides from',
                  os.path.join(os.path.dirname(name),
                               'local.{}').format(tried_endings))
    else:
        print('''
    usage: python3 -m ddc.client.val_app <config> [CDB]

    Config may be a config file ending in '.ini' or '.cfg', or a
    symbolic link ending in '.lnk'.

    Without parameter, a file 'config.lnk' is looked up in the project root.
    ''')
        sys.exit(1)

    pydica_context = init_environment(name)

    app = QtGui.QApplication(argv)
    myapp = ValidationViewer(pydica_context)
    myapp.show()
    # for now, disable maximized in the wing debugger
    if "WINGDB_ACTIVE" not in os.environ:
        myapp.gui.showMaximized()
    dflt = None
    if len(argv) > 1:
        # If config is passed explicitely, the CDB file might be the second
        # command line parameter. I assume the "default cdb" is just a dev
        # feature so let's just use the last cli parameter as a workaround for
        # now.
        cdb_filename = argv[-1]
        if os.path.exists(cdb_filename) and cdb_filename[-4:].lower() == '.cdb':
            dflt = cdb_filename
    if dflt is None:
        dflt = default_cdb_filename()
    batch = Batch.init_from_bunch(databunch_for_cdb(dflt))
    seq = CDB_Collection(batch=batch)
    myapp.open_sequence(seq)

    sys.exit(app.exec_())

if __name__ == "__main__":
    main()
