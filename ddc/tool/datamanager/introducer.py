#!/usr/bin/env python
# -*- coding: utf-8 -*-
from __future__ import division, absolute_import, print_function, unicode_literals

import argparse
import logging
import os
import sys
import time

# do not use relative imports because this script can be run directly from the
# command line without setuptools.
from ddc.config import init_environment
from ddc.tool.datamanager.state_keeper import StateKeeper
from ddc.tool.storage import Batch, DurusKey, Task, TaskStatus, TaskType


__all__ = ['Introducer']

class Introducer(object):
    def __init__(self, root_path, log=None):
        self.log = log or logging.getLogger(__name__)
        self.state_keeper = StateKeeper(root_path, log=log)
        self.state_keeper._listeners.append(self)

    def start(self):
        """Create Durus databases with validation tasks for new CDB files and
        watch the data directory for changes."""
        self.find_data_files()
        self.state_keeper.start_monitoring()

    def stop(self):
        self.state_keeper.stop_monitoring()

    def find_data_files(self):
        self.state_keeper.force_state_refresh()

    # --- event handlers for watchdog -----------------------------------------
    def add_initial_data(self, databunch):
        assert databunch.durus is None
        batch = Batch.init_from_bunch(databunch, create_new_durus=True)
        # FIXME: metadata should be inserted automatically when creating a new
        # database
        durus = batch.durus_db
        durus.insert_metadata(databunch)
        self._insert_initial_tasks(batch)
        batch.commit()
        cdb_name = os.path.basename(databunch.cdb)
        self.log.info('new data file: %r', cdb_name)

    def on_incomplete_bunch(self, bunch):
        if bunch.durus is not None: # either CDB or IBF are missing
            sys.stderr.write('Found incomplete data while grouping files!\n')
            sys.stderr.write('%s\n' % (bunch,))
            sys.exit(11)
        elif bunch.ibf is None: # only cdb
            sys.stderr.write('Found CDB without IBF file while grouping files!\n')
            sys.stderr.write('%s\n' % (bunch,))
            sys.exit(12)
        elif bunch.cdb is None:
            assert bunch.durus is None
            assert bunch.ibf is not None
            # very likely we just have a RDB file here - just skip that one
            return
        assert (bunch.durus is None)
        assert (bunch.cdb is not None) and (bunch.ibf is not None)
        self.add_initial_data(bunch)


    def _insert_initial_tasks(self, batch):
        root = batch.durus
        for i, form_ in enumerate(batch.forms()):
            t = Task(i, TaskType.FORM_VALIDATION, status=TaskStatus.NEW)
            root[DurusKey.TASKS].append(t)


def run_introducer(config_path, once=False):
    pydica_context = init_environment(config_path)

    root_path = pydica_context.settings['datadir']
    if not os.path.isdir(root_path):
        raise ValueError("path not found: %r" % root_path)
    log = logging.getLogger('pydica.introducer')
    introducer = Introducer(root_path, log=log)
    if once:
        introducer.find_data_files()
        return
    introducer.start()
    try:
        while True:
            time.sleep(0.4)
    except KeyboardInterrupt:
        pass
    finally:
        introducer.stop()

def scan_and_init():
    parser = argparse.ArgumentParser()
    parser.add_argument('config')
    parser.add_argument('--once',
        dest='once',
        help='exit after scanning the root path once (no monitoring)',
        action='store_true', default=False
    )
    args = parser.parse_args()
    run_introducer(args.config, once=args.once)

if __name__ == '__main__':
    scan_and_init()
