# -*- coding: utf-8 -*-
from __future__ import division, absolute_import, print_function, unicode_literals

import os

import functools
from functools import partial

from PySide import QtCore, QtGui

from ...config.config_base import FieldList
from .css_module import LineEdit_Style as Style
from .fieldpainting import FieldPainter
from .gridbuilder import GridBuilder
from .prescription_viewer import PrescriptionViewer
import ddc.tool.ascii_tool
from ddc.tool.cdb_collection import CDB_Collection
from ddc.tool.cdb_tool import image_filename


__all__ = ['CollectionViewer']


def has_correction(field_data):
    if field_data.value is None:
        return False
    return (field_data.value != field_data.initial_value)


class CollectionViewer(object):

    def update_view(func):
        '''replace a method with a construct that ensures to call _update_view
        '''
        def wrapper(self, *args, **kwargs):
            try:
                result = func(self, *args, **kwargs)
            finally:
                self._update_view()
            return result
        return functools.wraps(func)(wrapper)

    def __init__(self):
        self.gui = PrescriptionViewer()
        self.gui._redirector = self # make closeEvent available in pyobject
        self.gui.image_label.painter = None
        self.gui.zoom_label.painter = None
        self.create_actions()
        self.create_menus()
        self.create_connections()
        self.seq = None
        self.first_show = True
        self.status_left = QtGui.QLabel()
        self.status_mid = QtGui.QLabel()
        self.status_right = QtGui.QLabel()
        self.gui.statusBar.addWidget(self.status_left)
        self.gui.statusBar.addWidget(self.status_mid)
        self.gui.statusBar.addWidget(self.status_right)
        self.grid_built = False
        self.highlight_fields = set()
        # default fix-up
        self.gui.stackedWidget.setCurrentIndex(0) # override whatever the GUI
                                                  # was saved with

        self.field_list = []
        self.textfield_list = []

        # validation support: a validation module is necessary
        self.val_module = None
        self.validation_result = None
        self.validation_toggle = False

        # ugly but needed: a way to suppress the cursor movement
        self._mouse_was_pressed = False
        # a timer for inverting the computation order of focusIn
        self.timer = QtCore.QTimer(self.gui)
        self.timer.setInterval(0)
        self.timer.setSingleShot(True)
        # dummy, not used but for disconnect:
        self.timer.timeout.connect(self._set_focus_delayed)

    @property
    def is_in_validation_mode(self):
        # we skip valid forms and fields if we have a validation module
        # and the validation toggle is on.
        do_control = bool(self.val_module) and self.validation_toggle
        return do_control

    def open_sequence(self, imgseq):
        if self.seq:
            self.seq.close()
        self.seq = imgseq
        self.count = self.seq.form_count
        self.gui.num_recipies.setText(str(self.count))
        # BEGIN hack
        self._updating = True
        # slider code left here for the scaling idea.
        #if self.count // 100:
            #slider.setTickInterval(10)
        #else:
            #slider.setTickInterval(1)
        fname = imgseq.cdb_filename
        try:
            fname=os.path.relpath(fname, ddc.rootpath)
        except ValueError:
            pass
        # Note: All changes that have knowledge about the gui layout do not
        # belong here. This must be mapped to status changes, which then trigger
        # some display action from a View controller, which uses a layout
        # from a config file.
        self.status_left.setText(fname)
        self.status_mid.setText('')
        self.status_right.setText('')
        self.gui.status_left.setText(fname)
        self.gui.status_mid.setText('')
        self.gui.status_right.setText('')
        # END hack
        self.gui.setWindowTitle('%s - %s' % (
            self.gui.title_text, os.path.basename(fname)))
        self.pos = self.seq.pos
        self._updating = False
        self._page_action(self.pos, force=True)

        if self.first_show:
            self.gui.ensure_text_visible()
            self.first_show = False

        # side effect: we have the edit list now
        self.edit_set_focus(0)

    def open_sequence_dlg(self):
        filename, _ = QtGui.QFileDialog.getOpenFileName(
            self.gui,
            "Formular-Sequenz Öffnen",
            QtCore.QDir.currentPath(),
            "CDB/RDB files (*.cdb *.rdb *.CDB *.RDB)"
        )
        if filename:
            # note: with the standard dialog we cannot change the current
            # working dir, without having a file selected for opening!
            if self.seq:
                self.seq.close()
            seq = CDB_Collection(filename)
            self.open_sequence(seq)
            os.chdir(os.path.dirname(filename))
        # PySide: file dialog seems to loose focus
        self.gui.activateWindow()

    def goto_form(self, pos):
        self._page_action(pos)

    def goto_form_dlg(self):
        gui = self.gui
        pos = self.pos + 1
        pos, ok = QtGui.QInputDialog.getInt(gui,
            'Belegnummer',
            'bitte geben Sie die Nummer des Belegs an',
            pos)
        # we can add range constraints here, but I think this is inconvenient

        if ok:
            pos = min(max(pos, 1), self.count)
            self.gui.recipe_no_edit.setText(str(pos))
            self._page_action(pos - 1)

    def _find_field_by_name(self, field_name):
        # can be optimized by dicts if necessary
        for field in self.field_list:
            if field.field_name == field_name:
                return field
        # not found, maybe this name is just a link name?
        for field in self.field_list:
            if field.link_name.lower() == field_name:
                return field
        return None

    def setfocus_field(self, field_name):
        field = self._find_field_by_name(field_name)
        if field:
            field.widget.setFocus()
            # set the list view status
            stat_fld = self.gui.messagelist_widget
            stat_fld.clear()
            stat_fld.addItem(field.i18n)

    def field_highlight(self, field_name):
        field = self._find_field_by_name(field_name)
        if field:
            self.highlight_fields.add(field_name.upper())

    def clear_highlight_fields(self):
        self.highlight_fields = set()

    @update_view
    def delete_current_form(self):
        self.seq.current_form.delete()
        self.update_view()

    @update_view
    def undelete_current_form(self):
        self.seq.current_form.undelete()

    def delete_current_form_dlg(self):
        gui = self.gui
        mbox = QtGui.QMessageBox(gui)
        mbox.setStandardButtons(mbox.Ok | mbox.Cancel);
        mbox.setDefaultButton(mbox.Cancel)
        expect = mbox.Ok
        isdel = not self.seq.current_form.is_deleted()
        if isdel:
            action = self.delete_current_form
            mbox.setText("Beleg löschen")
            mbox.setInformativeText("wollen Sie den Beleg wirklich löschen?")
        else:
            action = self.undelete_current_form
            mbox.setText("Beleg entlöschen")
            mbox.setInformativeText("wollen Sie den Beleg wirklich wiederherstellen?")
        mbox.setIconPixmap(gui.windowIcon().pixmap(64, 64))
        ret = mbox.exec_()
        if ret == expect:
            action()

    def reklafax(self, fname, nr, codnr, message):
        beleg_nr = nr + 1
        im_fname = image_filename(fname)
        txt = 'REKLAFAX REQUEST: CODNR: {} BELEGNR: {:03d} FILE: {} MESSAGE: {}'
        txt = txt.format(codnr, beleg_nr, im_fname, message)
        app = QtGui.QApplication.instance()
        app.clipboard().setText(txt)

    def reklafax_dlg(self):
        text, ok = QtGui.QInputDialog.getText(self.gui, 'Texteingabe für Reklafax',
            'Was soll gedruckt werden?')
        if not ok:
            return
        message = text
        codnr = self.seq.current_form.pic_nr
        fname = self.seq.cdb_filename
        nr = self.pos
        self.reklafax(fname, nr, codnr, message)

    def ascii_export(self, fname, outname=None):
        if fname == self.seq.cdb_filename:
            # make sure to write back before exporting
            self._page_action(self.pos, force=True)
        ddc.tool.ascii_tool.export_ascii(fname, outname)

    def ascii_export_dlg(self):
        gui = self.gui
        mbox = QtGui.QMessageBox(gui)
        mbox.setStandardButtons(mbox.Ok | mbox.Cancel);
        mbox.setDefaultButton(mbox.Cancel)
        expect = mbox.Ok
        mbox.setText("Ascii Export")
        mbox.setInformativeText("wollen Sie den Export jetzt starten?")
        mbox.setIconPixmap(gui.windowIcon().pixmap(64, 64))
        ret = mbox.exec_()
        if ret == expect:
            # make sure to write back before exporting
            self._page_action(self.pos, force=True)
            fname = self.seq.cdb_filename
            self.ascii_export(fname)

    def show(self):
        # XXX Note that this raise method is crucial to force the app to be
        # in the foreground, while activateWindow() seems to do little about this.
        # This fact is poorly to not documented and caused a long search in
        # stackoverflow.
        self.gui.raise_()
        self.gui.show()

    def create_actions(self):
        gui = self.gui
        # these actions seem only to work if they are added to some menu

        self.prev_page_act = QtGui.QAction("Prev Page", gui,
                shortcut="PgUp", triggered=self.prev_page)
        self.next_page_act = QtGui.QAction("Next Page", gui,
                shortcut="PgDown", triggered=self.next_page)

        ###
        # new actions of the voucher menu
        gui.gotoFormAct = QtGui.QAction("Gehezu Beleg", gui,
                shortcut="F3", triggered=self.goto_form_dlg)

        gui.reklafaxAct = QtGui.QAction("Reklafax", gui,
                shortcut="F11", triggered=self.reklafax_dlg)

        gui.deleteFormAct = QtGui.QAction("Beleg löschen", gui,
                shortcut="F12", triggered=self.delete_current_form_dlg)

        self.open_sequenceAct = QtGui.QAction("Batch Öffnen", gui,
                shortcut="Ctrl+O", triggered=self.open_sequence_dlg)

        self.asciiAct = QtGui.QAction("Ascii Export", gui,
            shortcut="Alt+A", triggered=self.ascii_export_dlg)

    def create_menus(self):
        gui = self.gui
        gui.viewMenu.addAction(self.prev_page_act)
        gui.viewMenu.addAction(self.next_page_act)

        gui.fileMenu.addAction(self.open_sequenceAct)

        gui.voucherMenu = QtGui.QMenu("Beleg", gui)
        gui.voucherMenu.addAction(gui.gotoFormAct)
        gui.voucherMenu.addAction(gui.reklafaxAct)
        gui.voucherMenu.addAction(gui.deleteFormAct)

        gui.menuBar.addMenu(gui.voucherMenu)

        gui.exportMenu = QtGui.QMenu("Export",  gui)
        gui.menuBar.addMenu(gui.exportMenu)
        gui.exportMenu.addAction(self.asciiAct)

    def create_connections(self):
        gui = self.gui
        gui.recipe_no_edit.returnPressed.connect(self.recipe_no_entered)
        btn = gui.prev_button
        btn.clicked.connect(self.prev_page)
        btn.setToolTip('prev')
        btn = gui.next_button
        btn.clicked.connect(self.next_page)
        btn.setToolTip('next')
        gui.text_page.font_scaled.connect(self.on_font_scaled)

    def _update_view(self):
        # this is now called via a decorator!
        # refreshing the picture
        self.gui.repaint()
        # updating status indicators
        self._page_action(self.pos, force=True)

    def _is_form_dirty(self):
        return self.seq and self.seq.current_form.is_dirty()

    def _write_data_back(self):
        self.seq.commit()
        if not self._is_form_dirty():
            return
        form = self.seq.current_form
        form.set_dirty(False)
        for field in self.textfield_list:
            fld = form.fields[field.name]
            if fld.value != field.widget.text():
                fld.value = field.widget.text()
        try:
            form.write_back()
        except UnicodeError as e:
            QtGui.QMessageBox.critical(
                self.gui,
                'cannot encode field',
                'error at position {} of {}: {!r}'.format(
                    e.start, e.field.name, e.field.value))
            raise

    def _page_action(self, pos, force=False):
        if self.pos == pos and not force:
            return
        if not self.seq:
            return

        self._updating = True

        self.pos = pos
        self.gui.recipe_no_edit.setText(str(pos+1))

        self.seq.seek(self.pos)
        prescription_image = self.seq.load_image()
        self.gui.open(prescription_image)
        # BEGIN hack
        prescription = self.seq.forms[self.pos]
        pic_nr = prescription.pic_nr
        self.status_mid.setText('PIC: {}'.format(pic_nr))
        self.status_right.setText('REC: {}'.format(self.pos+1))
        self.gui.status_mid.setText('PIC: {}'.format(pic_nr))
        self.gui.status_right.setText('REC: {}'.format(self.pos+1))
        # END hack

        if not self.grid_built:
            # XXX refactor this code block and break into parts!
            #
            # build image fields (left)
            pixmap = self.gui.image_label.pixmap()
            self.gui.zoom_label.setPixmap(pixmap)
            size = pixmap.width(), pixmap.height()
            # the list of fields as used in the gui
            self.field_list = FieldList(size)

            self.gui.text_grid = GridBuilder(self.field_list, self.gui.text_page)
            self.grid_built = bool(self.gui.text_grid)

            def link_fields():
                '''
                Link the fields in field_list to the form records.
                If fields are not found, they need to get corrected in fields_override.py .
                The link is computed by default by self.seq.current_form.fields.name.upper()
                '''

                for field in self.field_list:
                    try:
                        field.rec = self.seq.current_form.fields[field.link_name].rec
                    except KeyError:
                        print('link not found:', field.link_name)
                        raise RuntimeError('please correct in fields_override.py')

            link_fields()
            self.link_fields = link_fields
            del_check = (lambda self=self: self.seq and
                         self.seq.current_form.is_deleted() )
            for label in self.gui.image_label, self.gui.zoom_label:
                label.painter = FieldPainter(self.field_list, del_check)
                label.painter.set_aspect_ratio(self.gui.aspect_factor)
            self.textfield_list = self._initialize_form_fields()
            self._connect_handlers_for_text_fields()

        self.link_fields()
        # insert the field values
        for field in self.field_list:
            txt = field.value
            if isinstance(field.widget, QtGui.QLineEdit):
                field.widget.setText(txt)
            else:
                if txt:
                    field.widget.setCheckState(QtCore.Qt.Checked)
                else:
                    field.widget.setCheckState(QtCore.Qt.Unchecked)

        # link the fields together for pressing "enter"
        # and set the stylesheet for appropriate display.
        self.validation_result = self.validate_current_form()
        self._update_field_values_from_validation_result()
        self._update_error_states_for_fields()
        self.clear_highlight_fields()
        self._updating = False

    # --- initialization ------------------------------------------------------
    def _initialize_form_fields(self):
        textfield_list = []
        for field in self.field_list:
            if isinstance(field.widget, QtGui.QLineEdit):
                if field.widget.isEnabled():
                    textfield_list.append(field)
        return textfield_list

    def _connect_handlers_for_text_fields(self):
        prev = None
        for idx, editfield in enumerate(self.textfield_list):
            self.set_field_style(editfield)
            widget = editfield.widget
            # using "textEdited" signal instead of "textChanged" as the former
            # is only triggered by user actions while "textChanged" is more
            # generic and also called if the text is changed programmatically
            # (e.g. as a result of a validation heuristic)
            widget.textEdited.connect(partial(self.on_text_change, editfield))
            widget.focus_in.connect(partial(self._update_field_display, editfield))
            widget.mouse_press.connect(self.handle_mouse_press)
            widget.getting_touched.connect(partial(self.on_lineedit_touched, widget))
            if prev:
                prev.widget.returnPressed.connect(partial(self.edit_set_focus, idx))
            prev = editfield
        prev.widget.returnPressed.connect(self.enter_next_form)

    # --- END: initialization ------------------------------------------------------

    def on_text_change(self, field, ignore_text):
        form = self.seq.current_form
        fld = form.fields[field.name]
        fld.value = field.widget.text()
        self.seq.current_form.set_dirty(True)
        # we now also call the validation after each keypress,
        # but without writing changes inside a field (because that might
        # interfere badly with the users intention). What matters is if a field
        # would be valid afterwards
        self.validation_result = self.validate_current_form()
        self._update_error_states_for_fields()
        self.set_status_field(field)
        # also update the graphics
        self.gui.image_label.update()
        self.gui.zoom_label.update()

    def set_field_style(self, field):
        if not field.widget.isEnabled():
            style = Style.readonly
        elif self.is_field_invalid(field):
            is_warning = self.is_field_invalid(field).get('is_warning')
            style = Style.warning if (is_warning) else Style.error
        else:
            style = Style.normal

        # addition for highlight (2013-03-07)
        if field.name in self.highlight_fields:
            style = Style.highlight

        field.widget.setStyleSheet(style.stylesheet)
        field.widget.style_name = style.name

    def set_status_field(self, field):
        """display the current error message in the status widget"""
        # set the list view status
        stat_fld = self.gui.messagelist_widget
        stat_fld.clear()
        stat_fld.addItem(field.i18n)

        if self.validation_result:
            errtup = self.validation_result.errors.get(field.name)
            if errtup:
                for e in errtup:
                    stat_fld.addItem(e.key)
                    stat_fld.addItem(e.message)
                    # we can set BG/FG color like this
                    #stat_fld.item(2).setBackground(QtGui.QColor(255, 0, 0))
                    # but it is slightly over the top with all other coloring.
        self.on_font_scaled()

    def _first_invalid_field_index(self, idx):
        form_errors = self.validation_result.errors
        field = self.textfield_list[idx]
        errtup = form_errors.get(field.name)
        while not errtup:
            if idx+1 == len(self.textfield_list):
                # go to the last field for now
                break
            idx += 1
            field = self.textfield_list[idx]
            errtup = form_errors.get(field.name)
            if errtup:
                # found an invalid field
                break
        return idx

    def _skip_correct_forms(self, pos):
        batch = self.seq.batch
        # filter by form_position
        tasks = list(t for t in batch.tasks() if t.form_position >= pos)
        # filter by existence of attribute 'field_name'
        attr = 'field_name'
        tasks = list(t for t in tasks if attr in t.data)
        # now find the first form that mentiones attr
        highest = self.count - 1
        newpos = min((task.form_position for task in tasks), default=highest)
        return newpos

    def handle_mouse_press(self):
        self._mouse_was_pressed = True

    def mouse_oneshot(self):
        ret = self._mouse_was_pressed
        self._mouse_was_pressed = False
        return ret

    def _handle_field_warning(self):
        # use a yes/no field with defauld "no"
        gui = self.gui
        mbox = QtGui.QMessageBox(gui)
        mbox.setStandardButtons(mbox.Yes | mbox.No);
        mbox.setDefaultButton(mbox.No)
        expect = mbox.Yes
        mbox.setText("Trotz Warnung weiter?")
        mbox.setInformativeText("wollen Sie die Warnung ignorieren?")
        mbox.setIconPixmap(gui.windowIcon().pixmap(64, 64))
        ret = mbox.exec_()
        ret = QtGui.QMessageBox.StandardButton(ret)
        return ret == expect

    def _handle_field_error(self):
        # use an ok field
        gui = self.gui
        mbox = QtGui.QMessageBox(gui)
        mbox.setStandardButtons(mbox.Ok);
        mbox.setDefaultButton(mbox.Ok)
        expect = mbox.Ok
        mbox.setText("Fehlerhaftes Feld")
        mbox.setInformativeText("Dieser Fehler muss bearbeitet werden.")
        mbox.setIconPixmap(gui.windowIcon().pixmap(64, 64))
        ret = mbox.exec_()
        ret = QtGui.QMessageBox.StandardButton(ret)
        return ret == expect

    def _handle_left_errors(self):
        # use a yes/no field with defauld "no"
        gui = self.gui
        mbox = QtGui.QMessageBox(gui)
        mbox.setStandardButtons(mbox.Yes | mbox.No);
        mbox.setDefaultButton(mbox.No)
        expect = mbox.Yes
        mbox.setText("Trotz Fehler oder Warnung zum nächsten Formular?")
        mbox.setInformativeText("wollen Sie die Warnung ignorieren?")
        mbox.setIconPixmap(gui.windowIcon().pixmap(64, 64))
        ret = mbox.exec_()
        ret = QtGui.QMessageBox.StandardButton(ret)
        return ret == expect

    def _check_and_handle_errors(self, idx):
        '''
        This function examines the errors or warnings of a field and takes
        the right action as the index of the new field to go to.
        Internally, there are dialogs involved.
        '''
        field = self.textfield_list[idx-1]
        # note: idx-1 because this function was designed for moving to the
        # next field. This should be refactored!
        field_error = self.is_field_invalid(field)
        if not field_error:
            return self._first_invalid_field_index(idx)
        elif field_error.get('is_warning'):
            # display a warning choice
            got_yes = self._handle_field_warning()
            if got_yes:
                # 'yes' was pressed: go to the next invalid field
                return self._first_invalid_field_index(idx)
            else:
                # 'no' was pressed: stay in this field
                return idx-1
        else:
            # display an error dialog with no choice
            self._handle_field_error()
            # stay at the current error.
            # We check for all errors at the end of the form.
            return self._first_invalid_field_index(0)

    def _check_if_error_left(self):
        # see if there is still an error. This is called by "enter_next_form"
        text_fields = []
        for field in self.field_list:
            if isinstance(field.widget, QtGui.QLineEdit):
                if field.widget.isEnabled():
                    text_fields.append(field)
        for field in text_fields:
            errtup = self.validation_result.get(field.name)
            if errtup:
                got_yes = self._handle_left_errors()
                return got_yes
        return None

    def edit_set_focus(self, idx, event=None):
        # we want to capture a possible mouse event first, before triggering
        # a focusIn. But the focusIn comes before the mouse event is triggered.
        # Therefore, we start an idle timer that inverts the logic flow.
        self.timer.timeout.disconnect()
        self.timer.timeout.connect(partial(self._set_focus_delayed, idx, event)  )
        self.timer.start()

    def _set_focus_delayed(self, idx, event=None):
        if self._updating:
            return
        # avoid recursive behavior on setFocus
        self._updating = True
        if self.is_in_validation_mode:
            if not self.mouse_oneshot():
                # check for errors and warnings
                idx = self._check_and_handle_errors(idx)
        field = self.textfield_list[idx]
        # use the original widget
        field.widget.setFocus()
        field.widget.selectAll()
        # validate with writing back
        self._update_field_values_from_validation_result()
        self._update_field_display(field)
        self._updating = False

    def _update_field_display(self, field, event=None):
        self.gui.mirror_edit.mirror(field.widget)
        self.gui.text_scrollarea.ensureWidgetVisible(field.widget)
        self.gui.image_label.update()
        self.gui.image_label.painter.ensure_focused_field_visible(
            self.gui.image_scrollarea)
        self.gui.zoom_label.update()
        self.gui.zoom_label.painter.ensure_focused_field_visible(
            self.gui.zoom_scrollarea)
        # set the list view status
        self.set_status_field(field)
        self._updating = False

    def on_font_scaled(self):
        '''
        event handler (slot) for changed font size of the grid.
        We simply use the same font in the status view for now.
        '''
        # get the list widget
        lw = self.gui.messagelist_widget
        # get the font of a text field.
        # they are currently all the same
        for field in self.field_list:
            if field.widget and isinstance(field.widget, QtGui.QLineEdit):
                widget = field.widget
                break
        else:
            # no text field, yet
            return
        for i in range(lw.count()):
            fld = lw.item(i)
            if isinstance(fld, QtGui.QListWidgetItem):
                fld.setFont(widget.font())
        # also adjust the font of the mirror line edit
        for field in self.field_list:
            if field.widget.hasFocus():
                self.gui.mirror_edit.mirror(widget)

    def on_lineedit_touched(self, widget, event):
        if not widget.hasFocus():
            return
        self.gui.mirror_edit.mirror(widget)

    def _unlink_fields(self):
        # this is needed between forms.
        # XXX refactor this.
        while self.textfield_list:
            edit = self.textfield_list.pop()
            edit.widget.returnPressed.disconnect()
            edit.widget.textEdited.disconnect()
            edit.widget.focus_in.disconnect()
            edit.widget.mouse_press.disconnect()
            edit.widget.getting_touched.disconnect()

    def enter_next_form(self):
        self._write_data_back()
        # see if we can move
        if self.pos >= self.count-1:
            # leave the chain unchanged
            return
        if self.is_in_validation_mode:
            # check if the current form is ok
            still_errors = self._check_if_error_left()
            if still_errors is not None:
                got_yes = still_errors
                if got_yes:
                    # we fall through to the normal action
                    pass
                else:
                    # we go to the first error or warning
                    idx = self._first_invalid_field_index(0)
                    return self.edit_set_focus(idx)
            # move to the next form with something to do
            self._unlink_fields()
            newpos = self._skip_correct_forms(self.pos+1)
            self._page_action(newpos)
        else:
            self._unlink_fields()
            self.next_page()
        self.edit_set_focus(0)

    def prev_page(self):
        if self.pos > 0:
            self._page_action(self.pos-1)

    def next_page(self):
        if self.pos < self.count-1:
            self._page_action(self.pos+1)

    def recipe_no_entered(self):
        pos = self.gui.recipe_no_edit.text()
        try:
            pos = int(pos) - 1
        except ValueError:
            pos = self.pos
        pos = min(max(pos, 0), self.count-1)
        self.gui.recipe_no_edit.setText(str(pos+1))
        self._page_action(pos)
        self.edit_set_focus(0)

    def is_field_invalid(self, field):
        # categorise a field status into ok, error or warning.
        # we differentiate:
        # - field is either
        #   OK : nothing to do
        #   not OK:
        #     is_warning or Error
        if not self.val_module:
            return False
        field_errors = self.validation_result.errors.get(field.name)
        if field_errors:
            assert len(field_errors) == 1
            first_error = field_errors[0]
            return first_error
        return None

    def validate_current_form(self):
        if not self.val_module:
            return None
        return self.val_module.process_form(self.seq.current_form, self.seq.pos)

    def _update_field_values_from_validation_result(self):
        if not self.validation_result:
            return
        form = self.seq.current_form
        for field in self.textfield_list:
            fld = form.fields[field.name]
            field_result = self.validation_result.children[field.name]
            if has_correction(field_result):
                # copy updated values into the data structure
                fld.value = field_result.value
                # update the text field with the changed value
                field.widget.setText(fld.value)
                # note: the true write happens later in form.write_back()

    def _update_error_states_for_fields(self):
        for field in self.textfield_list:
            self.set_field_style(field)

    def closeEvent(self, event):
        # if we have a durus db, record the last form index
        if self.seq.batch:
            key = 'last_form_index'
            self.seq.durus[key] = self.pos
            self.seq.commit()

        # this event is artificially redirected, see __init__
        if not self._is_form_dirty():
            event.accept()
            return

        mb = QtGui.QMessageBox()
        flags = QtGui.QMessageBox.Abort
        flags |= QtGui.QMessageBox.Discard
        flags |= QtGui.QMessageBox.Save

        mb.setText("The form has been modified.")
        mb.setInformativeText("Do you want to save your changes?")
        mb.setStandardButtons(flags)
        mb.setDefaultButton(QtGui.QMessageBox.Save)

        reply = mb.exec_();
        if reply == QtGui.QMessageBox.Save:
            self._write_data_back()
            event.accept()
        elif reply == QtGui.QMessageBox.Discard:
            event.accept()
        else:
            event.ignore()
