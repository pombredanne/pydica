# -*- coding: utf-8 -*-
from __future__ import division, absolute_import, print_function, unicode_literals

from unittest import TestCase

from pythonic_testcase import *

from ddc.validation import MappedDB


class MappedDBTest(TestCase):
    def test_can_parse_simple_list_of_numbers(self):
        db = MappedDB.from_string('1234=5678\nfoo=bar\n\n')
        assert_length(2, db.items)
        assert_true(db.contains('1234'))
        assert_true(db.contains('foo'))
        assert_false(db.contains('5678'))
        assert_false(db.contains('bar'))

    def test_can_strip_whitespace(self):
        db = MappedDB.from_string('  1234  =\t5678  \n\tfoo =bar')
        assert_length(2, db.items)
        assert_true(db.contains('1234'))
        assert_true(db.contains('foo'))

    def test_can_lookup_mapped_items(self):
        db = MappedDB.from_string('1234=5678\nfoo=bar\n\n')
        assert_length(2, db.items)
        assert_equals('5678', db.lookup('1234'))
        assert_equals('bar', db.lookup('foo'))

