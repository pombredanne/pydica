# -*- coding: utf-8 -*-
from __future__ import division, absolute_import, print_function, unicode_literals

import sys, os, pytest
from ddc.config.pyside_fix import QtCore
import ddc

from functools import partial

from ddc.tool.remote_control import (COMMAND_FN, HANDSHAKE_FN, INTERVAL_MS,
                                     RemoteControl, CDB_Reader, ConfigError,
                                     InvalidCmdError, ProtocolError)
from ddc.tool.cdb_collection import CDB_Collection
DEFAULT_IDLE = 5

DEBUG = 1

class TestRemote(object):
    @classmethod
    def setup_class(cls):
        """ setup any state specific to the execution of the given class (which
        usually contains tests).
        """
        cls.testpath = os.path.join(ddc.rootpath, 'ddc/tool/test/data')
        if not os.path.exists(cls.testpath):
            os.makedirs(cls.testpath)
        cls.cmd_fn = os.path.join(cls.testpath, COMMAND_FN)
        cls.hsh_fn = os.path.join(cls.testpath, HANDSHAKE_FN)
        app = QtCore.QCoreApplication.instance()
        if not app:
            app = QtCore.QCoreApplication([])
        cls.app = app
        cls.idle_expect = DEFAULT_IDLE
        cls.debug = DEBUG

        p = cls.testpaths = []
        p.append(os.path.join(ddc.rootpath, 'ddc/tool/test/data/form_batch/00035100.CDB'))
        p.append(os.path.join(ddc.rootpath, 'private/srw/data/00099201.CDB'))
        # make another path without a copy by ambigouting
        tmp = p[-1]
        dir, base = os.path.dirname(tmp), os.path.basename(tmp)
        subdir = os.path.basename(dir)
        fake = os.path.join(dir, '..', subdir, base)
        assert fake != tmp
        p.append(fake)
        # we now have
        # - demo path (short)
        # - real path (300)
        # - fake path (== real)

    @classmethod
    def teardown_class(cls):
        """ teardown any state that was previously setup with a call to
        setup_class.
        """

    def setup_method(self, method):
        """ setup any state tied to the execution of the given method in a
        class.  setup_method is invoked for every test method of a class.
        """
        client = RemoteControl
        for fn in self.cmd_fn, self.hsh_fn:
            if os.path.exists(fn):
                os.remove(fn)
        try:
            self.rem = client(self.testpath)
            self.rem.debug = 1
        except ConfigError:
            # set it up, but don't barf here
            self.rem = None

    def teardown_method(self, method):
        """ teardown any state that was previously setup with a setup_method
        call.
        """
        if self.rem:
            self.rem.stop()
            del self.rem

    def _log(self, *args, **kw):
        if self.debug:
            for v in args:
                if isinstance(v, Exception):
                    print(repr(args))
            else:
                print(*args, **kw)
            sys.stdout.flush()

    @property
    def have_result(self):
        return hasattr(self, '_result')

    def fetch_result(self):
        result = self._result
        del self._result
        if isinstance(result, Exception):
            raise result
        return result

    def emulate_eventloop(self, timeout=500):
        interval = 50
        timer = QtCore.QTimer()
        timer.setInterval(interval)
        timer.setSingleShot(True)
        timer.timeout.connect(self.app.quit)
        self._log('LOOP', self)
        self.error_func = self.expect_error()
        self.rem.got_an_error.connect(self.error_func)
        self.result_func = self.expect_result()
        self.rem.got_opendel_cmd.connect(self.result_func)
        self.rem.got_openasc_cmd.connect(self.result_func)

        for i in range(timeout // interval):
            if self.have_result:
                break
            if self.rem.idle_count >= self.idle_expect:
                break
            timer.start()
            self.app.exec_()

    def expect_error(self):
        ''' support for cell variables is broken, so we use explicit binding '''
        @QtCore.Slot(object, Exception)
        def error_func(self, e):
            self._log('got:', e)
            assert isinstance(e, Exception)
            self._result = e
            self._log('MYSELF', self)
        self._log('EXPECT', self)
        return partial(error_func, self)

    def expect_result(self):
        ''' support for cell variables is broken, so we use explicit binding '''
        @QtCore.Slot(object, str, int, str)
        def result_func(self, *args):
            self._log('got:', args)
            assert not isinstance(args, Exception)
            self._result = args
            self._log('MYSELF', self)
        self._log('EXPECT', self)
        return partial(result_func, self)


#------------------------------------
# now the tests
#
    def test_remote_setup(self):
        rem = RemoteControl(self.testpath)
        wrong = self.testpath + 'xxx'
        assert not os.path.exists(wrong)
        with pytest.raises(ConfigError):
            RemoteControl(wrong)

    def test_remote_idle(self):
        rem = self.rem
        # to see this, use
        # bin/python -m pytest . -k remote -v -s
        rem.start()
        self.emulate_eventloop()
        assert rem.idle_count == self.idle_expect

    def test_remote_missing_cmdfile(self):
        rem = self.rem
        rem.start()
        with open(self.hsh_fn, 'w') as f:
            f.close()
            self._log('TEST', self)
            self.emulate_eventloop()
        with pytest.raises(ConfigError):
            assert self.have_result
            assert isinstance(self._result, Exception)
            self._log(self._result)
            self.fetch_result()

    def test_remote_open_cmd_error(self):
        for i, (wrongarg, exc) in enumerate( (
            ('blurb ', InvalidCmdError),
            ('blurb crap', InvalidCmdError),
            ('opENdel crap', SyntaxError),
            ('openASC too;few', SyntaxError),
            ('OpEndel too;many;things;here', SyntaxError)
            )
        ):
            rem = self.rem
            rem.start()
            with open(self.cmd_fn, 'w') as f:
                f.write(wrongarg)
            with open(self.hsh_fn, 'w'):
                self._log('TEST', self)
                self.emulate_eventloop()
            with pytest.raises(exc):
                self._log('should get', i, exc)
                self._log(self._result)
                self.fetch_result()

    def test_remote_open_cmd_ok(self):
        rem = self.rem
        rem.start()
        with open(self.cmd_fn, 'w') as f:
            f.write('opendel somefile;42;4711')
        with open(self.hsh_fn, 'w'):
            self.emulate_eventloop()
        self._log(self._result)
        self.fetch_result()

    def test_remote_open_real_ok(self):
        paths = self.testpaths
        if not os.path.isfile(self.testpaths[1]):
            self._log(1000*'X', 'private paths not found')
            import pprint
            self._log(pprint.pprint(paths))
            self.testpaths = paths[:1]
        for path in self.testpaths:
            with open(self.hsh_fn, 'w'):
                rem = self.rem
                rem.start()
                with open(self.cmd_fn, 'w') as f:
                    f.write('openasc {};42;4711'.format(self.testpath))
                with open(self.hsh_fn, 'w'):
                    self.emulate_eventloop()
                self._log(self._result)
                self.fetch_result()
                assert(os.path.isfile(path))

    def test_cdb_open_real_ok(self, reader=None):
        paths = self.testpaths
        if not os.path.isfile(self.testpaths[1]):
            self._log(1000*'X', 'private paths not found')
            import pprint
            self._log(pprint.pprint(paths))
            self.testpaths = paths[:1]

        if not reader:
            reader = CDB_Reader()
        for path in self.testpaths:
            with open(self.hsh_fn, 'w'):
                rem = self.rem
                rem.start()
                with open(self.cmd_fn, 'w') as f:
                    f.write('openasc {};42;4711'.format(path))
                with open(self.hsh_fn, 'w'):
                    self.emulate_eventloop()
                self._log(self._result)
                self.fetch_result()
                assert(os.path.isfile(path))

                # we now have the file.
                # try to read the first record!
                testseq = CDB_Collection(path)
                pic_nr = testseq.current_form.pic_nr
                testseq.close()
                # call the new function
                res = reader.opendel_cmd(path, 0, pic_nr)
                reader.close_cmd()
                assert res == 0
                testseq = CDB_Collection(path)
                testseq.seek(1)
                pic_nr = testseq.current_form.pic_nr
                testseq.close()
                res = reader.opendel_cmd(path, 0, pic_nr)
                reader.close_cmd()
                assert res == 1


    def test_reader_connections(self):
        '''
        This is a special test to check which signals are connected
        with which signatures. We want to be able to check automatically
        if a certain signal has got a connection. This way we can show a message
        pop-up if a signal is not handled!
        '''
        def dummy(self, *args, **kw):
            pass
        reader = CDB_Reader()
        # note: we need to use 'str' here, because python 2.7 fails with unicode.
        # but I don't change the header!
        assert reader.receivers(str("2signal_goto(int)")) == 0
        reader.signal_goto.connect(dummy)
        assert reader.receivers(str("2signal_goto(int)")) == 1
        assert reader.receivers(str("2signal_open(PyObject)")) == 0
        reader.signal_open.connect(dummy)
        assert reader.receivers(str("2signal_open(PyObject)")) == 1
        assert reader.receivers(str("2signal_error(PyObject)")) == 0
        reader.signal_error.connect(dummy)
        assert reader.receivers(str("2signal_error(PyObject)")) == 1


    def test_open_del_asc(self):
        # path for issue#7
        p = os.path.join(ddc.rootpath, 'private/srw/data/issue/7/00099102.CDB')

        if not os.path.isfile(p):
            msg = 'private path not found'
            self._log(1000*'X', msg)
            pytest.skip(msg)

        reader = CDB_Reader()

        # this tests the correct behavior without the remote control
        reader.opendel_cmd(p, 268-1, '30409900600024')
        form = reader.seq.current_form
        assert form.is_deleted()
        assert form._form_index == 267

        # now the same with openasc
        reader.openasc_cmd(p, 268-1, '30409900600024')
        form = reader.seq.current_form
        assert not form.is_deleted()
        assert form._form_index == 268
