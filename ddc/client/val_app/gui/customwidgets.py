# -*- coding: utf-8 -*-
from __future__ import division, absolute_import, print_function, unicode_literals

from PySide import QtCore, QtGui


__all__ = ['MyImageLabel', 'MyTextWidget', 'MyScrollArea', 'MyLineEdit', 'MirrorLineEdit']

class MyImageLabel(QtGui.QLabel):
    """
    An extension to QLabel to support our own drawing.
    The purpose is to provide a hook, but insert the code
    later using a delegate.
    """
    def __init__(self, parent=None, f=0):
        super(MyImageLabel, self).__init__(parent, f)
        self.painter = None
        # we use a delegate function from the gui

    def sizePolicy(self):
        policy = super(MyImageLabel, self).sizePolicy()
        policy.hasHeightForWidth = lambda: True
        return policy

    def heightForWidth(self, w):
        pix = self.pixmap()
        if pix:
            h = w * pix.height() / pix.width()
        else:
            h = w
        return h

    def paintEvent(self, e):
        # this line does the default stuff: drawing our recipe
        super(MyImageLabel, self).paintEvent(e)
        # then, we add our own drawing primitives, enclosed into standard stuff.
        if self.painter and self.pixmap():
            qp = QtGui.QPainter()
            qp.begin(self)
            qp.setWindow(self.pixmap().rect())
            qp.setViewport(self.rect())
            self.painter.paint(qp)
            qp.end()

    def resizeEvent(self, e):
        s = e.size()

        w, h = s.width(), s.height()
        prop_h = self.heightForWidth(w)

        if h != prop_h:
            #self.resize(w, prop_h)
            # this happens only when setWidgetResizable is set,
            # so we can expect that the actual height is smaller.
            c = QtCore.Qt
            align = self.alignment()
            if align & c.AlignTop:
                # aling to top
                corr_y = 0
            elif align & c.AlignVCenter:
                # align to center
                corr_y = (h - prop_h) / 2
            else:
                # align to bottom
                corr_y = h - prop_h
            self.setGeometry(0, corr_y, w, prop_h)


class _Mediator(QtCore.QObject):
    '''
    This is a helper class for defining signals.
    Signal classes need to be directly inherited from QObject.
    This trick to overcome this limitation is to use a delegate
    via properties.
    '''

    font_scaled = QtCore.Signal()

    def __init__(self):
        super(_Mediator, self).__init__()


class MyTextWidget(QtGui.QWidget):
    def __init__(self, parent=None):
        super(MyTextWidget, self).__init__(parent)
        self._mediator = _Mediator()
        self.builder = None
        self.scroll_area = None
        self.auto_resize = False

    def resizeEvent(self, e):
        if not self.builder or not self.auto_resize:
            return
        w = e.size().width()
        oldw = e.oldSize().width()
        ww = self.layout().totalMinimumSize().width()
        if self.scroll_area:
            w = self.scroll_area.viewport().width()
        if oldw < w:
            neww = max(w, ww)
        else:
            neww = min(w, ww)
        self.builder.set_best_fitting_scale(neww)
        self.font_scaled.emit()
        return True

    @property
    def font_scaled(self):
        return self._mediator.font_scaled


class MyScrollArea(QtGui.QScrollArea):
    def resizeEvent(self, e):
        return super(MyScrollArea, self).resizeEvent(e)


class MyLineEdit(QtGui.QLineEdit):

    focus_in = QtCore.Signal(QtCore.QEvent, name='focusIn')
    mouse_press = QtCore.Signal(QtCore.QEvent, name='mousePress')
    getting_touched = QtCore.Signal(QtCore.QEvent, name='gettingTouched')

    def focusInEvent(self, event):
        self.focus_in.emit(event)
        return QtGui.QLineEdit.focusInEvent(self, event)

    def mousePressEvent(self, event):
        self.mouse_press.emit(event)
        return QtGui.QLineEdit.mousePressEvent(self, event)

    # this is a bit brute force but works better than
    # checking keyPressEvent and keyReleaseEvent.
    def event(self, event):
        if not isinstance(event, QtCore.QEvent):
            # Weird: we sometimes got the wrong event type
            # QtGui.QListWidgetItem. But since this if clause is in place
            # the bug did not show up, again.
            print('PySide bug: wrong event instance, ignored')
            return False
        self.getting_touched.emit(event)
        return QtGui.QLineEdit.event(self, event)


class MirrorLineEdit(QtGui.QLineEdit):
    '''
    How the mirror line edit works:

    The control's main function is
        mirror(widget)
    All settings of the 'widget' are read and assigned to 'self'.
    This makes the widget 'self' look exactly like widget 'widget'.

    Special tricks:
        self.setFocusProxy(widget)
    lets 'self' look like it has the focus, but being just a delegate.
        self.focusInEvent(QtGui.QFocusEvent(QtCore.QEvent.FocusIn))
    is a trick to leave a second cursor visible. Normally, focusIn and
    focusOut are balanced and allow just a single cursor.

    The redirection of mouse event was trickier to find out.
    Mouse events are created on a widget that really has the mouse spot.
    After lots of trying, the solution was very simple:
    We call the mouse event again on the target widget and clear our own event.
    '''
    def __init__(self, parent=None):
        self.mirrored_widget = None
        super(MirrorLineEdit, self).__init__(parent)

    def mirror(self, widget):
        if not isinstance(widget, QtGui.QLineEdit):
            return
        self.mirrored_widget = widget
        self.setReadOnly(False)
        self.setStyleSheet(widget.styleSheet())
        self.setFont(widget.font())
        self.setToolTip(widget.toolTip())
        # this is _the_ trick to produce a second cursor!
        self.focusInEvent(QtGui.QFocusEvent(QtCore.QEvent.FocusIn))
        self.setFixedSize(widget.size())
        self.setText(widget.text())
        self.setCursorPosition(widget.cursorPosition())
        start = widget.selectionStart()
        if start >= 0:
            self.setSelection(start, len(widget.selectedText()))
        # this redirects the focus to the real control but keeps the highlight.
        self.setFocusProxy(widget)

    def mousePressEvent(self, event):
        self._handle_mirrored_mouse(event)
        return QtGui.QLineEdit.mousePressEvent(self, event)

    def mouseReleaseEvent(self, event):
        self._handle_mirrored_mouse(event)
        return QtGui.QLineEdit.mouseReleaseEvent(self, event)

    def mouseMoveEvent(self, event):
        self._handle_mirrored_mouse(event)
        return QtGui.QLineEdit.mouseMoveEvent(self, event)

    def mouseDoubleClickEvent(self, event):
        self._handle_mirrored_mouse(event)
        return QtGui.QLineEdit.mouseDoubleClickEvent(self, event)

    def _handle_mirrored_mouse(self, event):
        widget = self.mirrored_widget
        if widget:
            widget.event(event)
        return True
