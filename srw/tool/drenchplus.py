# game

from random import randint

color_names = tuple('red yellow green blue orange purple'.split())

box = [None] * 3
box[0] = '┌───┐'
box[1] = '│   │'
box[2] = '└───┘'

yellows = '💛💛💚💛📗🐋💛🐰💛'

class Maze(object):
    def __init__(self, size):
        self.size = size
        self.create()
        self.randomize()

    def create(self):
        data = self.data = []
        size = self.size
        for r in range(0, size + 2):
            self.data.append(bytearray(b' ' * (size + 1) + b'\n'))

    def randomize(self):
        size = self.size
        ncolors = len(color_names)
        for r in range(1, size + 1):
            for c in range(size + 1):
                self.data[r][c] = ord(color_names[randint(0, ncolors-1)][0])

    def __repr__(self):
        txt = b''.join(self.data).decode('ascii')
        txt += '\n' + '\n'.join(box)
        return txt

x = 42