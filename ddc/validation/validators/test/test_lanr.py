# -*- coding: utf-8 -*-
from __future__ import division, absolute_import, print_function, unicode_literals

from pycerberus.test_util import *

from ddc.validation.validators.lanr import LANRValidator


class LANRTest(ValidationTest):
    validator_class = LANRValidator

    def assert_error_with_key(self, error_key, value, *args, **kwargs):
        e = self.assert_error(value, *args, **kwargs)
        assert_equals(error_key, e.details().key())
        return e

    def test_accepts_valid_number(self):
        # checksum 8+36=44 => %10='4' => 10 - 4 = 6
        assert_equals('240000601', self.process('240000601'))
        assert_equals('123456600', self.process('123456600'))

    def test_rejects_non_numeric_strings(self):
        self.assert_error_with_key('bad_pattern', 'abcdefghi')
        self.assert_error_with_key('bad_pattern', '00000000A')

    def test_rejects_number_strings_with_bad_number_of_digits(self):
        self.assert_error_with_key('bad_pattern', '1234567')
        self.assert_error_with_key('bad_pattern', '1234567890')

    def test_rejects_numbers_with_bad_check_digit(self):
        self.assert_error_with_key('bad_checkdigit', '123456701')
        self.assert_error_with_key('bad_checkdigit', '123456706')

    def test_rejects_bad_input(self):
        self.assert_error(1)
        self.assert_error(None)
        self.assert_error([])

    def test_accepts_pseudo_bsnr(self):
        # Für Bundeswehrärzte, Zahnärzte und Hebammen gibt es keine LANR und
        # keine BSNR. Diese sollten die Pseudo-Nummern 179999900 für die BSNR
        # und 999999900 für die LANR verwenden.
        # https://de.wikipedia.org/wiki/Lebenslange_Arztnummer
        assert_equals('999999900', self.process('999999900'))

