# -*- coding: utf-8 -*-
from __future__ import division, absolute_import, print_function, unicode_literals

import logging
import os
import sys

from pathtools.patterns import match_any_paths
from watchdog.events import PatternMatchingEventHandler
from watchdog.observers import Observer

from .bunch_assembler import BunchAssembler
from .data_manager import DataManager
from .discover_lib import find_data_files
from ..storage.paths import ibf_subdir


class IStateListener(object):
    def on_incomplete_bunch(self, bunch):
        pass

    def on_deleted_bunch(self, bunch):
        pass


class TerminatingErrorHandler(IStateListener):
    def on_error(self, error):
        sys.stderr.write('Found inconsistent data while grouping files!\n')
        sys.stderr.write('%s\n' % error.text)
        sys.exit(10)


def watchdog_delegate(discoverer):
    class WatchdogDelegate(PatternMatchingEventHandler):
        def on_created(self, event):
            discoverer(event.src_path)

        def on_deleted(self, event):
            discoverer(event.src_path)

        def on_moved(self, event):
            # This should handle the case when a .RDB file is already present
            # and some other tool just renames it to 'CDB'.
            # We might get into trouble when a complete CDB/IBF pair is moved
            # into the watched directory but according to Daniel Szoska
            # (2014-09-23) that should be treated as an error condition right
            # now.
            new_filename = event.dest_path
            if self._matches_watch_pattern(new_filename):
                discoverer(event.dest_path)
        def _matches_watch_pattern(self, filename):
            return match_any_paths((filename, ),
               included_patterns=self.patterns,
               excluded_patterns=self.ignore_patterns,
               case_sensitive=self.case_sensitive
            )

    return WatchdogDelegate(
        patterns=('*.cdb', '*.ibf', '*.durus'),
        case_sensitive=False,
        ignore_directories=False,
    )


# The StateKeeper is missing some functionality especially registering for
# notifications which is sorely needed to handle errors within the GUI client.
# I think we should at least display an alert window in case one of the data
# files is buggy (e.g. huge ibf). Also none of the classes
# (e.g. DataManager, StateKeeper) is thread-safe even though they should
# implement that for real watchdog integration (which provides a second source
# of events)
class StateKeeper(object):
    def __init__(self, root_path, log=None):
        self.root_path = root_path
        self.log = log or logging.getLogger(__name__)
        # LATER: I guess that should be a parameter for the constructor
        self.data_manager = DataManager()
        # listener registration not yet exposed as I don't know yet how that
        # should work in the end.
        self._listeners = [TerminatingErrorHandler()]
        self._observer = None

    def start_monitoring(self):
        self._observer = Observer()
        handler = watchdog_delegate(self.on_file_change)
        self._observer.schedule(handler, self.root_path, recursive=False)
        ibf_path = os.path.join(self.root_path, ibf_subdir)
        self._observer.schedule(handler, ibf_path, recursive=False)
        self._observer.start()

    def stop_monitoring(self):
        if self._observer is None:
            return
        self._observer.stop()
        self._observer.join()

    def force_state_refresh(self):
        data_files = find_data_files(self.root_path, recursive=False)
        bunch_results = BunchAssembler(data_files).process_filenames()
        self.notify_about_errors(bunch_results.errors)
        self.notify_about_incomplete_bunches(bunch_results.incomplete)
        self.data_manager.set_bunches(bunch_results.complete)

    def on_file_change(self, file_path):
        # I noticed watchdog (0.8.1, inotify observer) on Linux (Fedora 20,
        # ext4 file system) ignores a file removal if a file is deleted twice.
        # Not sure if that's a bug in my implementation (I didn't file a
        # watchdog bug yet) but for now going the safe way by just scanning
        # everything again.
        # The missing change will be found the time another file changes.
        self.force_state_refresh()

    # --- internal notification emitters --------------------------------------
    def notify_about_errors(self, errors):
        for error in errors:
            self._notify_listeners('error', error)

    def notify_about_incomplete_bunches(self, bunches):
        for bunch in bunches:
            self._notify_listeners('incomplete_bunch', bunch)

    def _notify_listeners(self, name, *args):
        for listener in self._listeners:
            handler = getattr(listener, 'on_'+name, None)
            if handler is not None:
                handler(*args)

