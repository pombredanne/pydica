# -*- coding: utf-8 -*-
from __future__ import division, absolute_import, print_function, unicode_literals

import logging

from pythonic_testcase import *

from ddc.tool.storage.batch import Batch
from ddc.tool.storage.cdb.cdb_fixtures import create_cdb_with_dummy_data
from ddc.tool.storage.durus_ import create_durus_fixture
from ddc.tool.storage.ibf.ibf_fixtures import create_ibf
from ddc.tool.storage.ask.ask_fixtures import create_ask
from ddc.tool.storage.paths import DataBunch
from ddc.tool.storage.testhelpers import set_durus_loglevel


class BatchTest(PythonicTestCase):
    def setUp(self):
        super(BatchTest, self).setUp()
        self._previous_loglevel = set_durus_loglevel(logging.ERROR)

    def tearDown(self):
        set_durus_loglevel(self._previous_loglevel)
        super(BatchTest, self).tearDown()

    def test_can_initialize_batch_without_real_files(self):
        databunch = DataBunch(
            cdb=create_cdb_with_dummy_data(nr_forms=1),
            ibf=create_ibf(nr_images=1),
            durus=create_durus_fixture(),
            ask=create_ask(),
        )
        batch = Batch.init_from_bunch(databunch, create_new_durus=False)
        assert_isinstance(batch, Batch)
