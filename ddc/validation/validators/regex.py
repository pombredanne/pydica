# -*- coding: utf-8 -*-
from __future__ import division, absolute_import, print_function, unicode_literals

import re

from pycerberus.validators import StringValidator


__all__ = ['RegexValidator']

class RegexValidator(StringValidator):
    def __init__(self, regex=None, use_match_for_conversion=False, *args, **kwargs):
        pattern = regex
        if not pattern.startswith('^'):
            pattern = '^'+pattern
        if not pattern.endswith('$'):
            pattern += '$'
        self.regex = re.compile(pattern)
        self.use_match_for_conversion = use_match_for_conversion
        self.super(*args, **kwargs)

    def messages(self):
        return {
            'bad_pattern': u'Input %(input_)s does not match the expected pattern.',
        }

    def convert(self, value, context):
        string_value = super(RegexValidator, self).convert(value, context)
        if not self.use_match_for_conversion:
            return string_value
        return self._assert_regex(string_value, context)

    def _assert_regex(self, value, context):
        match = self.regex.match(value)
        if match is None:
            self.raise_error('bad_pattern', value, context, input_=value)
        return match

    def validate(self, value, context):
        if not self.use_match_for_conversion:
            self._assert_regex(value, context)

