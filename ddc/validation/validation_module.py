# -*- coding: utf-8 -*-
from __future__ import division, absolute_import, print_function, unicode_literals

from datetime import datetime
import os
from pprint import pformat
import sys

from ddc.config import init_environment
from ddc.lib.attribute_dict import AttrDict
from ddc.lib.dict_merger import merge_dicts
from ddc.lib.request_logger import RequestLogger
from ddc.tool.storage import Batch, DurusKey, Task, TaskStatus, TaskType
from ddc.tool.storage.paths import databunch_for_cdb
from ddc.validation.form_validation import FormValidation


__all__ = ['validation_module_for_batch', 'ValidationModule']

def validation_module_for_batch(batch, bunch, context):
    plugin_mgr = context['plugin_manager']
    batch_meta = plugin_mgr.query_plugins('batch_metadata', bunch=bunch, batch=batch)
    batch.meta.update(batch_meta)
    batch_context = merge_dicts(context, {'batch_meta': batch_meta})
    return ValidationModule(batch, batch_context)

# TODO: log all actions
class ValidationModule(object):
    """
    This class should handle all validation-related activity including
    heuristic fixes, customer-specific data/error handling), tasks and logging.
    It knows batches and tasks is generally the "go-to" place when you want to
    perform (automatic) validation.

    Please note that all operations which work on the "plain" form data are
    implemented in the FormValidation class while this class should be about
    integrating the actual validation into the pydica system.

    Please note that the ValidationModule is currently work in progress, some
    features (such as tasks and logging) are missing/not implemented completely.
    Also the API may change later on depending on the actual GUI needs.
    """
    def __init__(self, batch, context):
        self.batch = batch
        self.context = context
        self.schema, self.heuristics, self.post_processors = self.load_validation_rules()

    def load_validation_rules(self):
        schema = None
        heuristics = []
        post_processors = []
        activated_plugins = self.context['plugin_manager'].activated_plugins
        for plugin in activated_plugins.values():
            if schema is None:
                schema = plugin.form_validation_schema(self.context)
            heuristics.extend(plugin.validation_heuristics(self.context))
            post_processors.extend(plugin.validation_post_processors(self.context))
        if schema is None:
            raise ValueError('No plugin contributed a validation schema')
        return (schema, heuristics, post_processors)

    def process_tasks(self):
        tasks = self.batch.new_tasks(TaskType.FORM_VALIDATION)
        for task in tasks:
            self.process_task(task)

    def process_task(self, task):
        """Validate all fields of a form (specified by the given form
        validation task).
        Afterwards the initial task will be closed. The method creates
        new tasks in case of validation errors."""
        # LATER: Somehow we need to deal with the edge case that there are
        # other validation errors regarding the same prescription.
        # (need to handle duplication)
        assert task.type_ == TaskType.FORM_VALIDATION
        form_position = task.form_position
        form = self.batch.form(form_position)
        form_result = self.process_form(form, form_position)

        new_tasks = []
        for field_name, field_errors in form_result.errors.items():
            _create = self._create_verification_task_for_error
            task = _create(form_position, field_name, field_errors)
            new_tasks.append(task)

        root = self.batch.durus
        root[DurusKey.TASKS].extend(new_tasks)

        task.last_modified = datetime.now()
        task.status = TaskStatus.CLOSED

    def process_values(self, form_values, extra_context=None):
        """
        This method is really just an alias for doing validation plainly on the
        provided values without any reference to forms.
        Callers should have a single API for doing verification.
        """
        validator = FormValidation(self.schema,
            heuristics=self.heuristics,
            post_processors=self.post_processors
        )
        context = self.context
        if extra_context is not None:
            context = self.context.copy()
            context.update(extra_context)
        form_result = validator.run(form_values, context=context)
        if self._contains_unexpected_exceptions(form_result):
            self._log_validation_exception(form_result, context)
        return form_result

    def process_form(self, form, form_position):
        """Validate all form fields and return the form result."""
        form_values = dict((name, form.fields[name].value) for name in form.field_names)
        plugin_mgr = self.context['plugin_manager']
        form_meta = plugin_mgr.query_plugins('form_metadata', form=form, form_position=form_position, context=self.context)
        return self.process_values(form_values, extra_context={'form_meta': form_meta})

    # --- internal helpers ----------------------------------------------------

    def _create_verification_task_for_error(self, form_position, field_name,
                                            errors):
        task_meta = AttrDict(
            field_name = field_name,
            errors = tuple(errors),
        )
        return Task(form_position, TaskType.VERIFICATION,
                    status=TaskStatus.NEW, data=task_meta)

    # --- logging of unexpected exceptions (bugs in validators) ---------------
    def _contains_unexpected_exceptions(self, form_result):
        """return True if form_result contains an error with the
        'unexpected_exception' key"""
        contains_errors = form_result
        if contains_errors and (self._find_first_unexpected_exception(form_result) is not None):
            return True
        return False

    def _find_first_unexpected_exception(self, form_result):
        for field, errors in form_result.errors.items():
            for error in errors:
                if error.key == 'unexpected_exception':
                    return (field, error)
        return None

    def _log_validation_exception(self, form_result, context):
        "create an error report file if context contains an environment dir."
        # LATER: create shared utility function for common exception reporting
        env_path = self.context.get('env_dir')
        if not env_path:
            return
        exception_dir = os.path.join(env_path, 'exceptions')
        if not os.path.exists(exception_dir):
            os.makedirs(exception_dir)

        field_name, validation_error = self._find_first_unexpected_exception(form_result)
        fake_request = AttrDict(
            method='Error when validating field %s\n\n' % field_name,
            path_qs='',
            http_version='',
            headers=form_result.initial_value,
        )
        username = os.getlogin()
        # LATER: get PIC nr/cdb batch file name
        kv_to_string = lambda item: '%s: %s' % (item[0], pformat(item[1]))
        context_string = '\n'.join(map(kv_to_string, sorted(context.items())))
        extras = context_string
        logger = RequestLogger(fake_request,
            username=username,
            exc_info=validation_error.meta,
            extra_info=extras.encode('utf8'),
        )
        logger.create_and_store_report(exception_dir)


def main(argv=sys.argv):
    def error_and_exit(extra_message=None):
        print('usage: {0!s} CONFIGFILE CDBFILENAME'.format(argv[0]))
        if extra_message:
            print(extra_message)
        sys.exit(0)

    if len(sys.argv) < 3:
        error_and_exit()
    config_file = argv[1]
    cdb_filename = argv[2]
    if not os.path.exists(config_file):
        error_and_exit('config file not found: %s' % config_file)
    if not os.path.exists(cdb_filename):
        error_and_exit('cdb file not found: %s' % cdb_filename)

    pydica_context = init_environment(config_file)
    batch = Batch.init_from_bunch(databunch_for_cdb(cdb_filename))

    module = ValidationModule(batch, pydica_context)
    module.process_tasks()
    batch.commit()

if __name__ == '__main__':
    main()
