# -*- coding: utf-8 -*-
from __future__ import division, absolute_import, print_function, unicode_literals

from unittest import TestCase

from pythonic_testcase import *

from ddc.validation import SimpleDB


class SimpleDBTest(TestCase):
    def test_can_parse_simple_list_of_numbers(self):
        db = SimpleDB.from_string('1234\n5678\n\n')
        assert_length(2, db.items)
        assert_true(db.contains('1234'))
        assert_true(db.contains('5678'))
        assert_false(db.contains('12345678'))

    def test_can_ignore_comments(self):
        db = SimpleDB.from_string('#foo\n;bar\n1234\n#baz\n')
        assert_length(1, db.items)
        assert_true(db.contains('1234'))

    def test_can_strip_whitespace_and_deal_with_windows_style_line_endings(self):
        db = SimpleDB.from_string('   12   \r\n34')
        assert_length(2, db.items)
        assert_true(db.contains('12'))
        assert_true(db.contains('34'))

